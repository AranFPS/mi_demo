﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class ThreeSixtyManager : MonoBehaviour {

	public GameObject SphereVideo;
	public GameObject SpherePhoto;

	public VideoPlayer videoPlayer;

	void Awake(){
		StartPhoto ();
	}

	public void StartVideo() {
		SpherePhoto.SetActive (false);
		SphereVideo.SetActive (true);
		StartCoroutine (playVideo ());
	}

	public void StartPhoto() {
		SphereVideo.SetActive (false);
		SpherePhoto.SetActive (true);
	}

	public void Reset() {
		MainManager.Instance.SetCurrentScene (SCENES.MainMenu);
	}

	IEnumerator playVideo()
	{
		videoPlayer.playOnAwake = false;
		videoPlayer.Prepare ();

		//Waitfor awhile
		WaitForSeconds waitTime = new WaitForSeconds(1);
		while (!videoPlayer.isPrepared) {
			yield return waitTime;
			break;
		}
		videoPlayer.Play ();

		while (videoPlayer.isPlaying) {
			yield return null;
		}
	}
}
