﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeManager : MonoBehaviour {
	float RotationSpeed = 10;
	private void OnMouseDrag()
	{
		float x = Input.GetAxis ("Mouse X") * RotationSpeed * Mathf.Deg2Rad;
		transform.RotateAround (Vector3.down, x);
	}
}
