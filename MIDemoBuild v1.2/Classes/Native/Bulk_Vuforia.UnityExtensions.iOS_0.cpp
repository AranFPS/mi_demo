﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Vuforia.VuforiaNativeIosWrapper
struct VuforiaNativeIosWrapper_t2037146173;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;




#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef VUFORIANATIVEIOSWRAPPER_T2037146173_H
#define VUFORIANATIVEIOSWRAPPER_T2037146173_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaNativeIosWrapper
struct  VuforiaNativeIosWrapper_t2037146173  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIANATIVEIOSWRAPPER_T2037146173_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3528271667* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3528271667* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3528271667** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3528271667* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef STRINGBUILDER_T_H
#define STRINGBUILDER_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.StringBuilder
struct  StringBuilder_t  : public RuntimeObject
{
public:
	// System.Int32 System.Text.StringBuilder::_length
	int32_t ____length_1;
	// System.String System.Text.StringBuilder::_str
	String_t* ____str_2;
	// System.String System.Text.StringBuilder::_cached_str
	String_t* ____cached_str_3;
	// System.Int32 System.Text.StringBuilder::_maxCapacity
	int32_t ____maxCapacity_4;

public:
	inline static int32_t get_offset_of__length_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____length_1)); }
	inline int32_t get__length_1() const { return ____length_1; }
	inline int32_t* get_address_of__length_1() { return &____length_1; }
	inline void set__length_1(int32_t value)
	{
		____length_1 = value;
	}

	inline static int32_t get_offset_of__str_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____str_2)); }
	inline String_t* get__str_2() const { return ____str_2; }
	inline String_t** get_address_of__str_2() { return &____str_2; }
	inline void set__str_2(String_t* value)
	{
		____str_2 = value;
		Il2CppCodeGenWriteBarrier((&____str_2), value);
	}

	inline static int32_t get_offset_of__cached_str_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____cached_str_3)); }
	inline String_t* get__cached_str_3() const { return ____cached_str_3; }
	inline String_t** get_address_of__cached_str_3() { return &____cached_str_3; }
	inline void set__cached_str_3(String_t* value)
	{
		____cached_str_3 = value;
		Il2CppCodeGenWriteBarrier((&____cached_str_3), value);
	}

	inline static int32_t get_offset_of__maxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ____maxCapacity_4)); }
	inline int32_t get__maxCapacity_4() const { return ____maxCapacity_4; }
	inline int32_t* get_address_of__maxCapacity_4() { return &____maxCapacity_4; }
	inline void set__maxCapacity_4(int32_t value)
	{
		____maxCapacity_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGBUILDER_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_2)); }
	inline uint32_t get_m_value_2() const { return ___m_value_2; }
	inline uint32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H



// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceDeinitCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceDeinitCamera_m1114664889 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceGetCameraDirection()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceGetCameraDirection_m432955548 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceGetCameraFieldOfViewRads(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceGetCameraFieldOfViewRads_m2397779624 (RuntimeObject * __this /* static, unused */, intptr_t ___fovVectorContainer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::cameraDeviceGetVideoMode(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_cameraDeviceGetVideoMode_m3934990628 (RuntimeObject * __this /* static, unused */, int32_t ___idx0, intptr_t ___videoMode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceInitCamera(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceInitCamera_m1726182893 (RuntimeObject * __this /* static, unused */, int32_t ___camera0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceSelectVideoMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceSelectVideoMode_m3476877314 (RuntimeObject * __this /* static, unused */, int32_t ___idx0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::cameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_cameraDeviceSetCameraConfiguration_m3550565238 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceStartCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceStartCamera_m813609651 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceStopCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceStopCamera_m2155385929 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_AddDistortionCoefficient(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_AddDistortionCoefficient_m1019494423 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_delete(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_delete_m2548462463 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::customViewerParameters_new(System.Single,System.String,System.String)
extern "C"  intptr_t VuforiaNativeIosWrapper_customViewerParameters_new_m3376464988 (RuntimeObject * __this /* static, unused */, float ___version0, String_t* ___name1, String_t* ___manufacturer2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetButtonType(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetButtonType_m3745586757 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, int32_t ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetContainsMagnet(System.IntPtr,System.Boolean)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetContainsMagnet_m334534216 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, bool ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetFieldOfView(System.IntPtr,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetFieldOfView_m3080117215 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, intptr_t ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetInterLensDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetInterLensDistance_m1494617937 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetLensCentreToTrayDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetLensCentreToTrayDistance_m2220789831 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetScreenToLensDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetScreenToLensDistance_m2540654929 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetTrayAlignment(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetTrayAlignment_m1020920963 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, int32_t ___val1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_cylinderTargetGetDimensions_m2170405224 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___dimensionPtr2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_cylinderTargetSetSideLength_m1744635518 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, float ___sideLength2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetDestroyTrackable(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetDestroyTrackable_m3399286625 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, int32_t ___trackableId1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetExists(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetExists_m835795820 (RuntimeObject * __this /* static, unused */, String_t* ___relativePath0, int32_t ___storageType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetGetNumTrackableType(System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetGetNumTrackableType_m2199850757 (RuntimeObject * __this /* static, unused */, int32_t ___trackableType0, intptr_t ___dataSetPtr1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetGetTrackableName_m1401398735 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, int32_t ___trackableId1, StringBuilder_t * ___trackableName2, int32_t ___nameMaxLength3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetGetTrackablesOfType_m2513650173 (RuntimeObject * __this /* static, unused */, int32_t ___trackableType0, intptr_t ___trackableDataArray1, int32_t ___trackableDataArrayLength2, intptr_t ___dataSetPtr3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetLoad(System.String,System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetLoad_m436879107 (RuntimeObject * __this /* static, unused */, String_t* ___relativePath0, int32_t ___storageType1, intptr_t ___dataSetPtr2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::deinitFrameState(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_deinitFrameState_m1882496214 (RuntimeObject * __this /* static, unused */, intptr_t ___frameState0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_GetMode()
extern "C"  int32_t VuforiaNativeIosWrapper_device_GetMode_m2453552606 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::device_GetViewerList()
extern "C"  intptr_t VuforiaNativeIosWrapper_device_GetViewerList_m2362888643 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_IsViewerPresent()
extern "C"  int32_t VuforiaNativeIosWrapper_device_IsViewerPresent_m1201399086 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_SelectViewer(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_device_SelectViewer_m1853041852 (RuntimeObject * __this /* static, unused */, intptr_t ___vp0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_SetMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_device_SetMode_m2286413161 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::device_SetViewerPresent(System.Boolean)
extern "C"  void VuforiaNativeIosWrapper_device_SetViewerPresent_m2161993502 (RuntimeObject * __this /* static, unused */, bool ___present0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::deviceIsEyewearDevice()
extern "C"  int32_t VuforiaNativeIosWrapper_deviceIsEyewearDevice_m3851964756 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceGetScreenOrientation()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceGetScreenOrientation_m2250536774 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceIsDisplayExtended()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceIsDisplayExtended_m1602644497 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceIsDualDisplay()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceIsDualDisplay_m3124003728 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceIsSeeThru()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceIsSeeThru_m4200768691 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceSetDisplayExtended(System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceSetDisplayExtended_m3471292286 (RuntimeObject * __this /* static, unused */, bool ___enable0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::getProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_getProjectionGL_m427606978 (RuntimeObject * __this /* static, unused */, float ___nearPlane0, float ___farPlane1, intptr_t ___projectionContainer2, int32_t ___screenOrientation3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::hasSurfaceBeenRecreated()
extern "C"  int32_t VuforiaNativeIosWrapper_hasSurfaceBeenRecreated_m1823633968 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderBuild(System.String,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetBuilderBuild_m3582937393 (RuntimeObject * __this /* static, unused */, String_t* ___name0, float ___screenSizeWidth1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderGetFrameQuality()
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetBuilderGetFrameQuality_m3184258030 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderGetTrackableSource()
extern "C"  intptr_t VuforiaNativeIosWrapper_imageTargetBuilderGetTrackableSource_m8878657 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderStartScan()
extern "C"  void VuforiaNativeIosWrapper_imageTargetBuilderStartScan_m3177974730 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderStopScan()
extern "C"  void VuforiaNativeIosWrapper_imageTargetBuilderStopScan_m4159555976 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetCreateVirtualButton_m4190927545 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, intptr_t ___rectData3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetDestroyVirtualButton_m3151629955 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetGetNumVirtualButtons(System.IntPtr,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetGetNumVirtualButtons_m3250531349 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetGetVirtualButtonName_m1930708682 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, int32_t ___idx2, StringBuilder_t * ___vbName3, int32_t ___nameMaxLength4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetGetVirtualButtons_m2274780740 (RuntimeObject * __this /* static, unused */, intptr_t ___virtualButtonDataArray0, intptr_t ___rectangleDataArray1, int32_t ___virtualButtonDataArrayLength2, intptr_t ___dataSetPtr3, String_t* ___trackableName4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::initFrameState(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_initFrameState_m251019466 (RuntimeObject * __this /* static, unused */, intptr_t ___frameState0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::initPlatformNative()
extern "C"  void VuforiaNativeIosWrapper_initPlatformNative_m1148360790 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single Vuforia.VuforiaNativeIosWrapper::multiTargetGetLargestSizeComponent(System.IntPtr,System.String)
extern "C"  float VuforiaNativeIosWrapper_multiTargetGetLargestSizeComponent_m877427687 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTargetGetSize_m2380033087 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___sizePtr2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTargetSetSize_m1443530746 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___sizePtr2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTrackerActivateDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTrackerActivateDataSet_m989691554 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::objectTrackerCreateDataSet()
extern "C"  intptr_t VuforiaNativeIosWrapper_objectTrackerCreateDataSet_m3029708900 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTrackerDeactivateDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTrackerDeactivateDataSet_m975093771 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTrackerDestroyDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTrackerDestroyDataSet_m311987895 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::onPause()
extern "C"  void VuforiaNativeIosWrapper_onPause_m2162095750 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::onResume()
extern "C"  void VuforiaNativeIosWrapper_onResume_m1164754594 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::onSurfaceChanged(System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_onSurfaceChanged_m3866066131 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::qcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_qcarAddCameraFrame_m3321820358 (RuntimeObject * __this /* static, unused */, intptr_t ___pixels0, int32_t ___width1, int32_t ___height2, int32_t ___format3, int32_t ___stride4, int32_t ___frameIdx5, int32_t ___flipHorizontally6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::qcarDeinit()
extern "C"  void VuforiaNativeIosWrapper_qcarDeinit_m4212576879 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarGetBufferSize(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarGetBufferSize_m3158994288 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___format2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarInit(System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarInit_m496390534 (RuntimeObject * __this /* static, unused */, String_t* ___licenseKey0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarSetFrameFormat(System.Int32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarSetFrameFormat_m4090591613 (RuntimeObject * __this /* static, unused */, int32_t ___format0, int32_t ___enabled1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarSetHint(System.UInt32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarSetHint_m2555757340 (RuntimeObject * __this /* static, unused */, uint32_t ___hint0, int32_t ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::reconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_reconstructionFromTargetSetInitializationTarget_m1740495597 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, intptr_t ___dataSetPtr1, int32_t ___trackableID2, intptr_t ___occluderMin3, intptr_t ___occluderMax4, intptr_t ___offsetToOccluder5, intptr_t ___rotationAxisToOccluder6, float ___rotationAngleToOccluder7, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::reconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_reconstructionSetMaximumArea_m3961464079 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, intptr_t ___maximumArea1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::reconstructionSetNavMeshPadding(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_reconstructionSetNavMeshPadding_m881105308 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, float ___padding1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::reconstructionStart(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_reconstructionStart_m4250848222 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::rendererCreateNativeTexture(System.UInt32,System.UInt32,System.Int32)
extern "C"  intptr_t VuforiaNativeIosWrapper_rendererCreateNativeTexture_m1694208006 (RuntimeObject * __this /* static, unused */, uint32_t ___width0, uint32_t ___height1, int32_t ___format2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererGetGraphicsAPI()
extern "C"  int32_t VuforiaNativeIosWrapper_rendererGetGraphicsAPI_m3560655757 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::rendererGetVideoBackgroundCfg(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_rendererGetVideoBackgroundCfg_m1678358512 (RuntimeObject * __this /* static, unused */, intptr_t ___bgCfg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererGetVideoBackgroundTextureInfo(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_rendererGetVideoBackgroundTextureInfo_m2700164971 (RuntimeObject * __this /* static, unused */, intptr_t ___texInfo0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererIsVideoBackgroundTextureInfoAvailable()
extern "C"  int32_t VuforiaNativeIosWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m1274323877 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::rendererSetVideoBackgroundCfg(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_rendererSetVideoBackgroundCfg_m1972712013 (RuntimeObject * __this /* static, unused */, intptr_t ___bgCfg0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererSetVideoBackgroundTextureID(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_rendererSetVideoBackgroundTextureID_m1187272398 (RuntimeObject * __this /* static, unused */, int32_t ___textureID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererSetVideoBackgroundTexturePtr(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_rendererSetVideoBackgroundTexturePtr_m3392285240 (RuntimeObject * __this /* static, unused */, intptr_t ___texturePtr0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_DeleteCopy()
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_DeleteCopy_m535892042 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetDistortionMesh(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetDistortionMesh_m399350896 (RuntimeObject * __this /* static, unused */, int32_t ___viewId0, intptr_t ___meshData1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetDistortionMeshSize(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetDistortionMeshSize_m3433711271 (RuntimeObject * __this /* static, unused */, int32_t ___viewId0, intptr_t ___size1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetEffectiveFov(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetEffectiveFov_m1766427464 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___fovContainer1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetEyeDisplayAdjustmentMatrix(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetEyeDisplayAdjustmentMatrix_m1813656632 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___matrixContainer1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetNormalizedViewport(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetNormalizedViewport_m1528191784 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___viewportContainer1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetProjectionMatrix(System.Int32,System.Single,System.Single,System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetProjectionMatrix_m1357884672 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, float ___near1, float ___far2, intptr_t ___projectionContainer3, int32_t ___screenOrientation4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetViewport(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetViewport_m2833809889 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___viewportContainer1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetViewportCentreToEyeAxis(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetViewportCentreToEyeAxis_m2133419223 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___vectorContainer1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_GetModelCorrectionTransform(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_rotationalDeviceTracker_GetModelCorrectionTransform_m4116192559 (RuntimeObject * __this /* static, unused */, intptr_t ___pivot0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_Recenter()
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_Recenter_m2292918386 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_SetModelCorrectionMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_SetModelCorrectionMode_m3760711071 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_SetModelCorrectionModeWithTransform(System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_SetModelCorrectionModeWithTransform_m1499777731 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, intptr_t ___pivot1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_SetPosePrediction(System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_SetPosePrediction_m3158892482 (RuntimeObject * __this /* static, unused */, bool ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::setApplicationEnvironment(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_setApplicationEnvironment_m86779826 (RuntimeObject * __this /* static, unused */, int32_t ___unityVersionMajor0, int32_t ___unityVersionMinor1, int32_t ___unityVersionChange2, int32_t ___sdkWrapperType3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::setRenderBuffers(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_setRenderBuffers_m3774876697 (RuntimeObject * __this /* static, unused */, intptr_t ___colorBuffer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainBuilderAddReconstruction(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainBuilderAddReconstruction_m1293846854 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromTarget()
extern "C"  intptr_t VuforiaNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3676350375 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainBuilderRemoveReconstruction(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainBuilderRemoveReconstruction_m1763309370 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainTrackerDeinitBuilder()
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainTrackerDeinitBuilder_m2292570849 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainTrackerInitBuilder()
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainTrackerInitBuilder_m3524084684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainTrackerSetScaleToMillimeter(System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainTrackerSetScaleToMillimeter_m563366966 (RuntimeObject * __this /* static, unused */, float ___scaleToMillimenters0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::startExtendedTracking(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_startExtendedTracking_m357143318 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, int32_t ___trackableID1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderDeinit()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderDeinit_m1075093056 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderGetInitState()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderGetInitState_m2122533097 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderGetResults(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderGetResults_m29416918 (RuntimeObject * __this /* static, unused */, intptr_t ___searchResultArray0, int32_t ___searchResultArrayLength1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderStartInit(System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderStartInit_m282508 (RuntimeObject * __this /* static, unused */, String_t* ___userKey0, String_t* ___secretKey1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderStartRecognition()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderStartRecognition_m2312748431 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderStop()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderStop_m1766141929 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::targetFinderUpdate(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_targetFinderUpdate_m307650428 (RuntimeObject * __this /* static, unused */, intptr_t ___targetFinderState0, int32_t ___filterMode1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::trackerManagerDeinitTracker(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_trackerManagerDeinitTracker_m862961198 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::trackerManagerInitTracker(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_trackerManagerInitTracker_m3155645533 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::trackerStart(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_trackerStart_m3650945244 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::trackerStop(System.Int32)
extern "C"  void VuforiaNativeIosWrapper_trackerStop_m136099586 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::updateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_updateQCAR_m3823614422 (RuntimeObject * __this /* static, unused */, intptr_t ___imageHeaderDataArray0, int32_t ___imageHeaderArrayLength1, intptr_t ___frameState2, int32_t ___screenOrientation3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::viewerParameters_delete(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_viewerParameters_delete_m1022060148 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::viewerParametersList_GetByIndex(System.IntPtr,System.Int32)
extern "C"  intptr_t VuforiaNativeIosWrapper_viewerParametersList_GetByIndex_m1568290052 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, int32_t ___idx1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::viewerParametersList_GetByNameManufacturer(System.IntPtr,System.String,System.String)
extern "C"  intptr_t VuforiaNativeIosWrapper_viewerParametersList_GetByNameManufacturer_m2600346792 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, String_t* ___name1, String_t* ___manufacturer2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VuforiaNativeIosWrapper::viewerParametersList_SetSDKFilter(System.IntPtr,System.String)
extern "C"  void VuforiaNativeIosWrapper_viewerParametersList_SetSDKFilter_m1101420845 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, String_t* ___filter1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::viewerParametersList_Size(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_viewerParametersList_Size_m3374613508 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonGetId(System.IntPtr,System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonGetId_m2887502506 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonSetAreaRectangle_m3959624685 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, intptr_t ___rectData3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonSetEnabled_m4103479847 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, int32_t ___enabled3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonSetSensitivity_m862172097 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, int32_t ___sensitivity3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::vuforiaGetRenderEventCallback()
extern "C"  intptr_t VuforiaNativeIosWrapper_vuforiaGetRenderEventCallback_m3112993526 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::vuMarkTemplateGetOrigin(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_vuMarkTemplateGetOrigin_m917331585 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___originPtr2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::vuMarkTemplateSetTrackingFromRuntimeAppearance(System.IntPtr,System.String,System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_vuMarkTemplateSetTrackingFromRuntimeAppearance_m3954844824 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, bool ___enable2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListAddWordsFromFile(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListAddWordsFromFile_m2691145391 (RuntimeObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListAddWordToFilterListU(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListAddWordToFilterListU_m944574365 (RuntimeObject * __this /* static, unused */, intptr_t ___word0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListAddWordU(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListAddWordU_m3831652562 (RuntimeObject * __this /* static, unused */, intptr_t ___word0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListLoadFilterList(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListLoadFilterList_m604465529 (RuntimeObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListLoadWordList(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListLoadWordList_m3605546583 (RuntimeObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListSetFilterMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListSetFilterMode_m2109095690 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListUnloadAllLists()
extern "C"  int32_t VuforiaNativeIosWrapper_wordListUnloadAllLists_m316317724 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceDeinitCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceDeinitCamera_m2265966432 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_cameraDeviceDeinitCamera_m1114664889(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceGetCameraDirection()
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceGetCameraDirection_m3970870237 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_cameraDeviceGetCameraDirection_m432955548(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceGetCameraFieldOfViewRads(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceGetCameraFieldOfViewRads_m1721373878 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___fovVectorContainer0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___fovVectorContainer0;
		int32_t L_1 = VuforiaNativeIosWrapper_cameraDeviceGetCameraFieldOfViewRads_m2397779624(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CameraDeviceGetVideoMode(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_CameraDeviceGetVideoMode_m1119707731 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___idx0, intptr_t ___videoMode1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___idx0;
		intptr_t L_1 = ___videoMode1;
		VuforiaNativeIosWrapper_cameraDeviceGetVideoMode_m3934990628(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceInitCamera(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceInitCamera_m3977988649 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___camera0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___camera0;
		int32_t L_1 = VuforiaNativeIosWrapper_cameraDeviceInitCamera_m1726182893(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceSelectVideoMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceSelectVideoMode_m3857227441 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___idx0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___idx0;
		int32_t L_1 = VuforiaNativeIosWrapper_cameraDeviceSelectVideoMode_m3476877314(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_CameraDeviceSetCameraConfiguration_m2906443201 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		VuforiaNativeIosWrapper_cameraDeviceSetCameraConfiguration_m3550565238(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceStartCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceStartCamera_m4281273918 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_cameraDeviceStartCamera_m813609651(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CameraDeviceStopCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_CameraDeviceStopCamera_m1232949092 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_cameraDeviceStopCamera_m2155385929(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_AddDistortionCoefficient(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_AddDistortionCoefficient_m3161392617 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		float L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_AddDistortionCoefficient_m1019494423(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_delete(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_delete_m2714481664 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		VuforiaNativeIosWrapper_customViewerParameters_delete_m2548462463(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_new(System.Single,System.String,System.String)
extern "C"  intptr_t VuforiaNativeIosWrapper_CustomViewerParameters_new_m3683021815 (VuforiaNativeIosWrapper_t2037146173 * __this, float ___version0, String_t* ___name1, String_t* ___manufacturer2, const RuntimeMethod* method)
{
	{
		float L_0 = ___version0;
		String_t* L_1 = ___name1;
		String_t* L_2 = ___manufacturer2;
		intptr_t L_3 = VuforiaNativeIosWrapper_customViewerParameters_new_m3376464988(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetButtonType(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetButtonType_m1379986059 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, int32_t ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		int32_t L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetButtonType_m3745586757(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetContainsMagnet(System.IntPtr,System.Boolean)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetContainsMagnet_m1719624713 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, bool ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		bool L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetContainsMagnet_m334534216(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetFieldOfView(System.IntPtr,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetFieldOfView_m1174026234 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, intptr_t ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		intptr_t L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetFieldOfView_m3080117215(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetInterLensDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetInterLensDistance_m959942070 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		float L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetInterLensDistance_m1494617937(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetLensCentreToTrayDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetLensCentreToTrayDistance_m2405383613 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		float L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetLensCentreToTrayDistance_m2220789831(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetScreenToLensDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetScreenToLensDistance_m1050756037 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		float L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetScreenToLensDistance_m2540654929(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::CustomViewerParameters_SetTrayAlignment(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_CustomViewerParameters_SetTrayAlignment_m2766020139 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, int32_t ___val1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		int32_t L_1 = ___val1;
		VuforiaNativeIosWrapper_customViewerParameters_SetTrayAlignment_m1020920963(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_CylinderTargetGetDimensions_m240505357 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___dimensionPtr2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		intptr_t L_2 = ___dimensionPtr2;
		int32_t L_3 = VuforiaNativeIosWrapper_cylinderTargetGetDimensions_m2170405224(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::CylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_CylinderTargetSetSideLength_m2622284254 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, float ___sideLength2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		float L_2 = ___sideLength2;
		int32_t L_3 = VuforiaNativeIosWrapper_cylinderTargetSetSideLength_m1744635518(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DataSetDestroyTrackable(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_DataSetDestroyTrackable_m864732963 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, int32_t ___trackableId1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		int32_t L_1 = ___trackableId1;
		int32_t L_2 = VuforiaNativeIosWrapper_dataSetDestroyTrackable_m3399286625(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DataSetExists(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_DataSetExists_m1010005507 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___relativePath0, int32_t ___storageType1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___relativePath0;
		int32_t L_1 = ___storageType1;
		int32_t L_2 = VuforiaNativeIosWrapper_dataSetExists_m835795820(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DataSetGetNumTrackableType(System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_DataSetGetNumTrackableType_m2093616101 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___trackableType0, intptr_t ___dataSetPtr1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___trackableType0;
		intptr_t L_1 = ___dataSetPtr1;
		int32_t L_2 = VuforiaNativeIosWrapper_dataSetGetNumTrackableType_m2199850757(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_DataSetGetTrackableName_m356984319 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, int32_t ___trackableId1, StringBuilder_t * ___trackableName2, int32_t ___nameMaxLength3, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		int32_t L_1 = ___trackableId1;
		StringBuilder_t * L_2 = ___trackableName2;
		int32_t L_3 = ___nameMaxLength3;
		int32_t L_4 = VuforiaNativeIosWrapper_dataSetGetTrackableName_m1401398735(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_DataSetGetTrackablesOfType_m737526614 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___trackableType0, intptr_t ___trackableDataArray1, int32_t ___trackableDataArrayLength2, intptr_t ___dataSetPtr3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___trackableType0;
		intptr_t L_1 = ___trackableDataArray1;
		int32_t L_2 = ___trackableDataArrayLength2;
		intptr_t L_3 = ___dataSetPtr3;
		int32_t L_4 = VuforiaNativeIosWrapper_dataSetGetTrackablesOfType_m2513650173(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DataSetLoad(System.String,System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_DataSetLoad_m3125195698 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___relativePath0, int32_t ___storageType1, intptr_t ___dataSetPtr2, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___relativePath0;
		int32_t L_1 = ___storageType1;
		intptr_t L_2 = ___dataSetPtr2;
		int32_t L_3 = VuforiaNativeIosWrapper_dataSetLoad_m436879107(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::DeinitFrameState(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_DeinitFrameState_m2470437443 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___frameState0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___frameState0;
		VuforiaNativeIosWrapper_deinitFrameState_m1882496214(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::Device_GetMode()
extern "C"  int32_t VuforiaNativeIosWrapper_Device_GetMode_m3093272190 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_device_GetMode_m2453552606(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::Device_GetViewerList()
extern "C"  intptr_t VuforiaNativeIosWrapper_Device_GetViewerList_m2065237817 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = VuforiaNativeIosWrapper_device_GetViewerList_m2362888643(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::Device_IsViewerPresent()
extern "C"  int32_t VuforiaNativeIosWrapper_Device_IsViewerPresent_m2863785011 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_device_IsViewerPresent_m1201399086(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::Device_SelectViewer(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_Device_SelectViewer_m2833055961 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___vp0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___vp0;
		int32_t L_1 = VuforiaNativeIosWrapper_device_SelectViewer_m1853041852(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::Device_SetMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_Device_SetMode_m3982781532 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		int32_t L_1 = VuforiaNativeIosWrapper_device_SetMode_m2286413161(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::Device_SetViewerPresent(System.Boolean)
extern "C"  void VuforiaNativeIosWrapper_Device_SetViewerPresent_m2503478090 (VuforiaNativeIosWrapper_t2037146173 * __this, bool ___present0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___present0;
		VuforiaNativeIosWrapper_device_SetViewerPresent_m2161993502(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::DeviceIsEyewearDevice()
extern "C"  int32_t VuforiaNativeIosWrapper_DeviceIsEyewearDevice_m3447119203 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_deviceIsEyewearDevice_m3851964756(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::EyewearDeviceGetScreenOrientation()
extern "C"  int32_t VuforiaNativeIosWrapper_EyewearDeviceGetScreenOrientation_m3465149969 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_eyewearDeviceGetScreenOrientation_m2250536774(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::EyewearDeviceIsDisplayExtended()
extern "C"  int32_t VuforiaNativeIosWrapper_EyewearDeviceIsDisplayExtended_m2812790290 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_eyewearDeviceIsDisplayExtended_m1602644497(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::EyewearDeviceIsDualDisplay()
extern "C"  int32_t VuforiaNativeIosWrapper_EyewearDeviceIsDualDisplay_m656986617 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_eyewearDeviceIsDualDisplay_m3124003728(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::EyewearDeviceIsSeeThru()
extern "C"  int32_t VuforiaNativeIosWrapper_EyewearDeviceIsSeeThru_m3887865171 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_eyewearDeviceIsSeeThru_m4200768691(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::EyewearDeviceSetDisplayExtended(System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_EyewearDeviceSetDisplayExtended_m3685077314 (VuforiaNativeIosWrapper_t2037146173 * __this, bool ___enable0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___enable0;
		int32_t L_1 = VuforiaNativeIosWrapper_eyewearDeviceSetDisplayExtended_m3471292286(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::GetProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_GetProjectionGL_m1681564090 (VuforiaNativeIosWrapper_t2037146173 * __this, float ___nearPlane0, float ___farPlane1, intptr_t ___projectionContainer2, int32_t ___screenOrientation3, const RuntimeMethod* method)
{
	{
		float L_0 = ___nearPlane0;
		float L_1 = ___farPlane1;
		intptr_t L_2 = ___projectionContainer2;
		int32_t L_3 = ___screenOrientation3;
		int32_t L_4 = VuforiaNativeIosWrapper_getProjectionGL_m427606978(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::HasSurfaceBeenRecreated()
extern "C"  int32_t VuforiaNativeIosWrapper_HasSurfaceBeenRecreated_m999732390 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_hasSurfaceBeenRecreated_m1823633968(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetBuilderBuild(System.String,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetBuilderBuild_m3901128401 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___name0, float ___screenSizeWidth1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___name0;
		float L_1 = ___screenSizeWidth1;
		int32_t L_2 = VuforiaNativeIosWrapper_imageTargetBuilderBuild_m3582937393(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetBuilderGetFrameQuality()
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetBuilderGetFrameQuality_m1970903018 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_imageTargetBuilderGetFrameQuality_m3184258030(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::ImageTargetBuilderGetTrackableSource()
extern "C"  intptr_t VuforiaNativeIosWrapper_ImageTargetBuilderGetTrackableSource_m2774982043 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = VuforiaNativeIosWrapper_imageTargetBuilderGetTrackableSource_m8878657(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::ImageTargetBuilderStartScan()
extern "C"  void VuforiaNativeIosWrapper_ImageTargetBuilderStartScan_m2413428929 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_imageTargetBuilderStartScan_m3177974730(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::ImageTargetBuilderStopScan()
extern "C"  void VuforiaNativeIosWrapper_ImageTargetBuilderStopScan_m2828255901 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_imageTargetBuilderStopScan_m4159555976(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetCreateVirtualButton_m2487967195 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, intptr_t ___rectData3, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		String_t* L_2 = ___virtualButtonName2;
		intptr_t L_3 = ___rectData3;
		int32_t L_4 = VuforiaNativeIosWrapper_imageTargetCreateVirtualButton_m4190927545(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetDestroyVirtualButton_m4196721382 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		String_t* L_2 = ___virtualButtonName2;
		int32_t L_3 = VuforiaNativeIosWrapper_imageTargetDestroyVirtualButton_m3151629955(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetGetNumVirtualButtons(System.IntPtr,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetGetNumVirtualButtons_m1917978756 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		int32_t L_2 = VuforiaNativeIosWrapper_imageTargetGetNumVirtualButtons_m3250531349(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetGetVirtualButtonName_m2392596223 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, int32_t ___idx2, StringBuilder_t * ___vbName3, int32_t ___nameMaxLength4, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		int32_t L_2 = ___idx2;
		StringBuilder_t * L_3 = ___vbName3;
		int32_t L_4 = ___nameMaxLength4;
		int32_t L_5 = VuforiaNativeIosWrapper_imageTargetGetVirtualButtonName_m1930708682(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ImageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_ImageTargetGetVirtualButtons_m1777365889 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___virtualButtonDataArray0, intptr_t ___rectangleDataArray1, int32_t ___virtualButtonDataArrayLength2, intptr_t ___dataSetPtr3, String_t* ___trackableName4, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___virtualButtonDataArray0;
		intptr_t L_1 = ___rectangleDataArray1;
		int32_t L_2 = ___virtualButtonDataArrayLength2;
		intptr_t L_3 = ___dataSetPtr3;
		String_t* L_4 = ___trackableName4;
		int32_t L_5 = VuforiaNativeIosWrapper_imageTargetGetVirtualButtons_m2274780740(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::InitFrameState(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_InitFrameState_m1221822175 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___frameState0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___frameState0;
		VuforiaNativeIosWrapper_initFrameState_m251019466(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::InitPlatformNative()
extern "C"  void VuforiaNativeIosWrapper_InitPlatformNative_m3892927554 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_initPlatformNative_m1148360790(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Vuforia.VuforiaNativeIosWrapper::MultiTargetGetLargestSizeComponent(System.IntPtr,System.String)
extern "C"  float VuforiaNativeIosWrapper_MultiTargetGetLargestSizeComponent_m1286554907 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		float L_2 = VuforiaNativeIosWrapper_multiTargetGetLargestSizeComponent_m877427687(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ObjectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ObjectTargetGetSize_m2126658495 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___sizePtr2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		intptr_t L_2 = ___sizePtr2;
		int32_t L_3 = VuforiaNativeIosWrapper_objectTargetGetSize_m2380033087(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ObjectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ObjectTargetSetSize_m2837456666 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___sizePtr2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		intptr_t L_2 = ___sizePtr2;
		int32_t L_3 = VuforiaNativeIosWrapper_objectTargetSetSize_m1443530746(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ObjectTrackerActivateDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ObjectTrackerActivateDataSet_m1077980848 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		int32_t L_1 = VuforiaNativeIosWrapper_objectTrackerActivateDataSet_m989691554(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::ObjectTrackerCreateDataSet()
extern "C"  intptr_t VuforiaNativeIosWrapper_ObjectTrackerCreateDataSet_m680874239 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = VuforiaNativeIosWrapper_objectTrackerCreateDataSet_m3029708900(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ObjectTrackerDeactivateDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ObjectTrackerDeactivateDataSet_m3593093630 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		int32_t L_1 = VuforiaNativeIosWrapper_objectTrackerDeactivateDataSet_m975093771(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ObjectTrackerDestroyDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ObjectTrackerDestroyDataSet_m2073345732 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		int32_t L_1 = VuforiaNativeIosWrapper_objectTrackerDestroyDataSet_m311987895(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::OnPause()
extern "C"  void VuforiaNativeIosWrapper_OnPause_m561968775 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_onPause_m2162095750(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::OnResume()
extern "C"  void VuforiaNativeIosWrapper_OnResume_m3859594913 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_onResume_m1164754594(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::OnSurfaceChanged(System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_OnSurfaceChanged_m788580119 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		VuforiaNativeIosWrapper_onSurfaceChanged_m3866066131(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::QcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_QcarAddCameraFrame_m2089452036 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___pixels0, int32_t ___width1, int32_t ___height2, int32_t ___format3, int32_t ___stride4, int32_t ___frameIdx5, int32_t ___flipHorizontally6, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___pixels0;
		int32_t L_1 = ___width1;
		int32_t L_2 = ___height2;
		int32_t L_3 = ___format3;
		int32_t L_4 = ___stride4;
		int32_t L_5 = ___frameIdx5;
		int32_t L_6 = ___flipHorizontally6;
		VuforiaNativeIosWrapper_qcarAddCameraFrame_m3321820358(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::QcarDeinit()
extern "C"  void VuforiaNativeIosWrapper_QcarDeinit_m2683752990 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_qcarDeinit_m4212576879(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_QcarGetBufferSize_m2759899220 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		int32_t L_3 = VuforiaNativeIosWrapper_qcarGetBufferSize_m3158994288(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::QcarInit(System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_QcarInit_m3580514854 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___licenseKey0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___licenseKey0;
		int32_t L_1 = VuforiaNativeIosWrapper_qcarInit_m496390534(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::QcarSetFrameFormat(System.Int32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_QcarSetFrameFormat_m946773674 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___format0, int32_t ___enabled1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___format0;
		int32_t L_1 = ___enabled1;
		int32_t L_2 = VuforiaNativeIosWrapper_qcarSetFrameFormat_m4090591613(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::QcarSetHint(System.UInt32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_QcarSetHint_m1144128337 (VuforiaNativeIosWrapper_t2037146173 * __this, uint32_t ___hint0, int32_t ___value1, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___hint0;
		int32_t L_1 = ___value1;
		int32_t L_2 = VuforiaNativeIosWrapper_qcarSetHint_m2555757340(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ReconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_ReconstructionFromTargetSetInitializationTarget_m3832893713 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___reconstruction0, intptr_t ___dataSetPtr1, int32_t ___trackableID2, intptr_t ___occluderMin3, intptr_t ___occluderMax4, intptr_t ___offsetToOccluder5, intptr_t ___rotationAxisToOccluder6, float ___rotationAngleToOccluder7, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___reconstruction0;
		intptr_t L_1 = ___dataSetPtr1;
		int32_t L_2 = ___trackableID2;
		intptr_t L_3 = ___occluderMin3;
		intptr_t L_4 = ___occluderMax4;
		intptr_t L_5 = ___offsetToOccluder5;
		intptr_t L_6 = ___rotationAxisToOccluder6;
		float L_7 = ___rotationAngleToOccluder7;
		int32_t L_8 = VuforiaNativeIosWrapper_reconstructionFromTargetSetInitializationTarget_m1740495597(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ReconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ReconstructionSetMaximumArea_m778266012 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___reconstruction0, intptr_t ___maximumArea1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___reconstruction0;
		intptr_t L_1 = ___maximumArea1;
		int32_t L_2 = VuforiaNativeIosWrapper_reconstructionSetMaximumArea_m3961464079(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::ReconstructionSetNavMeshPadding(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_ReconstructionSetNavMeshPadding_m604849401 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___reconstruction0, float ___padding1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___reconstruction0;
		float L_1 = ___padding1;
		VuforiaNativeIosWrapper_reconstructionSetNavMeshPadding_m881105308(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ReconstructionStart(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ReconstructionStart_m1555602029 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___reconstruction0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___reconstruction0;
		int32_t L_1 = VuforiaNativeIosWrapper_reconstructionStart_m4250848222(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::RendererCreateNativeTexture(System.UInt32,System.UInt32,System.Int32)
extern "C"  intptr_t VuforiaNativeIosWrapper_RendererCreateNativeTexture_m1030779038 (VuforiaNativeIosWrapper_t2037146173 * __this, uint32_t ___width0, uint32_t ___height1, int32_t ___format2, const RuntimeMethod* method)
{
	{
		uint32_t L_0 = ___width0;
		uint32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		intptr_t L_3 = VuforiaNativeIosWrapper_rendererCreateNativeTexture_m1694208006(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RendererGetGraphicsAPI()
extern "C"  int32_t VuforiaNativeIosWrapper_RendererGetGraphicsAPI_m2918224611 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_rendererGetGraphicsAPI_m3560655757(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RendererGetVideoBackgroundCfg(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RendererGetVideoBackgroundCfg_m2666850021 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___bgCfg0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___bgCfg0;
		VuforiaNativeIosWrapper_rendererGetVideoBackgroundCfg_m1678358512(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RendererGetVideoBackgroundTextureInfo(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_RendererGetVideoBackgroundTextureInfo_m1427462542 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___texInfo0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___texInfo0;
		int32_t L_1 = VuforiaNativeIosWrapper_rendererGetVideoBackgroundTextureInfo_m2700164971(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RendererIsVideoBackgroundTextureInfoAvailable()
extern "C"  int32_t VuforiaNativeIosWrapper_RendererIsVideoBackgroundTextureInfoAvailable_m1356118522 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m1274323877(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RendererSetVideoBackgroundCfg(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RendererSetVideoBackgroundCfg_m82208406 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___bgCfg0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___bgCfg0;
		VuforiaNativeIosWrapper_rendererSetVideoBackgroundCfg_m1972712013(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RendererSetVideoBackgroundTextureID(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_RendererSetVideoBackgroundTextureID_m1827700617 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___textureID0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___textureID0;
		int32_t L_1 = VuforiaNativeIosWrapper_rendererSetVideoBackgroundTextureID_m1187272398(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RendererSetVideoBackgroundTexturePtr(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_RendererSetVideoBackgroundTexturePtr_m3936279920 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___texturePtr0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___texturePtr0;
		int32_t L_1 = VuforiaNativeIosWrapper_rendererSetVideoBackgroundTexturePtr_m3392285240(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_DeleteCopy()
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_DeleteCopy_m3042743587 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		VuforiaNativeIosWrapper_renderingPrimitives_DeleteCopy_m535892042(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetDistortionMesh(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetDistortionMesh_m2879554459 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewId0, intptr_t ___meshData1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewId0;
		intptr_t L_1 = ___meshData1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetDistortionMesh_m399350896(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetDistortionMeshSize(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetDistortionMeshSize_m1609997212 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewId0, intptr_t ___size1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewId0;
		intptr_t L_1 = ___size1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetDistortionMeshSize_m3433711271(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetEffectiveFov(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetEffectiveFov_m3687069950 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewID0, intptr_t ___fovContainer1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewID0;
		intptr_t L_1 = ___fovContainer1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetEffectiveFov_m1766427464(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetEyeDisplayAdjustmentMatrix(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetEyeDisplayAdjustmentMatrix_m2509946407 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewID0, intptr_t ___matrixContainer1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewID0;
		intptr_t L_1 = ___matrixContainer1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetEyeDisplayAdjustmentMatrix_m1813656632(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetNormalizedViewport(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetNormalizedViewport_m2581911154 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewID0, intptr_t ___viewportContainer1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewID0;
		intptr_t L_1 = ___viewportContainer1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetNormalizedViewport_m1528191784(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetProjectionMatrix(System.Int32,System.Single,System.Single,System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetProjectionMatrix_m3866935781 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewID0, float ___near1, float ___far2, intptr_t ___projectionContainer3, int32_t ___screenOrientation4, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewID0;
		float L_1 = ___near1;
		float L_2 = ___far2;
		intptr_t L_3 = ___projectionContainer3;
		int32_t L_4 = ___screenOrientation4;
		VuforiaNativeIosWrapper_renderingPrimitives_GetProjectionMatrix_m1357884672(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetViewport(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetViewport_m234227759 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewID0, intptr_t ___viewportContainer1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewID0;
		intptr_t L_1 = ___viewportContainer1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetViewport_m2833809889(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RenderingPrimitives_GetViewportCentreToEyeAxis(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RenderingPrimitives_GetViewportCentreToEyeAxis_m702093804 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___viewID0, intptr_t ___vectorContainer1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___viewID0;
		intptr_t L_1 = ___vectorContainer1;
		VuforiaNativeIosWrapper_renderingPrimitives_GetViewportCentreToEyeAxis_m2133419223(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::RotationalDeviceTracker_GetModelCorrectionTransform(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_RotationalDeviceTracker_GetModelCorrectionTransform_m1695454046 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___pivot0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___pivot0;
		VuforiaNativeIosWrapper_rotationalDeviceTracker_GetModelCorrectionTransform_m4116192559(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RotationalDeviceTracker_Recenter()
extern "C"  int32_t VuforiaNativeIosWrapper_RotationalDeviceTracker_Recenter_m1585957567 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_rotationalDeviceTracker_Recenter_m2292918386(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RotationalDeviceTracker_SetModelCorrectionMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_RotationalDeviceTracker_SetModelCorrectionMode_m2227899018 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		int32_t L_1 = VuforiaNativeIosWrapper_rotationalDeviceTracker_SetModelCorrectionMode_m3760711071(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RotationalDeviceTracker_SetModelCorrectionModeWithTransform(System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_RotationalDeviceTracker_SetModelCorrectionModeWithTransform_m1375928298 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___mode0, intptr_t ___pivot1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		intptr_t L_1 = ___pivot1;
		int32_t L_2 = VuforiaNativeIosWrapper_rotationalDeviceTracker_SetModelCorrectionModeWithTransform_m1499777731(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::RotationalDeviceTracker_SetPosePrediction(System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_RotationalDeviceTracker_SetPosePrediction_m516171761 (VuforiaNativeIosWrapper_t2037146173 * __this, bool ___mode0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___mode0;
		int32_t L_1 = VuforiaNativeIosWrapper_rotationalDeviceTracker_SetPosePrediction_m3158892482(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::SetApplicationEnvironment(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_SetApplicationEnvironment_m3324350816 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___unityVersionMajor0, int32_t ___unityVersionMinor1, int32_t ___unityVersionChange2, int32_t ___sdkWrapperType3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___unityVersionMajor0;
		int32_t L_1 = ___unityVersionMinor1;
		int32_t L_2 = ___unityVersionChange2;
		int32_t L_3 = ___sdkWrapperType3;
		VuforiaNativeIosWrapper_setApplicationEnvironment_m86779826(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::SetRenderBuffers(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_SetRenderBuffers_m2393362058 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___colorBuffer0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___colorBuffer0;
		VuforiaNativeIosWrapper_setRenderBuffers_m3774876697(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::SmartTerrainBuilderAddReconstruction(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_SmartTerrainBuilderAddReconstruction_m2972237459 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___reconstruction0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___reconstruction0;
		int32_t L_1 = VuforiaNativeIosWrapper_smartTerrainBuilderAddReconstruction_m1293846854(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::SmartTerrainBuilderCreateReconstructionFromTarget()
extern "C"  intptr_t VuforiaNativeIosWrapper_SmartTerrainBuilderCreateReconstructionFromTarget_m3862267680 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = VuforiaNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3676350375(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::SmartTerrainBuilderRemoveReconstruction(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_SmartTerrainBuilderRemoveReconstruction_m1673677821 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___reconstruction0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___reconstruction0;
		int32_t L_1 = VuforiaNativeIosWrapper_smartTerrainBuilderRemoveReconstruction_m1763309370(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::SmartTerrainTrackerDeinitBuilder()
extern "C"  int32_t VuforiaNativeIosWrapper_SmartTerrainTrackerDeinitBuilder_m2473749581 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_smartTerrainTrackerDeinitBuilder_m2292570849(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::SmartTerrainTrackerInitBuilder()
extern "C"  int32_t VuforiaNativeIosWrapper_SmartTerrainTrackerInitBuilder_m1705183433 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_smartTerrainTrackerInitBuilder_m3524084684(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::SmartTerrainTrackerSetScaleToMillimeter(System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_SmartTerrainTrackerSetScaleToMillimeter_m3653778281 (VuforiaNativeIosWrapper_t2037146173 * __this, float ___scaleToMillimenters0, const RuntimeMethod* method)
{
	{
		float L_0 = ___scaleToMillimenters0;
		int32_t L_1 = VuforiaNativeIosWrapper_smartTerrainTrackerSetScaleToMillimeter_m563366966(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::StartExtendedTracking(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_StartExtendedTracking_m370495810 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, int32_t ___trackableID1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		int32_t L_1 = ___trackableID1;
		int32_t L_2 = VuforiaNativeIosWrapper_startExtendedTracking_m357143318(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TargetFinderDeinit()
extern "C"  int32_t VuforiaNativeIosWrapper_TargetFinderDeinit_m1040573152 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_targetFinderDeinit_m1075093056(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TargetFinderGetInitState()
extern "C"  int32_t VuforiaNativeIosWrapper_TargetFinderGetInitState_m687678161 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_targetFinderGetInitState_m2122533097(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TargetFinderGetResults(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_TargetFinderGetResults_m131403001 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___searchResultArray0, int32_t ___searchResultArrayLength1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___searchResultArray0;
		int32_t L_1 = ___searchResultArrayLength1;
		int32_t L_2 = VuforiaNativeIosWrapper_targetFinderGetResults_m29416918(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TargetFinderStartInit(System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_TargetFinderStartInit_m1635056013 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___userKey0, String_t* ___secretKey1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___userKey0;
		String_t* L_1 = ___secretKey1;
		int32_t L_2 = VuforiaNativeIosWrapper_targetFinderStartInit_m282508(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TargetFinderStartRecognition()
extern "C"  int32_t VuforiaNativeIosWrapper_TargetFinderStartRecognition_m2920150140 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_targetFinderStartRecognition_m2312748431(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TargetFinderStop()
extern "C"  int32_t VuforiaNativeIosWrapper_TargetFinderStop_m4179280393 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_targetFinderStop_m1766141929(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::TargetFinderUpdate(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_TargetFinderUpdate_m2801461324 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___targetFinderState0, int32_t ___filterMode1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___targetFinderState0;
		int32_t L_1 = ___filterMode1;
		VuforiaNativeIosWrapper_targetFinderUpdate_m307650428(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TrackerManagerDeinitTracker(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_TrackerManagerDeinitTracker_m3724501654 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___trackerTypeID0;
		int32_t L_1 = VuforiaNativeIosWrapper_trackerManagerDeinitTracker_m862961198(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TrackerManagerInitTracker(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_TrackerManagerInitTracker_m2743846525 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___trackerTypeID0;
		int32_t L_1 = VuforiaNativeIosWrapper_trackerManagerInitTracker_m3155645533(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::TrackerStart(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_TrackerStart_m2322246777 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___trackerTypeID0;
		int32_t L_1 = VuforiaNativeIosWrapper_trackerStart_m3650945244(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::TrackerStop(System.Int32)
extern "C"  void VuforiaNativeIosWrapper_TrackerStop_m2684670634 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___trackerTypeID0;
		VuforiaNativeIosWrapper_trackerStop_m136099586(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::UpdateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_UpdateQCAR_m2855628656 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___imageHeaderDataArray0, int32_t ___imageHeaderArrayLength1, intptr_t ___frameState2, int32_t ___screenOrientation3, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___imageHeaderDataArray0;
		int32_t L_1 = ___imageHeaderArrayLength1;
		intptr_t L_2 = ___frameState2;
		int32_t L_3 = ___screenOrientation3;
		int32_t L_4 = VuforiaNativeIosWrapper_updateQCAR_m3823614422(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::ViewerParameters_delete(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_ViewerParameters_delete_m1239969598 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___obj0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___obj0;
		VuforiaNativeIosWrapper_viewerParameters_delete_m1022060148(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::ViewerParametersList_GetByIndex(System.IntPtr,System.Int32)
extern "C"  intptr_t VuforiaNativeIosWrapper_ViewerParametersList_GetByIndex_m4248105032 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___vpList0, int32_t ___idx1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___vpList0;
		int32_t L_1 = ___idx1;
		intptr_t L_2 = VuforiaNativeIosWrapper_viewerParametersList_GetByIndex_m1568290052(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::ViewerParametersList_GetByNameManufacturer(System.IntPtr,System.String,System.String)
extern "C"  intptr_t VuforiaNativeIosWrapper_ViewerParametersList_GetByNameManufacturer_m686218173 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___vpList0, String_t* ___name1, String_t* ___manufacturer2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___vpList0;
		String_t* L_1 = ___name1;
		String_t* L_2 = ___manufacturer2;
		intptr_t L_3 = VuforiaNativeIosWrapper_viewerParametersList_GetByNameManufacturer_m2600346792(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void Vuforia.VuforiaNativeIosWrapper::ViewerParametersList_SetSDKFilter(System.IntPtr,System.String)
extern "C"  void VuforiaNativeIosWrapper_ViewerParametersList_SetSDKFilter_m1789691188 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___vpList0, String_t* ___filter1, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___vpList0;
		String_t* L_1 = ___filter1;
		VuforiaNativeIosWrapper_viewerParametersList_SetSDKFilter_m1101420845(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::ViewerParametersList_Size(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_ViewerParametersList_Size_m235887603 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___vpList0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___vpList0;
		int32_t L_1 = VuforiaNativeIosWrapper_viewerParametersList_Size_m3374613508(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::VirtualButtonGetId(System.IntPtr,System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_VirtualButtonGetId_m2033696034 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		String_t* L_2 = ___virtualButtonName2;
		int32_t L_3 = VuforiaNativeIosWrapper_virtualButtonGetId_m2887502506(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::VirtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_VirtualButtonSetAreaRectangle_m1807331640 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, intptr_t ___rectData3, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		String_t* L_2 = ___virtualButtonName2;
		intptr_t L_3 = ___rectData3;
		int32_t L_4 = VuforiaNativeIosWrapper_virtualButtonSetAreaRectangle_m3959624685(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::VirtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_VirtualButtonSetEnabled_m3220145831 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, int32_t ___enabled3, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		String_t* L_2 = ___virtualButtonName2;
		int32_t L_3 = ___enabled3;
		int32_t L_4 = VuforiaNativeIosWrapper_virtualButtonSetEnabled_m4103479847(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::VirtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_VirtualButtonSetSensitivity_m3118570938 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, int32_t ___sensitivity3, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		String_t* L_2 = ___virtualButtonName2;
		int32_t L_3 = ___sensitivity3;
		int32_t L_4 = VuforiaNativeIosWrapper_virtualButtonSetSensitivity_m862172097(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::VuforiaGetRenderEventCallback()
extern "C"  intptr_t VuforiaNativeIosWrapper_VuforiaGetRenderEventCallback_m2483183263 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = VuforiaNativeIosWrapper_vuforiaGetRenderEventCallback_m3112993526(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::VuMarkTemplateGetOrigin(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_VuMarkTemplateGetOrigin_m2778980958 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___originPtr2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		intptr_t L_2 = ___originPtr2;
		int32_t L_3 = VuforiaNativeIosWrapper_vuMarkTemplateGetOrigin_m917331585(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::VuMarkTemplateSetTrackingFromRuntimeAppearance(System.IntPtr,System.String,System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_VuMarkTemplateSetTrackingFromRuntimeAppearance_m686380294 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___dataSetPtr0, String_t* ___trackableName1, bool ___enable2, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___dataSetPtr0;
		String_t* L_1 = ___trackableName1;
		bool L_2 = ___enable2;
		int32_t L_3 = VuforiaNativeIosWrapper_vuMarkTemplateSetTrackingFromRuntimeAppearance_m3954844824(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListAddWordsFromFile(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_WordListAddWordsFromFile_m2806219794 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		int32_t L_1 = ___storageType1;
		int32_t L_2 = VuforiaNativeIosWrapper_wordListAddWordsFromFile_m2691145391(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListAddWordToFilterListU(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_WordListAddWordToFilterListU_m973885060 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___word0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___word0;
		int32_t L_1 = VuforiaNativeIosWrapper_wordListAddWordToFilterListU_m944574365(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListAddWordU(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_WordListAddWordU_m597188736 (VuforiaNativeIosWrapper_t2037146173 * __this, intptr_t ___word0, const RuntimeMethod* method)
{
	{
		intptr_t L_0 = ___word0;
		int32_t L_1 = VuforiaNativeIosWrapper_wordListAddWordU_m3831652562(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListLoadFilterList(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_WordListLoadFilterList_m2725923679 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		int32_t L_1 = ___storageType1;
		int32_t L_2 = VuforiaNativeIosWrapper_wordListLoadFilterList_m604465529(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListLoadWordList(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_WordListLoadWordList_m3583647471 (VuforiaNativeIosWrapper_t2037146173 * __this, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___path0;
		int32_t L_1 = ___storageType1;
		int32_t L_2 = VuforiaNativeIosWrapper_wordListLoadWordList_m3605546583(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListSetFilterMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_WordListSetFilterMode_m2689833857 (VuforiaNativeIosWrapper_t2037146173 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		int32_t L_1 = VuforiaNativeIosWrapper_wordListSetFilterMode_m2109095690(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 Vuforia.VuforiaNativeIosWrapper::WordListUnloadAllLists()
extern "C"  int32_t VuforiaNativeIosWrapper_WordListUnloadAllLists_m2532236860 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = VuforiaNativeIosWrapper_wordListUnloadAllLists_m316317724(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
extern "C" int32_t DEFAULT_CALL cameraDeviceDeinitCamera();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceDeinitCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceDeinitCamera_m1114664889 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceDeinitCamera)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL cameraDeviceGetCameraDirection();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceGetCameraDirection()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceGetCameraDirection_m432955548 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceGetCameraDirection)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL cameraDeviceGetCameraFieldOfViewRads(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceGetCameraFieldOfViewRads(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceGetCameraFieldOfViewRads_m2397779624 (RuntimeObject * __this /* static, unused */, intptr_t ___fovVectorContainer0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceGetCameraFieldOfViewRads)(___fovVectorContainer0);

	return returnValue;
}
extern "C" void DEFAULT_CALL cameraDeviceGetVideoMode(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::cameraDeviceGetVideoMode(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_cameraDeviceGetVideoMode_m3934990628 (RuntimeObject * __this /* static, unused */, int32_t ___idx0, intptr_t ___videoMode1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(cameraDeviceGetVideoMode)(___idx0, ___videoMode1);

}
extern "C" int32_t DEFAULT_CALL cameraDeviceInitCamera(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceInitCamera(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceInitCamera_m1726182893 (RuntimeObject * __this /* static, unused */, int32_t ___camera0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceInitCamera)(___camera0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL cameraDeviceSelectVideoMode(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceSelectVideoMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceSelectVideoMode_m3476877314 (RuntimeObject * __this /* static, unused */, int32_t ___idx0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceSelectVideoMode)(___idx0);

	return returnValue;
}
extern "C" void DEFAULT_CALL cameraDeviceSetCameraConfiguration(int32_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::cameraDeviceSetCameraConfiguration(System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_cameraDeviceSetCameraConfiguration_m3550565238 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(cameraDeviceSetCameraConfiguration)(___width0, ___height1);

}
extern "C" int32_t DEFAULT_CALL cameraDeviceStartCamera();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceStartCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceStartCamera_m813609651 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceStartCamera)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL cameraDeviceStopCamera();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cameraDeviceStopCamera()
extern "C"  int32_t VuforiaNativeIosWrapper_cameraDeviceStopCamera_m2155385929 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cameraDeviceStopCamera)();

	return returnValue;
}
extern "C" void DEFAULT_CALL customViewerParameters_AddDistortionCoefficient(intptr_t, float);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_AddDistortionCoefficient(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_AddDistortionCoefficient_m1019494423 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_AddDistortionCoefficient)(___obj0, ___val1);

}
extern "C" void DEFAULT_CALL customViewerParameters_delete(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_delete(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_delete_m2548462463 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_delete)(___obj0);

}
extern "C" intptr_t DEFAULT_CALL customViewerParameters_new(float, char*, char*);
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::customViewerParameters_new(System.Single,System.String,System.String)
extern "C"  intptr_t VuforiaNativeIosWrapper_customViewerParameters_new_m3376464988 (RuntimeObject * __this /* static, unused */, float ___version0, String_t* ___name1, String_t* ___manufacturer2, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (float, char*, char*);

	// Marshaling of parameter '___name1' to native representation
	char* ____name1_marshaled = NULL;
	____name1_marshaled = il2cpp_codegen_marshal_string(___name1);

	// Marshaling of parameter '___manufacturer2' to native representation
	char* ____manufacturer2_marshaled = NULL;
	____manufacturer2_marshaled = il2cpp_codegen_marshal_string(___manufacturer2);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(customViewerParameters_new)(___version0, ____name1_marshaled, ____manufacturer2_marshaled);

	// Marshaling cleanup of parameter '___name1' native representation
	il2cpp_codegen_marshal_free(____name1_marshaled);
	____name1_marshaled = NULL;

	// Marshaling cleanup of parameter '___manufacturer2' native representation
	il2cpp_codegen_marshal_free(____manufacturer2_marshaled);
	____manufacturer2_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL customViewerParameters_SetButtonType(intptr_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetButtonType(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetButtonType_m3745586757 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, int32_t ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetButtonType)(___obj0, ___val1);

}
extern "C" void DEFAULT_CALL customViewerParameters_SetContainsMagnet(intptr_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetContainsMagnet(System.IntPtr,System.Boolean)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetContainsMagnet_m334534216 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, bool ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetContainsMagnet)(___obj0, static_cast<int32_t>(___val1));

}
extern "C" void DEFAULT_CALL customViewerParameters_SetFieldOfView(intptr_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetFieldOfView(System.IntPtr,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetFieldOfView_m3080117215 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, intptr_t ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetFieldOfView)(___obj0, ___val1);

}
extern "C" void DEFAULT_CALL customViewerParameters_SetInterLensDistance(intptr_t, float);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetInterLensDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetInterLensDistance_m1494617937 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetInterLensDistance)(___obj0, ___val1);

}
extern "C" void DEFAULT_CALL customViewerParameters_SetLensCentreToTrayDistance(intptr_t, float);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetLensCentreToTrayDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetLensCentreToTrayDistance_m2220789831 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetLensCentreToTrayDistance)(___obj0, ___val1);

}
extern "C" void DEFAULT_CALL customViewerParameters_SetScreenToLensDistance(intptr_t, float);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetScreenToLensDistance(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetScreenToLensDistance_m2540654929 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, float ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetScreenToLensDistance)(___obj0, ___val1);

}
extern "C" void DEFAULT_CALL customViewerParameters_SetTrayAlignment(intptr_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::customViewerParameters_SetTrayAlignment(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_customViewerParameters_SetTrayAlignment_m1020920963 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, int32_t ___val1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(customViewerParameters_SetTrayAlignment)(___obj0, ___val1);

}
extern "C" int32_t DEFAULT_CALL cylinderTargetGetDimensions(intptr_t, char*, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cylinderTargetGetDimensions(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_cylinderTargetGetDimensions_m2170405224 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___dimensionPtr2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, intptr_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cylinderTargetGetDimensions)(___dataSetPtr0, ____trackableName1_marshaled, ___dimensionPtr2);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL cylinderTargetSetSideLength(intptr_t, char*, float);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::cylinderTargetSetSideLength(System.IntPtr,System.String,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_cylinderTargetSetSideLength_m1744635518 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, float ___sideLength2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, float);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(cylinderTargetSetSideLength)(___dataSetPtr0, ____trackableName1_marshaled, ___sideLength2);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL dataSetDestroyTrackable(intptr_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetDestroyTrackable(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetDestroyTrackable_m3399286625 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, int32_t ___trackableId1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(dataSetDestroyTrackable)(___dataSetPtr0, ___trackableId1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL dataSetExists(char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetExists(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetExists_m835795820 (RuntimeObject * __this /* static, unused */, String_t* ___relativePath0, int32_t ___storageType1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___relativePath0' to native representation
	char* ____relativePath0_marshaled = NULL;
	____relativePath0_marshaled = il2cpp_codegen_marshal_string(___relativePath0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(dataSetExists)(____relativePath0_marshaled, ___storageType1);

	// Marshaling cleanup of parameter '___relativePath0' native representation
	il2cpp_codegen_marshal_free(____relativePath0_marshaled);
	____relativePath0_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL dataSetGetNumTrackableType(int32_t, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetGetNumTrackableType(System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetGetNumTrackableType_m2199850757 (RuntimeObject * __this /* static, unused */, int32_t ___trackableType0, intptr_t ___dataSetPtr1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(dataSetGetNumTrackableType)(___trackableType0, ___dataSetPtr1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL dataSetGetTrackableName(intptr_t, int32_t, char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetGetTrackableName(System.IntPtr,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetGetTrackableName_m1401398735 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, int32_t ___trackableId1, StringBuilder_t * ___trackableName2, int32_t ___nameMaxLength3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, char*, int32_t);

	// Marshaling of parameter '___trackableName2' to native representation
	char* ____trackableName2_marshaled = NULL;
	____trackableName2_marshaled = il2cpp_codegen_marshal_string_builder(___trackableName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(dataSetGetTrackableName)(___dataSetPtr0, ___trackableId1, ____trackableName2_marshaled, ___nameMaxLength3);

	// Marshaling of parameter '___trackableName2' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___trackableName2, ____trackableName2_marshaled);

	// Marshaling cleanup of parameter '___trackableName2' native representation
	il2cpp_codegen_marshal_free(____trackableName2_marshaled);
	____trackableName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL dataSetGetTrackablesOfType(int32_t, intptr_t, int32_t, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetGetTrackablesOfType(System.Int32,System.IntPtr,System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetGetTrackablesOfType_m2513650173 (RuntimeObject * __this /* static, unused */, int32_t ___trackableType0, intptr_t ___trackableDataArray1, int32_t ___trackableDataArrayLength2, intptr_t ___dataSetPtr3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t, int32_t, intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(dataSetGetTrackablesOfType)(___trackableType0, ___trackableDataArray1, ___trackableDataArrayLength2, ___dataSetPtr3);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL dataSetLoad(char*, int32_t, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::dataSetLoad(System.String,System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_dataSetLoad_m436879107 (RuntimeObject * __this /* static, unused */, String_t* ___relativePath0, int32_t ___storageType1, intptr_t ___dataSetPtr2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, intptr_t);

	// Marshaling of parameter '___relativePath0' to native representation
	char* ____relativePath0_marshaled = NULL;
	____relativePath0_marshaled = il2cpp_codegen_marshal_string(___relativePath0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(dataSetLoad)(____relativePath0_marshaled, ___storageType1, ___dataSetPtr2);

	// Marshaling cleanup of parameter '___relativePath0' native representation
	il2cpp_codegen_marshal_free(____relativePath0_marshaled);
	____relativePath0_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL deinitFrameState(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::deinitFrameState(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_deinitFrameState_m1882496214 (RuntimeObject * __this /* static, unused */, intptr_t ___frameState0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(deinitFrameState)(___frameState0);

}
extern "C" int32_t DEFAULT_CALL device_GetMode();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_GetMode()
extern "C"  int32_t VuforiaNativeIosWrapper_device_GetMode_m2453552606 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(device_GetMode)();

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL device_GetViewerList();
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::device_GetViewerList()
extern "C"  intptr_t VuforiaNativeIosWrapper_device_GetViewerList_m2362888643 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(device_GetViewerList)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL device_IsViewerPresent();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_IsViewerPresent()
extern "C"  int32_t VuforiaNativeIosWrapper_device_IsViewerPresent_m1201399086 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(device_IsViewerPresent)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL device_SelectViewer(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_SelectViewer(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_device_SelectViewer_m1853041852 (RuntimeObject * __this /* static, unused */, intptr_t ___vp0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(device_SelectViewer)(___vp0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL device_SetMode(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::device_SetMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_device_SetMode_m2286413161 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(device_SetMode)(___mode0);

	return returnValue;
}
extern "C" void DEFAULT_CALL device_SetViewerPresent(int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::device_SetViewerPresent(System.Boolean)
extern "C"  void VuforiaNativeIosWrapper_device_SetViewerPresent_m2161993502 (RuntimeObject * __this /* static, unused */, bool ___present0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(device_SetViewerPresent)(static_cast<int32_t>(___present0));

}
extern "C" int32_t DEFAULT_CALL deviceIsEyewearDevice();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::deviceIsEyewearDevice()
extern "C"  int32_t VuforiaNativeIosWrapper_deviceIsEyewearDevice_m3851964756 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(deviceIsEyewearDevice)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL eyewearDeviceGetScreenOrientation();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceGetScreenOrientation()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceGetScreenOrientation_m2250536774 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(eyewearDeviceGetScreenOrientation)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL eyewearDeviceIsDisplayExtended();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceIsDisplayExtended()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceIsDisplayExtended_m1602644497 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(eyewearDeviceIsDisplayExtended)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL eyewearDeviceIsDualDisplay();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceIsDualDisplay()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceIsDualDisplay_m3124003728 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(eyewearDeviceIsDualDisplay)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL eyewearDeviceIsSeeThru();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceIsSeeThru()
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceIsSeeThru_m4200768691 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(eyewearDeviceIsSeeThru)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL eyewearDeviceSetDisplayExtended(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::eyewearDeviceSetDisplayExtended(System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_eyewearDeviceSetDisplayExtended_m3471292286 (RuntimeObject * __this /* static, unused */, bool ___enable0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(eyewearDeviceSetDisplayExtended)(static_cast<int32_t>(___enable0));

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL getProjectionGL(float, float, intptr_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::getProjectionGL(System.Single,System.Single,System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_getProjectionGL_m427606978 (RuntimeObject * __this /* static, unused */, float ___nearPlane0, float ___farPlane1, intptr_t ___projectionContainer2, int32_t ___screenOrientation3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (float, float, intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(getProjectionGL)(___nearPlane0, ___farPlane1, ___projectionContainer2, ___screenOrientation3);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL hasSurfaceBeenRecreated();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::hasSurfaceBeenRecreated()
extern "C"  int32_t VuforiaNativeIosWrapper_hasSurfaceBeenRecreated_m1823633968 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(hasSurfaceBeenRecreated)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL imageTargetBuilderBuild(char*, float);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderBuild(System.String,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetBuilderBuild_m3582937393 (RuntimeObject * __this /* static, unused */, String_t* ___name0, float ___screenSizeWidth1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, float);

	// Marshaling of parameter '___name0' to native representation
	char* ____name0_marshaled = NULL;
	____name0_marshaled = il2cpp_codegen_marshal_string(___name0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetBuilderBuild)(____name0_marshaled, ___screenSizeWidth1);

	// Marshaling cleanup of parameter '___name0' native representation
	il2cpp_codegen_marshal_free(____name0_marshaled);
	____name0_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL imageTargetBuilderGetFrameQuality();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderGetFrameQuality()
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetBuilderGetFrameQuality_m3184258030 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetBuilderGetFrameQuality)();

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL imageTargetBuilderGetTrackableSource();
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderGetTrackableSource()
extern "C"  intptr_t VuforiaNativeIosWrapper_imageTargetBuilderGetTrackableSource_m8878657 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetBuilderGetTrackableSource)();

	return returnValue;
}
extern "C" void DEFAULT_CALL imageTargetBuilderStartScan();
// System.Void Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderStartScan()
extern "C"  void VuforiaNativeIosWrapper_imageTargetBuilderStartScan_m3177974730 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(imageTargetBuilderStartScan)();

}
extern "C" void DEFAULT_CALL imageTargetBuilderStopScan();
// System.Void Vuforia.VuforiaNativeIosWrapper::imageTargetBuilderStopScan()
extern "C"  void VuforiaNativeIosWrapper_imageTargetBuilderStopScan_m4159555976 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(imageTargetBuilderStopScan)();

}
extern "C" int32_t DEFAULT_CALL imageTargetCreateVirtualButton(intptr_t, char*, char*, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetCreateVirtualButton(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetCreateVirtualButton_m4190927545 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, intptr_t ___rectData3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*, intptr_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___virtualButtonName2' to native representation
	char* ____virtualButtonName2_marshaled = NULL;
	____virtualButtonName2_marshaled = il2cpp_codegen_marshal_string(___virtualButtonName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetCreateVirtualButton)(___dataSetPtr0, ____trackableName1_marshaled, ____virtualButtonName2_marshaled, ___rectData3);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___virtualButtonName2' native representation
	il2cpp_codegen_marshal_free(____virtualButtonName2_marshaled);
	____virtualButtonName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL imageTargetDestroyVirtualButton(intptr_t, char*, char*);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetDestroyVirtualButton(System.IntPtr,System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetDestroyVirtualButton_m3151629955 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___virtualButtonName2' to native representation
	char* ____virtualButtonName2_marshaled = NULL;
	____virtualButtonName2_marshaled = il2cpp_codegen_marshal_string(___virtualButtonName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetDestroyVirtualButton)(___dataSetPtr0, ____trackableName1_marshaled, ____virtualButtonName2_marshaled);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___virtualButtonName2' native representation
	il2cpp_codegen_marshal_free(____virtualButtonName2_marshaled);
	____virtualButtonName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL imageTargetGetNumVirtualButtons(intptr_t, char*);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetGetNumVirtualButtons(System.IntPtr,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetGetNumVirtualButtons_m3250531349 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetGetNumVirtualButtons)(___dataSetPtr0, ____trackableName1_marshaled);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL imageTargetGetVirtualButtonName(intptr_t, char*, int32_t, char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetGetVirtualButtonName(System.IntPtr,System.String,System.Int32,System.Text.StringBuilder,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetGetVirtualButtonName_m1930708682 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, int32_t ___idx2, StringBuilder_t * ___vbName3, int32_t ___nameMaxLength4, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, int32_t, char*, int32_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___vbName3' to native representation
	char* ____vbName3_marshaled = NULL;
	____vbName3_marshaled = il2cpp_codegen_marshal_string_builder(___vbName3);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetGetVirtualButtonName)(___dataSetPtr0, ____trackableName1_marshaled, ___idx2, ____vbName3_marshaled, ___nameMaxLength4);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling of parameter '___vbName3' back from native representation
	il2cpp_codegen_marshal_string_builder_result(___vbName3, ____vbName3_marshaled);

	// Marshaling cleanup of parameter '___vbName3' native representation
	il2cpp_codegen_marshal_free(____vbName3_marshaled);
	____vbName3_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL imageTargetGetVirtualButtons(intptr_t, intptr_t, int32_t, intptr_t, char*);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::imageTargetGetVirtualButtons(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_imageTargetGetVirtualButtons_m2274780740 (RuntimeObject * __this /* static, unused */, intptr_t ___virtualButtonDataArray0, intptr_t ___rectangleDataArray1, int32_t ___virtualButtonDataArrayLength2, intptr_t ___dataSetPtr3, String_t* ___trackableName4, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, int32_t, intptr_t, char*);

	// Marshaling of parameter '___trackableName4' to native representation
	char* ____trackableName4_marshaled = NULL;
	____trackableName4_marshaled = il2cpp_codegen_marshal_string(___trackableName4);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(imageTargetGetVirtualButtons)(___virtualButtonDataArray0, ___rectangleDataArray1, ___virtualButtonDataArrayLength2, ___dataSetPtr3, ____trackableName4_marshaled);

	// Marshaling cleanup of parameter '___trackableName4' native representation
	il2cpp_codegen_marshal_free(____trackableName4_marshaled);
	____trackableName4_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL initFrameState(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::initFrameState(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_initFrameState_m251019466 (RuntimeObject * __this /* static, unused */, intptr_t ___frameState0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(initFrameState)(___frameState0);

}
extern "C" void DEFAULT_CALL initPlatformNative();
// System.Void Vuforia.VuforiaNativeIosWrapper::initPlatformNative()
extern "C"  void VuforiaNativeIosWrapper_initPlatformNative_m1148360790 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(initPlatformNative)();

}
extern "C" float DEFAULT_CALL multiTargetGetLargestSizeComponent(intptr_t, char*);
// System.Single Vuforia.VuforiaNativeIosWrapper::multiTargetGetLargestSizeComponent(System.IntPtr,System.String)
extern "C"  float VuforiaNativeIosWrapper_multiTargetGetLargestSizeComponent_m877427687 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, const RuntimeMethod* method)
{
	typedef float (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	float returnValue = reinterpret_cast<PInvokeFunc>(multiTargetGetLargestSizeComponent)(___dataSetPtr0, ____trackableName1_marshaled);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL objectTargetGetSize(intptr_t, char*, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTargetGetSize(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTargetGetSize_m2380033087 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___sizePtr2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, intptr_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(objectTargetGetSize)(___dataSetPtr0, ____trackableName1_marshaled, ___sizePtr2);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL objectTargetSetSize(intptr_t, char*, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTargetSetSize(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTargetSetSize_m1443530746 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___sizePtr2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, intptr_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(objectTargetSetSize)(___dataSetPtr0, ____trackableName1_marshaled, ___sizePtr2);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL objectTrackerActivateDataSet(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTrackerActivateDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTrackerActivateDataSet_m989691554 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(objectTrackerActivateDataSet)(___dataSetPtr0);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL objectTrackerCreateDataSet();
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::objectTrackerCreateDataSet()
extern "C"  intptr_t VuforiaNativeIosWrapper_objectTrackerCreateDataSet_m3029708900 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(objectTrackerCreateDataSet)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL objectTrackerDeactivateDataSet(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTrackerDeactivateDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTrackerDeactivateDataSet_m975093771 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(objectTrackerDeactivateDataSet)(___dataSetPtr0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL objectTrackerDestroyDataSet(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::objectTrackerDestroyDataSet(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_objectTrackerDestroyDataSet_m311987895 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(objectTrackerDestroyDataSet)(___dataSetPtr0);

	return returnValue;
}
extern "C" void DEFAULT_CALL onPause();
// System.Void Vuforia.VuforiaNativeIosWrapper::onPause()
extern "C"  void VuforiaNativeIosWrapper_onPause_m2162095750 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(onPause)();

}
extern "C" void DEFAULT_CALL onResume();
// System.Void Vuforia.VuforiaNativeIosWrapper::onResume()
extern "C"  void VuforiaNativeIosWrapper_onResume_m1164754594 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(onResume)();

}
extern "C" void DEFAULT_CALL onSurfaceChanged(int32_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::onSurfaceChanged(System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_onSurfaceChanged_m3866066131 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(onSurfaceChanged)(___width0, ___height1);

}
extern "C" void DEFAULT_CALL qcarAddCameraFrame(intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::qcarAddCameraFrame(System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_qcarAddCameraFrame_m3321820358 (RuntimeObject * __this /* static, unused */, intptr_t ___pixels0, int32_t ___width1, int32_t ___height2, int32_t ___format3, int32_t ___stride4, int32_t ___frameIdx5, int32_t ___flipHorizontally6, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(qcarAddCameraFrame)(___pixels0, ___width1, ___height2, ___format3, ___stride4, ___frameIdx5, ___flipHorizontally6);

}
extern "C" void DEFAULT_CALL qcarDeinit();
// System.Void Vuforia.VuforiaNativeIosWrapper::qcarDeinit()
extern "C"  void VuforiaNativeIosWrapper_qcarDeinit_m4212576879 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(qcarDeinit)();

}
extern "C" int32_t DEFAULT_CALL qcarGetBufferSize(int32_t, int32_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarGetBufferSize(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarGetBufferSize_m3158994288 (RuntimeObject * __this /* static, unused */, int32_t ___width0, int32_t ___height1, int32_t ___format2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(qcarGetBufferSize)(___width0, ___height1, ___format2);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL qcarInit(char*);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarInit(System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarInit_m496390534 (RuntimeObject * __this /* static, unused */, String_t* ___licenseKey0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___licenseKey0' to native representation
	char* ____licenseKey0_marshaled = NULL;
	____licenseKey0_marshaled = il2cpp_codegen_marshal_string(___licenseKey0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(qcarInit)(____licenseKey0_marshaled);

	// Marshaling cleanup of parameter '___licenseKey0' native representation
	il2cpp_codegen_marshal_free(____licenseKey0_marshaled);
	____licenseKey0_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL qcarSetFrameFormat(int32_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarSetFrameFormat(System.Int32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarSetFrameFormat_m4090591613 (RuntimeObject * __this /* static, unused */, int32_t ___format0, int32_t ___enabled1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(qcarSetFrameFormat)(___format0, ___enabled1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL qcarSetHint(uint32_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::qcarSetHint(System.UInt32,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_qcarSetHint_m2555757340 (RuntimeObject * __this /* static, unused */, uint32_t ___hint0, int32_t ___value1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (uint32_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(qcarSetHint)(___hint0, ___value1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL reconstructionFromTargetSetInitializationTarget(intptr_t, intptr_t, int32_t, intptr_t, intptr_t, intptr_t, intptr_t, float);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::reconstructionFromTargetSetInitializationTarget(System.IntPtr,System.IntPtr,System.Int32,System.IntPtr,System.IntPtr,System.IntPtr,System.IntPtr,System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_reconstructionFromTargetSetInitializationTarget_m1740495597 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, intptr_t ___dataSetPtr1, int32_t ___trackableID2, intptr_t ___occluderMin3, intptr_t ___occluderMax4, intptr_t ___offsetToOccluder5, intptr_t ___rotationAxisToOccluder6, float ___rotationAngleToOccluder7, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, int32_t, intptr_t, intptr_t, intptr_t, intptr_t, float);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(reconstructionFromTargetSetInitializationTarget)(___reconstruction0, ___dataSetPtr1, ___trackableID2, ___occluderMin3, ___occluderMax4, ___offsetToOccluder5, ___rotationAxisToOccluder6, ___rotationAngleToOccluder7);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL reconstructionSetMaximumArea(intptr_t, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::reconstructionSetMaximumArea(System.IntPtr,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_reconstructionSetMaximumArea_m3961464079 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, intptr_t ___maximumArea1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(reconstructionSetMaximumArea)(___reconstruction0, ___maximumArea1);

	return returnValue;
}
extern "C" void DEFAULT_CALL reconstructionSetNavMeshPadding(intptr_t, float);
// System.Void Vuforia.VuforiaNativeIosWrapper::reconstructionSetNavMeshPadding(System.IntPtr,System.Single)
extern "C"  void VuforiaNativeIosWrapper_reconstructionSetNavMeshPadding_m881105308 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, float ___padding1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, float);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(reconstructionSetNavMeshPadding)(___reconstruction0, ___padding1);

}
extern "C" int32_t DEFAULT_CALL reconstructionStart(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::reconstructionStart(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_reconstructionStart_m4250848222 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(reconstructionStart)(___reconstruction0);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL rendererCreateNativeTexture(uint32_t, uint32_t, int32_t);
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::rendererCreateNativeTexture(System.UInt32,System.UInt32,System.Int32)
extern "C"  intptr_t VuforiaNativeIosWrapper_rendererCreateNativeTexture_m1694208006 (RuntimeObject * __this /* static, unused */, uint32_t ___width0, uint32_t ___height1, int32_t ___format2, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (uint32_t, uint32_t, int32_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(rendererCreateNativeTexture)(___width0, ___height1, ___format2);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL rendererGetGraphicsAPI();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererGetGraphicsAPI()
extern "C"  int32_t VuforiaNativeIosWrapper_rendererGetGraphicsAPI_m3560655757 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rendererGetGraphicsAPI)();

	return returnValue;
}
extern "C" void DEFAULT_CALL rendererGetVideoBackgroundCfg(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::rendererGetVideoBackgroundCfg(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_rendererGetVideoBackgroundCfg_m1678358512 (RuntimeObject * __this /* static, unused */, intptr_t ___bgCfg0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(rendererGetVideoBackgroundCfg)(___bgCfg0);

}
extern "C" int32_t DEFAULT_CALL rendererGetVideoBackgroundTextureInfo(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererGetVideoBackgroundTextureInfo(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_rendererGetVideoBackgroundTextureInfo_m2700164971 (RuntimeObject * __this /* static, unused */, intptr_t ___texInfo0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rendererGetVideoBackgroundTextureInfo)(___texInfo0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL rendererIsVideoBackgroundTextureInfoAvailable();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererIsVideoBackgroundTextureInfoAvailable()
extern "C"  int32_t VuforiaNativeIosWrapper_rendererIsVideoBackgroundTextureInfoAvailable_m1274323877 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rendererIsVideoBackgroundTextureInfoAvailable)();

	return returnValue;
}
extern "C" void DEFAULT_CALL rendererSetVideoBackgroundCfg(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::rendererSetVideoBackgroundCfg(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_rendererSetVideoBackgroundCfg_m1972712013 (RuntimeObject * __this /* static, unused */, intptr_t ___bgCfg0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(rendererSetVideoBackgroundCfg)(___bgCfg0);

}
extern "C" int32_t DEFAULT_CALL rendererSetVideoBackgroundTextureID(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererSetVideoBackgroundTextureID(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_rendererSetVideoBackgroundTextureID_m1187272398 (RuntimeObject * __this /* static, unused */, int32_t ___textureID0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rendererSetVideoBackgroundTextureID)(___textureID0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL rendererSetVideoBackgroundTexturePtr(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rendererSetVideoBackgroundTexturePtr(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_rendererSetVideoBackgroundTexturePtr_m3392285240 (RuntimeObject * __this /* static, unused */, intptr_t ___texturePtr0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rendererSetVideoBackgroundTexturePtr)(___texturePtr0);

	return returnValue;
}
extern "C" void DEFAULT_CALL renderingPrimitives_DeleteCopy();
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_DeleteCopy()
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_DeleteCopy_m535892042 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_DeleteCopy)();

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetDistortionMesh(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetDistortionMesh(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetDistortionMesh_m399350896 (RuntimeObject * __this /* static, unused */, int32_t ___viewId0, intptr_t ___meshData1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetDistortionMesh)(___viewId0, ___meshData1);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetDistortionMeshSize(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetDistortionMeshSize(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetDistortionMeshSize_m3433711271 (RuntimeObject * __this /* static, unused */, int32_t ___viewId0, intptr_t ___size1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetDistortionMeshSize)(___viewId0, ___size1);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetEffectiveFov(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetEffectiveFov(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetEffectiveFov_m1766427464 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___fovContainer1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetEffectiveFov)(___viewID0, ___fovContainer1);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetEyeDisplayAdjustmentMatrix(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetEyeDisplayAdjustmentMatrix(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetEyeDisplayAdjustmentMatrix_m1813656632 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___matrixContainer1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetEyeDisplayAdjustmentMatrix)(___viewID0, ___matrixContainer1);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetNormalizedViewport(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetNormalizedViewport(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetNormalizedViewport_m1528191784 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___viewportContainer1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetNormalizedViewport)(___viewID0, ___viewportContainer1);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetProjectionMatrix(int32_t, float, float, intptr_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetProjectionMatrix(System.Int32,System.Single,System.Single,System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetProjectionMatrix_m1357884672 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, float ___near1, float ___far2, intptr_t ___projectionContainer3, int32_t ___screenOrientation4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, float, float, intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetProjectionMatrix)(___viewID0, ___near1, ___far2, ___projectionContainer3, ___screenOrientation4);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetViewport(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetViewport(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetViewport_m2833809889 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___viewportContainer1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetViewport)(___viewID0, ___viewportContainer1);

}
extern "C" void DEFAULT_CALL renderingPrimitives_GetViewportCentreToEyeAxis(int32_t, intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::renderingPrimitives_GetViewportCentreToEyeAxis(System.Int32,System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_renderingPrimitives_GetViewportCentreToEyeAxis_m2133419223 (RuntimeObject * __this /* static, unused */, int32_t ___viewID0, intptr_t ___vectorContainer1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(renderingPrimitives_GetViewportCentreToEyeAxis)(___viewID0, ___vectorContainer1);

}
extern "C" void DEFAULT_CALL rotationalDeviceTracker_GetModelCorrectionTransform(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_GetModelCorrectionTransform(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_rotationalDeviceTracker_GetModelCorrectionTransform_m4116192559 (RuntimeObject * __this /* static, unused */, intptr_t ___pivot0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(rotationalDeviceTracker_GetModelCorrectionTransform)(___pivot0);

}
extern "C" int32_t DEFAULT_CALL rotationalDeviceTracker_Recenter();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_Recenter()
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_Recenter_m2292918386 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rotationalDeviceTracker_Recenter)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL rotationalDeviceTracker_SetModelCorrectionMode(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_SetModelCorrectionMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_SetModelCorrectionMode_m3760711071 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rotationalDeviceTracker_SetModelCorrectionMode)(___mode0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL rotationalDeviceTracker_SetModelCorrectionModeWithTransform(int32_t, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_SetModelCorrectionModeWithTransform(System.Int32,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_SetModelCorrectionModeWithTransform_m1499777731 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, intptr_t ___pivot1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t, intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rotationalDeviceTracker_SetModelCorrectionModeWithTransform)(___mode0, ___pivot1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL rotationalDeviceTracker_SetPosePrediction(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::rotationalDeviceTracker_SetPosePrediction(System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_rotationalDeviceTracker_SetPosePrediction_m3158892482 (RuntimeObject * __this /* static, unused */, bool ___mode0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(rotationalDeviceTracker_SetPosePrediction)(static_cast<int32_t>(___mode0));

	return returnValue;
}
extern "C" void DEFAULT_CALL setApplicationEnvironment(int32_t, int32_t, int32_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::setApplicationEnvironment(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_setApplicationEnvironment_m86779826 (RuntimeObject * __this /* static, unused */, int32_t ___unityVersionMajor0, int32_t ___unityVersionMinor1, int32_t ___unityVersionChange2, int32_t ___sdkWrapperType3, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, int32_t, int32_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setApplicationEnvironment)(___unityVersionMajor0, ___unityVersionMinor1, ___unityVersionChange2, ___sdkWrapperType3);

}
extern "C" void DEFAULT_CALL setRenderBuffers(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::setRenderBuffers(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_setRenderBuffers_m3774876697 (RuntimeObject * __this /* static, unused */, intptr_t ___colorBuffer0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(setRenderBuffers)(___colorBuffer0);

}
extern "C" int32_t DEFAULT_CALL smartTerrainBuilderAddReconstruction(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainBuilderAddReconstruction(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainBuilderAddReconstruction_m1293846854 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(smartTerrainBuilderAddReconstruction)(___reconstruction0);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL smartTerrainBuilderCreateReconstructionFromTarget();
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::smartTerrainBuilderCreateReconstructionFromTarget()
extern "C"  intptr_t VuforiaNativeIosWrapper_smartTerrainBuilderCreateReconstructionFromTarget_m3676350375 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(smartTerrainBuilderCreateReconstructionFromTarget)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL smartTerrainBuilderRemoveReconstruction(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainBuilderRemoveReconstruction(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainBuilderRemoveReconstruction_m1763309370 (RuntimeObject * __this /* static, unused */, intptr_t ___reconstruction0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(smartTerrainBuilderRemoveReconstruction)(___reconstruction0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL smartTerrainTrackerDeinitBuilder();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainTrackerDeinitBuilder()
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainTrackerDeinitBuilder_m2292570849 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(smartTerrainTrackerDeinitBuilder)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL smartTerrainTrackerInitBuilder();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainTrackerInitBuilder()
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainTrackerInitBuilder_m3524084684 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(smartTerrainTrackerInitBuilder)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL smartTerrainTrackerSetScaleToMillimeter(float);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::smartTerrainTrackerSetScaleToMillimeter(System.Single)
extern "C"  int32_t VuforiaNativeIosWrapper_smartTerrainTrackerSetScaleToMillimeter_m563366966 (RuntimeObject * __this /* static, unused */, float ___scaleToMillimenters0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (float);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(smartTerrainTrackerSetScaleToMillimeter)(___scaleToMillimenters0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL startExtendedTracking(intptr_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::startExtendedTracking(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_startExtendedTracking_m357143318 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, int32_t ___trackableID1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(startExtendedTracking)(___dataSetPtr0, ___trackableID1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL targetFinderDeinit();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderDeinit()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderDeinit_m1075093056 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(targetFinderDeinit)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL targetFinderGetInitState();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderGetInitState()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderGetInitState_m2122533097 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(targetFinderGetInitState)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL targetFinderGetResults(intptr_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderGetResults(System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderGetResults_m29416918 (RuntimeObject * __this /* static, unused */, intptr_t ___searchResultArray0, int32_t ___searchResultArrayLength1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(targetFinderGetResults)(___searchResultArray0, ___searchResultArrayLength1);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL targetFinderStartInit(char*, char*);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderStartInit(System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderStartInit_m282508 (RuntimeObject * __this /* static, unused */, String_t* ___userKey0, String_t* ___secretKey1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___userKey0' to native representation
	char* ____userKey0_marshaled = NULL;
	____userKey0_marshaled = il2cpp_codegen_marshal_string(___userKey0);

	// Marshaling of parameter '___secretKey1' to native representation
	char* ____secretKey1_marshaled = NULL;
	____secretKey1_marshaled = il2cpp_codegen_marshal_string(___secretKey1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(targetFinderStartInit)(____userKey0_marshaled, ____secretKey1_marshaled);

	// Marshaling cleanup of parameter '___userKey0' native representation
	il2cpp_codegen_marshal_free(____userKey0_marshaled);
	____userKey0_marshaled = NULL;

	// Marshaling cleanup of parameter '___secretKey1' native representation
	il2cpp_codegen_marshal_free(____secretKey1_marshaled);
	____secretKey1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL targetFinderStartRecognition();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderStartRecognition()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderStartRecognition_m2312748431 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(targetFinderStartRecognition)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL targetFinderStop();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::targetFinderStop()
extern "C"  int32_t VuforiaNativeIosWrapper_targetFinderStop_m1766141929 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(targetFinderStop)();

	return returnValue;
}
extern "C" void DEFAULT_CALL targetFinderUpdate(intptr_t, int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::targetFinderUpdate(System.IntPtr,System.Int32)
extern "C"  void VuforiaNativeIosWrapper_targetFinderUpdate_m307650428 (RuntimeObject * __this /* static, unused */, intptr_t ___targetFinderState0, int32_t ___filterMode1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(targetFinderUpdate)(___targetFinderState0, ___filterMode1);

}
extern "C" int32_t DEFAULT_CALL trackerManagerDeinitTracker(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::trackerManagerDeinitTracker(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_trackerManagerDeinitTracker_m862961198 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(trackerManagerDeinitTracker)(___trackerTypeID0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL trackerManagerInitTracker(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::trackerManagerInitTracker(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_trackerManagerInitTracker_m3155645533 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(trackerManagerInitTracker)(___trackerTypeID0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL trackerStart(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::trackerStart(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_trackerStart_m3650945244 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(trackerStart)(___trackerTypeID0);

	return returnValue;
}
extern "C" void DEFAULT_CALL trackerStop(int32_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::trackerStop(System.Int32)
extern "C"  void VuforiaNativeIosWrapper_trackerStop_m136099586 (RuntimeObject * __this /* static, unused */, int32_t ___trackerTypeID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(trackerStop)(___trackerTypeID0);

}
extern "C" int32_t DEFAULT_CALL updateQCAR(intptr_t, int32_t, intptr_t, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::updateQCAR(System.IntPtr,System.Int32,System.IntPtr,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_updateQCAR_m3823614422 (RuntimeObject * __this /* static, unused */, intptr_t ___imageHeaderDataArray0, int32_t ___imageHeaderArrayLength1, intptr_t ___frameState2, int32_t ___screenOrientation3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t, intptr_t, int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(updateQCAR)(___imageHeaderDataArray0, ___imageHeaderArrayLength1, ___frameState2, ___screenOrientation3);

	return returnValue;
}
extern "C" void DEFAULT_CALL viewerParameters_delete(intptr_t);
// System.Void Vuforia.VuforiaNativeIosWrapper::viewerParameters_delete(System.IntPtr)
extern "C"  void VuforiaNativeIosWrapper_viewerParameters_delete_m1022060148 (RuntimeObject * __this /* static, unused */, intptr_t ___obj0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(viewerParameters_delete)(___obj0);

}
extern "C" intptr_t DEFAULT_CALL viewerParametersList_GetByIndex(intptr_t, int32_t);
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::viewerParametersList_GetByIndex(System.IntPtr,System.Int32)
extern "C"  intptr_t VuforiaNativeIosWrapper_viewerParametersList_GetByIndex_m1568290052 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, int32_t ___idx1, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, int32_t);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(viewerParametersList_GetByIndex)(___vpList0, ___idx1);

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL viewerParametersList_GetByNameManufacturer(intptr_t, char*, char*);
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::viewerParametersList_GetByNameManufacturer(System.IntPtr,System.String,System.String)
extern "C"  intptr_t VuforiaNativeIosWrapper_viewerParametersList_GetByNameManufacturer_m2600346792 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, String_t* ___name1, String_t* ___manufacturer2, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___name1' to native representation
	char* ____name1_marshaled = NULL;
	____name1_marshaled = il2cpp_codegen_marshal_string(___name1);

	// Marshaling of parameter '___manufacturer2' to native representation
	char* ____manufacturer2_marshaled = NULL;
	____manufacturer2_marshaled = il2cpp_codegen_marshal_string(___manufacturer2);

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(viewerParametersList_GetByNameManufacturer)(___vpList0, ____name1_marshaled, ____manufacturer2_marshaled);

	// Marshaling cleanup of parameter '___name1' native representation
	il2cpp_codegen_marshal_free(____name1_marshaled);
	____name1_marshaled = NULL;

	// Marshaling cleanup of parameter '___manufacturer2' native representation
	il2cpp_codegen_marshal_free(____manufacturer2_marshaled);
	____manufacturer2_marshaled = NULL;

	return returnValue;
}
extern "C" void DEFAULT_CALL viewerParametersList_SetSDKFilter(intptr_t, char*);
// System.Void Vuforia.VuforiaNativeIosWrapper::viewerParametersList_SetSDKFilter(System.IntPtr,System.String)
extern "C"  void VuforiaNativeIosWrapper_viewerParametersList_SetSDKFilter_m1101420845 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, String_t* ___filter1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*);

	// Marshaling of parameter '___filter1' to native representation
	char* ____filter1_marshaled = NULL;
	____filter1_marshaled = il2cpp_codegen_marshal_string(___filter1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(viewerParametersList_SetSDKFilter)(___vpList0, ____filter1_marshaled);

	// Marshaling cleanup of parameter '___filter1' native representation
	il2cpp_codegen_marshal_free(____filter1_marshaled);
	____filter1_marshaled = NULL;

}
extern "C" int32_t DEFAULT_CALL viewerParametersList_Size(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::viewerParametersList_Size(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_viewerParametersList_Size_m3374613508 (RuntimeObject * __this /* static, unused */, intptr_t ___vpList0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(viewerParametersList_Size)(___vpList0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL virtualButtonGetId(intptr_t, char*, char*);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonGetId(System.IntPtr,System.String,System.String)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonGetId_m2887502506 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___virtualButtonName2' to native representation
	char* ____virtualButtonName2_marshaled = NULL;
	____virtualButtonName2_marshaled = il2cpp_codegen_marshal_string(___virtualButtonName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(virtualButtonGetId)(___dataSetPtr0, ____trackableName1_marshaled, ____virtualButtonName2_marshaled);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___virtualButtonName2' native representation
	il2cpp_codegen_marshal_free(____virtualButtonName2_marshaled);
	____virtualButtonName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL virtualButtonSetAreaRectangle(intptr_t, char*, char*, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonSetAreaRectangle(System.IntPtr,System.String,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonSetAreaRectangle_m3959624685 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, intptr_t ___rectData3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*, intptr_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___virtualButtonName2' to native representation
	char* ____virtualButtonName2_marshaled = NULL;
	____virtualButtonName2_marshaled = il2cpp_codegen_marshal_string(___virtualButtonName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(virtualButtonSetAreaRectangle)(___dataSetPtr0, ____trackableName1_marshaled, ____virtualButtonName2_marshaled, ___rectData3);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___virtualButtonName2' native representation
	il2cpp_codegen_marshal_free(____virtualButtonName2_marshaled);
	____virtualButtonName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL virtualButtonSetEnabled(intptr_t, char*, char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonSetEnabled(System.IntPtr,System.String,System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonSetEnabled_m4103479847 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, int32_t ___enabled3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*, int32_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___virtualButtonName2' to native representation
	char* ____virtualButtonName2_marshaled = NULL;
	____virtualButtonName2_marshaled = il2cpp_codegen_marshal_string(___virtualButtonName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(virtualButtonSetEnabled)(___dataSetPtr0, ____trackableName1_marshaled, ____virtualButtonName2_marshaled, ___enabled3);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___virtualButtonName2' native representation
	il2cpp_codegen_marshal_free(____virtualButtonName2_marshaled);
	____virtualButtonName2_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL virtualButtonSetSensitivity(intptr_t, char*, char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::virtualButtonSetSensitivity(System.IntPtr,System.String,System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_virtualButtonSetSensitivity_m862172097 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, String_t* ___virtualButtonName2, int32_t ___sensitivity3, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, char*, int32_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Marshaling of parameter '___virtualButtonName2' to native representation
	char* ____virtualButtonName2_marshaled = NULL;
	____virtualButtonName2_marshaled = il2cpp_codegen_marshal_string(___virtualButtonName2);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(virtualButtonSetSensitivity)(___dataSetPtr0, ____trackableName1_marshaled, ____virtualButtonName2_marshaled, ___sensitivity3);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	// Marshaling cleanup of parameter '___virtualButtonName2' native representation
	il2cpp_codegen_marshal_free(____virtualButtonName2_marshaled);
	____virtualButtonName2_marshaled = NULL;

	return returnValue;
}
extern "C" intptr_t DEFAULT_CALL vuforiaGetRenderEventCallback();
// System.IntPtr Vuforia.VuforiaNativeIosWrapper::vuforiaGetRenderEventCallback()
extern "C"  intptr_t VuforiaNativeIosWrapper_vuforiaGetRenderEventCallback_m3112993526 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(vuforiaGetRenderEventCallback)();

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL vuMarkTemplateGetOrigin(intptr_t, char*, intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::vuMarkTemplateGetOrigin(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_vuMarkTemplateGetOrigin_m917331585 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, intptr_t ___originPtr2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, intptr_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(vuMarkTemplateGetOrigin)(___dataSetPtr0, ____trackableName1_marshaled, ___originPtr2);

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL vuMarkTemplateSetTrackingFromRuntimeAppearance(intptr_t, char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::vuMarkTemplateSetTrackingFromRuntimeAppearance(System.IntPtr,System.String,System.Boolean)
extern "C"  int32_t VuforiaNativeIosWrapper_vuMarkTemplateSetTrackingFromRuntimeAppearance_m3954844824 (RuntimeObject * __this /* static, unused */, intptr_t ___dataSetPtr0, String_t* ___trackableName1, bool ___enable2, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, char*, int32_t);

	// Marshaling of parameter '___trackableName1' to native representation
	char* ____trackableName1_marshaled = NULL;
	____trackableName1_marshaled = il2cpp_codegen_marshal_string(___trackableName1);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(vuMarkTemplateSetTrackingFromRuntimeAppearance)(___dataSetPtr0, ____trackableName1_marshaled, static_cast<int32_t>(___enable2));

	// Marshaling cleanup of parameter '___trackableName1' native representation
	il2cpp_codegen_marshal_free(____trackableName1_marshaled);
	____trackableName1_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListAddWordsFromFile(char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListAddWordsFromFile(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListAddWordsFromFile_m2691145391 (RuntimeObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListAddWordsFromFile)(____path0_marshaled, ___storageType1);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListAddWordToFilterListU(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListAddWordToFilterListU(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListAddWordToFilterListU_m944574365 (RuntimeObject * __this /* static, unused */, intptr_t ___word0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListAddWordToFilterListU)(___word0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListAddWordU(intptr_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListAddWordU(System.IntPtr)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListAddWordU_m3831652562 (RuntimeObject * __this /* static, unused */, intptr_t ___word0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListAddWordU)(___word0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListLoadFilterList(char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListLoadFilterList(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListLoadFilterList_m604465529 (RuntimeObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListLoadFilterList)(____path0_marshaled, ___storageType1);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListLoadWordList(char*, int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListLoadWordList(System.String,System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListLoadWordList_m3605546583 (RuntimeObject * __this /* static, unused */, String_t* ___path0, int32_t ___storageType1, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (char*, int32_t);

	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListLoadWordList)(____path0_marshaled, ___storageType1);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListSetFilterMode(int32_t);
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListSetFilterMode(System.Int32)
extern "C"  int32_t VuforiaNativeIosWrapper_wordListSetFilterMode_m2109095690 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListSetFilterMode)(___mode0);

	return returnValue;
}
extern "C" int32_t DEFAULT_CALL wordListUnloadAllLists();
// System.Int32 Vuforia.VuforiaNativeIosWrapper::wordListUnloadAllLists()
extern "C"  int32_t VuforiaNativeIosWrapper_wordListUnloadAllLists_m316317724 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(wordListUnloadAllLists)();

	return returnValue;
}
// System.Void Vuforia.VuforiaNativeIosWrapper::.ctor()
extern "C"  void VuforiaNativeIosWrapper__ctor_m904066745 (VuforiaNativeIosWrapper_t2037146173 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
