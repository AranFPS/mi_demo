﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry>
struct Dictionary_2_t1160396953;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t1515895227;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo>
struct Dictionary_2_t591960174;
// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status>
struct Dictionary_2_t80136809;
// Vuforia.VuMarkTemplateImpl
struct VuMarkTemplateImpl_t667343433;
// Vuforia.InstanceIdImpl
struct InstanceIdImpl_t2824054591;
// System.String[]
struct StringU5BU5D_t1281789340;
// Vuforia.IUnityPlayer
struct IUnityPlayer_t3870301646;
// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct SessionStateChanged_t3163629820;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.String
struct String_t;
// Vuforia.VuforiaAbstractBehaviour
struct VuforiaAbstractBehaviour_t3510878193;
// Vuforia.ARController
struct ARController_t116632334;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour>>
struct Dictionary_2_t941439618;
// System.Collections.Generic.List`1<Vuforia.VuMarkTarget>
struct List_1_t2601648545;
// System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour>
struct List_1_t2052726287;
// System.Action`1<Vuforia.VuMarkTarget>
struct Action_1_t1302041398;
// System.Action`1<Vuforia.VuMarkAbstractBehaviour>
struct Action_1_t753119140;
// System.Char[]
struct CharU5BU5D_t3528271667;
// Vuforia.DataSetImpl
struct DataSetImpl_t2094717509;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Action
struct Action_t1264377477;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Camera
struct Camera_t4157153871;
// Vuforia.HideExcessAreaAbstractBehaviour[]
struct HideExcessAreaAbstractBehaviourU5BU5D_t2893788201;
// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour>
struct List_1_t546497774;
// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour>
struct Dictionary_2_t507033335;
// Vuforia.BackgroundPlaneAbstractBehaviour
struct BackgroundPlaneAbstractBehaviour_t4147679365;
// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct SerializableViewerParameters_t2043332680;
// UnityEngine.Transform
struct Transform_t3600365921;
// Vuforia.VuforiaARController
struct VuforiaARController_t1876945237;
// Vuforia.DistortionRenderingBehaviour
struct DistortionRenderingBehaviour_t1858983148;
// Vuforia.Trackable
struct Trackable_t2451999991;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Vuforia.EyewearDevice
struct EyewearDevice_t3223385723;
// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct GenericVuforiaConfiguration_t1909783692;
// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration
struct DigitalEyewearConfiguration_t2828783036;
// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration
struct DatabaseLoadConfiguration_t3815674388;
// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration
struct VideoBackgroundConfiguration_t1174662728;
// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration
struct SmartTerrainTrackerConfiguration_t3112457179;
// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration
struct DeviceTrackerConfiguration_t1841344395;
// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration
struct WebCamConfiguration_t802847339;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t3436254912;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_t3211687919;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t445758600;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t3848515759;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1709987734;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;
// UnityEngine.RenderTexture[]
struct RenderTextureU5BU5D_t4111643188;
// Vuforia.TrackableBehaviour
struct TrackableBehaviour_t1113559212;
// System.Action`1<Vuforia.VuforiaAbstractBehaviour>
struct Action_1_t3683345788;
// System.Action`1<System.Boolean>
struct Action_1_t269755560;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// Vuforia.IExcessAreaClipping
struct IExcessAreaClipping_t3629265436;
// Vuforia.VideoBackgroundManager
struct VideoBackgroundManager_t2198727358;
// Vuforia.ObjectTracker
struct ObjectTracker_t4177997237;
// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler>
struct List_1_t1049732891;
// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler>
struct List_1_t2968050330;
// Vuforia.ReconstructionFromTargetAbstractBehaviour
struct ReconstructionFromTargetAbstractBehaviour_t2785373562;
// Vuforia.ObjectTarget
struct ObjectTarget_t3212252422;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifndef U3CMODULEU3E_T692745541_H
#define U3CMODULEU3E_T692745541_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745541 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745541_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745542_H
#define U3CMODULEU3E_T692745542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745542 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745542_H
#ifndef U3CMODULEU3E_T692745544_H
#define U3CMODULEU3E_T692745544_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745544 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745544_H
#ifndef U3CMODULEU3E_T692745543_H
#define U3CMODULEU3E_T692745543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745543 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745543_H
#ifndef REMOTESETTINGS_T1718627291_H
#define REMOTESETTINGS_T1718627291_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings
struct  RemoteSettings_t1718627291  : public RuntimeObject
{
public:

public:
};

struct RemoteSettings_t1718627291_StaticFields
{
public:
	// UnityEngine.RemoteSettings/UpdatedEventHandler UnityEngine.RemoteSettings::Updated
	UpdatedEventHandler_t1027848393 * ___Updated_0;

public:
	inline static int32_t get_offset_of_Updated_0() { return static_cast<int32_t>(offsetof(RemoteSettings_t1718627291_StaticFields, ___Updated_0)); }
	inline UpdatedEventHandler_t1027848393 * get_Updated_0() const { return ___Updated_0; }
	inline UpdatedEventHandler_t1027848393 ** get_address_of_Updated_0() { return &___Updated_0; }
	inline void set_Updated_0(UpdatedEventHandler_t1027848393 * value)
	{
		___Updated_0 = value;
		Il2CppCodeGenWriteBarrier((&___Updated_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REMOTESETTINGS_T1718627291_H
#ifndef DELEGATEHELPER_T3231076514_H
#define DELEGATEHELPER_T3231076514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DelegateHelper
struct  DelegateHelper_t3231076514  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEHELPER_T3231076514_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef HOLOLENSEXTENDEDTRACKINGMANAGER_T2009717195_H
#define HOLOLENSEXTENDEDTRACKINGMANAGER_T2009717195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager
struct  HoloLensExtendedTrackingManager_t2009717195  : public RuntimeObject
{
public:
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager::mNumFramesStablePose
	int32_t ___mNumFramesStablePose_0;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxPoseRelDistance
	float ___mMaxPoseRelDistance_1;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxPoseAngleDiff
	float ___mMaxPoseAngleDiff_2;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxCamPoseAbsDistance
	float ___mMaxCamPoseAbsDistance_3;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxCamPoseAngleDiff
	float ___mMaxCamPoseAngleDiff_4;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager::mMinNumFramesPoseOff
	int32_t ___mMinNumFramesPoseOff_5;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMinPoseUpdateRelDistance
	float ___mMinPoseUpdateRelDistance_6;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMinPoseUpdateAngleDiff
	float ___mMinPoseUpdateAngleDiff_7;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mTrackableSizeInViewThreshold
	float ___mTrackableSizeInViewThreshold_8;
	// System.Single Vuforia.HoloLensExtendedTrackingManager::mMaxDistanceFromViewCenterForPoseUpdate
	float ___mMaxDistanceFromViewCenterForPoseUpdate_9;
	// System.Boolean Vuforia.HoloLensExtendedTrackingManager::mSetWorldAnchors
	bool ___mSetWorldAnchors_10;
	// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry> Vuforia.HoloLensExtendedTrackingManager::mTrackingList
	Dictionary_2_t1160396953 * ___mTrackingList_11;
	// System.Collections.Generic.HashSet`1<System.Int32> Vuforia.HoloLensExtendedTrackingManager::mTrackablesExtendedTrackingEnabled
	HashSet_1_t1515895227 * ___mTrackablesExtendedTrackingEnabled_12;
	// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.HoloLensExtendedTrackingManager/PoseInfo> Vuforia.HoloLensExtendedTrackingManager::mTrackablesCurrentlyExtendedTracked
	Dictionary_2_t591960174 * ___mTrackablesCurrentlyExtendedTracked_13;
	// System.Collections.Generic.Dictionary`2<Vuforia.VuforiaManager/TrackableIdPair,Vuforia.TrackableBehaviour/Status> Vuforia.HoloLensExtendedTrackingManager::mExtendedTrackablesState
	Dictionary_2_t80136809 * ___mExtendedTrackablesState_14;

public:
	inline static int32_t get_offset_of_mNumFramesStablePose_0() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mNumFramesStablePose_0)); }
	inline int32_t get_mNumFramesStablePose_0() const { return ___mNumFramesStablePose_0; }
	inline int32_t* get_address_of_mNumFramesStablePose_0() { return &___mNumFramesStablePose_0; }
	inline void set_mNumFramesStablePose_0(int32_t value)
	{
		___mNumFramesStablePose_0 = value;
	}

	inline static int32_t get_offset_of_mMaxPoseRelDistance_1() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMaxPoseRelDistance_1)); }
	inline float get_mMaxPoseRelDistance_1() const { return ___mMaxPoseRelDistance_1; }
	inline float* get_address_of_mMaxPoseRelDistance_1() { return &___mMaxPoseRelDistance_1; }
	inline void set_mMaxPoseRelDistance_1(float value)
	{
		___mMaxPoseRelDistance_1 = value;
	}

	inline static int32_t get_offset_of_mMaxPoseAngleDiff_2() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMaxPoseAngleDiff_2)); }
	inline float get_mMaxPoseAngleDiff_2() const { return ___mMaxPoseAngleDiff_2; }
	inline float* get_address_of_mMaxPoseAngleDiff_2() { return &___mMaxPoseAngleDiff_2; }
	inline void set_mMaxPoseAngleDiff_2(float value)
	{
		___mMaxPoseAngleDiff_2 = value;
	}

	inline static int32_t get_offset_of_mMaxCamPoseAbsDistance_3() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMaxCamPoseAbsDistance_3)); }
	inline float get_mMaxCamPoseAbsDistance_3() const { return ___mMaxCamPoseAbsDistance_3; }
	inline float* get_address_of_mMaxCamPoseAbsDistance_3() { return &___mMaxCamPoseAbsDistance_3; }
	inline void set_mMaxCamPoseAbsDistance_3(float value)
	{
		___mMaxCamPoseAbsDistance_3 = value;
	}

	inline static int32_t get_offset_of_mMaxCamPoseAngleDiff_4() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMaxCamPoseAngleDiff_4)); }
	inline float get_mMaxCamPoseAngleDiff_4() const { return ___mMaxCamPoseAngleDiff_4; }
	inline float* get_address_of_mMaxCamPoseAngleDiff_4() { return &___mMaxCamPoseAngleDiff_4; }
	inline void set_mMaxCamPoseAngleDiff_4(float value)
	{
		___mMaxCamPoseAngleDiff_4 = value;
	}

	inline static int32_t get_offset_of_mMinNumFramesPoseOff_5() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMinNumFramesPoseOff_5)); }
	inline int32_t get_mMinNumFramesPoseOff_5() const { return ___mMinNumFramesPoseOff_5; }
	inline int32_t* get_address_of_mMinNumFramesPoseOff_5() { return &___mMinNumFramesPoseOff_5; }
	inline void set_mMinNumFramesPoseOff_5(int32_t value)
	{
		___mMinNumFramesPoseOff_5 = value;
	}

	inline static int32_t get_offset_of_mMinPoseUpdateRelDistance_6() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMinPoseUpdateRelDistance_6)); }
	inline float get_mMinPoseUpdateRelDistance_6() const { return ___mMinPoseUpdateRelDistance_6; }
	inline float* get_address_of_mMinPoseUpdateRelDistance_6() { return &___mMinPoseUpdateRelDistance_6; }
	inline void set_mMinPoseUpdateRelDistance_6(float value)
	{
		___mMinPoseUpdateRelDistance_6 = value;
	}

	inline static int32_t get_offset_of_mMinPoseUpdateAngleDiff_7() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMinPoseUpdateAngleDiff_7)); }
	inline float get_mMinPoseUpdateAngleDiff_7() const { return ___mMinPoseUpdateAngleDiff_7; }
	inline float* get_address_of_mMinPoseUpdateAngleDiff_7() { return &___mMinPoseUpdateAngleDiff_7; }
	inline void set_mMinPoseUpdateAngleDiff_7(float value)
	{
		___mMinPoseUpdateAngleDiff_7 = value;
	}

	inline static int32_t get_offset_of_mTrackableSizeInViewThreshold_8() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mTrackableSizeInViewThreshold_8)); }
	inline float get_mTrackableSizeInViewThreshold_8() const { return ___mTrackableSizeInViewThreshold_8; }
	inline float* get_address_of_mTrackableSizeInViewThreshold_8() { return &___mTrackableSizeInViewThreshold_8; }
	inline void set_mTrackableSizeInViewThreshold_8(float value)
	{
		___mTrackableSizeInViewThreshold_8 = value;
	}

	inline static int32_t get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mMaxDistanceFromViewCenterForPoseUpdate_9)); }
	inline float get_mMaxDistanceFromViewCenterForPoseUpdate_9() const { return ___mMaxDistanceFromViewCenterForPoseUpdate_9; }
	inline float* get_address_of_mMaxDistanceFromViewCenterForPoseUpdate_9() { return &___mMaxDistanceFromViewCenterForPoseUpdate_9; }
	inline void set_mMaxDistanceFromViewCenterForPoseUpdate_9(float value)
	{
		___mMaxDistanceFromViewCenterForPoseUpdate_9 = value;
	}

	inline static int32_t get_offset_of_mSetWorldAnchors_10() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mSetWorldAnchors_10)); }
	inline bool get_mSetWorldAnchors_10() const { return ___mSetWorldAnchors_10; }
	inline bool* get_address_of_mSetWorldAnchors_10() { return &___mSetWorldAnchors_10; }
	inline void set_mSetWorldAnchors_10(bool value)
	{
		___mSetWorldAnchors_10 = value;
	}

	inline static int32_t get_offset_of_mTrackingList_11() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mTrackingList_11)); }
	inline Dictionary_2_t1160396953 * get_mTrackingList_11() const { return ___mTrackingList_11; }
	inline Dictionary_2_t1160396953 ** get_address_of_mTrackingList_11() { return &___mTrackingList_11; }
	inline void set_mTrackingList_11(Dictionary_2_t1160396953 * value)
	{
		___mTrackingList_11 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackingList_11), value);
	}

	inline static int32_t get_offset_of_mTrackablesExtendedTrackingEnabled_12() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mTrackablesExtendedTrackingEnabled_12)); }
	inline HashSet_1_t1515895227 * get_mTrackablesExtendedTrackingEnabled_12() const { return ___mTrackablesExtendedTrackingEnabled_12; }
	inline HashSet_1_t1515895227 ** get_address_of_mTrackablesExtendedTrackingEnabled_12() { return &___mTrackablesExtendedTrackingEnabled_12; }
	inline void set_mTrackablesExtendedTrackingEnabled_12(HashSet_1_t1515895227 * value)
	{
		___mTrackablesExtendedTrackingEnabled_12 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesExtendedTrackingEnabled_12), value);
	}

	inline static int32_t get_offset_of_mTrackablesCurrentlyExtendedTracked_13() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mTrackablesCurrentlyExtendedTracked_13)); }
	inline Dictionary_2_t591960174 * get_mTrackablesCurrentlyExtendedTracked_13() const { return ___mTrackablesCurrentlyExtendedTracked_13; }
	inline Dictionary_2_t591960174 ** get_address_of_mTrackablesCurrentlyExtendedTracked_13() { return &___mTrackablesCurrentlyExtendedTracked_13; }
	inline void set_mTrackablesCurrentlyExtendedTracked_13(Dictionary_2_t591960174 * value)
	{
		___mTrackablesCurrentlyExtendedTracked_13 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackablesCurrentlyExtendedTracked_13), value);
	}

	inline static int32_t get_offset_of_mExtendedTrackablesState_14() { return static_cast<int32_t>(offsetof(HoloLensExtendedTrackingManager_t2009717195, ___mExtendedTrackablesState_14)); }
	inline Dictionary_2_t80136809 * get_mExtendedTrackablesState_14() const { return ___mExtendedTrackablesState_14; }
	inline Dictionary_2_t80136809 ** get_address_of_mExtendedTrackablesState_14() { return &___mExtendedTrackablesState_14; }
	inline void set_mExtendedTrackablesState_14(Dictionary_2_t80136809 * value)
	{
		___mExtendedTrackablesState_14 = value;
		Il2CppCodeGenWriteBarrier((&___mExtendedTrackablesState_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOLENSEXTENDEDTRACKINGMANAGER_T2009717195_H
#ifndef VUFORIAEXTENDEDTRACKINGMANAGER_T262318595_H
#define VUFORIAEXTENDEDTRACKINGMANAGER_T262318595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaExtendedTrackingManager
struct  VuforiaExtendedTrackingManager_t262318595  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAEXTENDEDTRACKINGMANAGER_T262318595_H
#ifndef VUMARKTARGETIMPL_T1052843922_H
#define VUMARKTARGETIMPL_T1052843922_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTargetImpl
struct  VuMarkTargetImpl_t1052843922  : public RuntimeObject
{
public:
	// Vuforia.VuMarkTemplateImpl Vuforia.VuMarkTargetImpl::mVuMarkTemplate
	VuMarkTemplateImpl_t667343433 * ___mVuMarkTemplate_0;
	// Vuforia.InstanceIdImpl Vuforia.VuMarkTargetImpl::mInstanceId
	InstanceIdImpl_t2824054591 * ___mInstanceId_1;
	// System.Int32 Vuforia.VuMarkTargetImpl::mTargetId
	int32_t ___mTargetId_2;

public:
	inline static int32_t get_offset_of_mVuMarkTemplate_0() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t1052843922, ___mVuMarkTemplate_0)); }
	inline VuMarkTemplateImpl_t667343433 * get_mVuMarkTemplate_0() const { return ___mVuMarkTemplate_0; }
	inline VuMarkTemplateImpl_t667343433 ** get_address_of_mVuMarkTemplate_0() { return &___mVuMarkTemplate_0; }
	inline void set_mVuMarkTemplate_0(VuMarkTemplateImpl_t667343433 * value)
	{
		___mVuMarkTemplate_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuMarkTemplate_0), value);
	}

	inline static int32_t get_offset_of_mInstanceId_1() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t1052843922, ___mInstanceId_1)); }
	inline InstanceIdImpl_t2824054591 * get_mInstanceId_1() const { return ___mInstanceId_1; }
	inline InstanceIdImpl_t2824054591 ** get_address_of_mInstanceId_1() { return &___mInstanceId_1; }
	inline void set_mInstanceId_1(InstanceIdImpl_t2824054591 * value)
	{
		___mInstanceId_1 = value;
		Il2CppCodeGenWriteBarrier((&___mInstanceId_1), value);
	}

	inline static int32_t get_offset_of_mTargetId_2() { return static_cast<int32_t>(offsetof(VuMarkTargetImpl_t1052843922, ___mTargetId_2)); }
	inline int32_t get_mTargetId_2() const { return ___mTargetId_2; }
	inline int32_t* get_address_of_mTargetId_2() { return &___mTargetId_2; }
	inline void set_mTargetId_2(int32_t value)
	{
		___mTargetId_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTARGETIMPL_T1052843922_H
#ifndef TRACKERCONFIGURATION_T588160133_H
#define TRACKERCONFIGURATION_T588160133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/TrackerConfiguration
struct  TrackerConfiguration_t588160133  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/TrackerConfiguration::autoInitTracker
	bool ___autoInitTracker_0;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/TrackerConfiguration::autoStartTracker
	bool ___autoStartTracker_1;

public:
	inline static int32_t get_offset_of_autoInitTracker_0() { return static_cast<int32_t>(offsetof(TrackerConfiguration_t588160133, ___autoInitTracker_0)); }
	inline bool get_autoInitTracker_0() const { return ___autoInitTracker_0; }
	inline bool* get_address_of_autoInitTracker_0() { return &___autoInitTracker_0; }
	inline void set_autoInitTracker_0(bool value)
	{
		___autoInitTracker_0 = value;
	}

	inline static int32_t get_offset_of_autoStartTracker_1() { return static_cast<int32_t>(offsetof(TrackerConfiguration_t588160133, ___autoStartTracker_1)); }
	inline bool get_autoStartTracker_1() const { return ___autoStartTracker_1; }
	inline bool* get_address_of_autoStartTracker_1() { return &___autoStartTracker_1; }
	inline void set_autoStartTracker_1(bool value)
	{
		___autoStartTracker_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKERCONFIGURATION_T588160133_H
#ifndef DATABASELOADCONFIGURATION_T3815674388_H
#define DATABASELOADCONFIGURATION_T3815674388_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration
struct  DatabaseLoadConfiguration_t3815674388  : public RuntimeObject
{
public:
	// System.String[] Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration::dataSetsToLoad
	StringU5BU5D_t1281789340* ___dataSetsToLoad_0;
	// System.String[] Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration::dataSetsToActivate
	StringU5BU5D_t1281789340* ___dataSetsToActivate_1;

public:
	inline static int32_t get_offset_of_dataSetsToLoad_0() { return static_cast<int32_t>(offsetof(DatabaseLoadConfiguration_t3815674388, ___dataSetsToLoad_0)); }
	inline StringU5BU5D_t1281789340* get_dataSetsToLoad_0() const { return ___dataSetsToLoad_0; }
	inline StringU5BU5D_t1281789340** get_address_of_dataSetsToLoad_0() { return &___dataSetsToLoad_0; }
	inline void set_dataSetsToLoad_0(StringU5BU5D_t1281789340* value)
	{
		___dataSetsToLoad_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetsToLoad_0), value);
	}

	inline static int32_t get_offset_of_dataSetsToActivate_1() { return static_cast<int32_t>(offsetof(DatabaseLoadConfiguration_t3815674388, ___dataSetsToActivate_1)); }
	inline StringU5BU5D_t1281789340* get_dataSetsToActivate_1() const { return ___dataSetsToActivate_1; }
	inline StringU5BU5D_t1281789340** get_address_of_dataSetsToActivate_1() { return &___dataSetsToActivate_1; }
	inline void set_dataSetsToActivate_1(StringU5BU5D_t1281789340* value)
	{
		___dataSetsToActivate_1 = value;
		Il2CppCodeGenWriteBarrier((&___dataSetsToActivate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABASELOADCONFIGURATION_T3815674388_H
#ifndef DEVICE_T64880687_H
#define DEVICE_T64880687_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Device
struct  Device_t64880687  : public RuntimeObject
{
public:

public:
};

struct Device_t64880687_StaticFields
{
public:
	// Vuforia.Device Vuforia.Device::mInstance
	Device_t64880687 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(Device_t64880687_StaticFields, ___mInstance_0)); }
	inline Device_t64880687 * get_mInstance_0() const { return ___mInstance_0; }
	inline Device_t64880687 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(Device_t64880687 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICE_T64880687_H
#ifndef PLAYMODEUNITYPLAYER_T3763348594_H
#define PLAYMODEUNITYPLAYER_T3763348594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeUnityPlayer
struct  PlayModeUnityPlayer_t3763348594  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEUNITYPLAYER_T3763348594_H
#ifndef NULLUNITYPLAYER_T2922069181_H
#define NULLUNITYPLAYER_T2922069181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullUnityPlayer
struct  NullUnityPlayer_t2922069181  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLUNITYPLAYER_T2922069181_H
#ifndef IOSCAMRECOVERINGHELPER_T4035151671_H
#define IOSCAMRECOVERINGHELPER_T4035151671_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.IOSCamRecoveringHelper
struct  IOSCamRecoveringHelper_t4035151671  : public RuntimeObject
{
public:

public:
};

struct IOSCamRecoveringHelper_t4035151671_StaticFields
{
public:
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sHasJustResumed
	bool ___sHasJustResumed_0;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sCheckFailedFrameAfterResume
	bool ___sCheckFailedFrameAfterResume_1;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sCheckedFailedFrameCounter
	int32_t ___sCheckedFailedFrameCounter_2;
	// System.Boolean Vuforia.IOSCamRecoveringHelper::sWaitToRecoverCameraRestart
	bool ___sWaitToRecoverCameraRestart_3;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sWaitedFrameRecoverCounter
	int32_t ___sWaitedFrameRecoverCounter_4;
	// System.Int32 Vuforia.IOSCamRecoveringHelper::sRecoveryAttemptCounter
	int32_t ___sRecoveryAttemptCounter_5;

public:
	inline static int32_t get_offset_of_sHasJustResumed_0() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t4035151671_StaticFields, ___sHasJustResumed_0)); }
	inline bool get_sHasJustResumed_0() const { return ___sHasJustResumed_0; }
	inline bool* get_address_of_sHasJustResumed_0() { return &___sHasJustResumed_0; }
	inline void set_sHasJustResumed_0(bool value)
	{
		___sHasJustResumed_0 = value;
	}

	inline static int32_t get_offset_of_sCheckFailedFrameAfterResume_1() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t4035151671_StaticFields, ___sCheckFailedFrameAfterResume_1)); }
	inline bool get_sCheckFailedFrameAfterResume_1() const { return ___sCheckFailedFrameAfterResume_1; }
	inline bool* get_address_of_sCheckFailedFrameAfterResume_1() { return &___sCheckFailedFrameAfterResume_1; }
	inline void set_sCheckFailedFrameAfterResume_1(bool value)
	{
		___sCheckFailedFrameAfterResume_1 = value;
	}

	inline static int32_t get_offset_of_sCheckedFailedFrameCounter_2() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t4035151671_StaticFields, ___sCheckedFailedFrameCounter_2)); }
	inline int32_t get_sCheckedFailedFrameCounter_2() const { return ___sCheckedFailedFrameCounter_2; }
	inline int32_t* get_address_of_sCheckedFailedFrameCounter_2() { return &___sCheckedFailedFrameCounter_2; }
	inline void set_sCheckedFailedFrameCounter_2(int32_t value)
	{
		___sCheckedFailedFrameCounter_2 = value;
	}

	inline static int32_t get_offset_of_sWaitToRecoverCameraRestart_3() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t4035151671_StaticFields, ___sWaitToRecoverCameraRestart_3)); }
	inline bool get_sWaitToRecoverCameraRestart_3() const { return ___sWaitToRecoverCameraRestart_3; }
	inline bool* get_address_of_sWaitToRecoverCameraRestart_3() { return &___sWaitToRecoverCameraRestart_3; }
	inline void set_sWaitToRecoverCameraRestart_3(bool value)
	{
		___sWaitToRecoverCameraRestart_3 = value;
	}

	inline static int32_t get_offset_of_sWaitedFrameRecoverCounter_4() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t4035151671_StaticFields, ___sWaitedFrameRecoverCounter_4)); }
	inline int32_t get_sWaitedFrameRecoverCounter_4() const { return ___sWaitedFrameRecoverCounter_4; }
	inline int32_t* get_address_of_sWaitedFrameRecoverCounter_4() { return &___sWaitedFrameRecoverCounter_4; }
	inline void set_sWaitedFrameRecoverCounter_4(int32_t value)
	{
		___sWaitedFrameRecoverCounter_4 = value;
	}

	inline static int32_t get_offset_of_sRecoveryAttemptCounter_5() { return static_cast<int32_t>(offsetof(IOSCamRecoveringHelper_t4035151671_StaticFields, ___sRecoveryAttemptCounter_5)); }
	inline int32_t get_sRecoveryAttemptCounter_5() const { return ___sRecoveryAttemptCounter_5; }
	inline int32_t* get_address_of_sRecoveryAttemptCounter_5() { return &___sRecoveryAttemptCounter_5; }
	inline void set_sRecoveryAttemptCounter_5(int32_t value)
	{
		___sRecoveryAttemptCounter_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSCAMRECOVERINGHELPER_T4035151671_H
#ifndef MESHUTILS_T922322213_H
#define MESHUTILS_T922322213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MeshUtils
struct  MeshUtils_t922322213  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHUTILS_T922322213_H
#ifndef UNITYPLAYER_T3127875071_H
#define UNITYPLAYER_T3127875071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityPlayer
struct  UnityPlayer_t3127875071  : public RuntimeObject
{
public:

public:
};

struct UnityPlayer_t3127875071_StaticFields
{
public:
	// Vuforia.IUnityPlayer Vuforia.UnityPlayer::sPlayer
	RuntimeObject* ___sPlayer_0;

public:
	inline static int32_t get_offset_of_sPlayer_0() { return static_cast<int32_t>(offsetof(UnityPlayer_t3127875071_StaticFields, ___sPlayer_0)); }
	inline RuntimeObject* get_sPlayer_0() const { return ___sPlayer_0; }
	inline RuntimeObject** get_address_of_sPlayer_0() { return &___sPlayer_0; }
	inline void set_sPlayer_0(RuntimeObject* value)
	{
		___sPlayer_0 = value;
		Il2CppCodeGenWriteBarrier((&___sPlayer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYPLAYER_T3127875071_H
#ifndef NULLHIDEEXCESSAREACLIPPING_T465635818_H
#define NULLHIDEEXCESSAREACLIPPING_T465635818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullHideExcessAreaClipping
struct  NullHideExcessAreaClipping_t465635818  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLHIDEEXCESSAREACLIPPING_T465635818_H
#ifndef UNITYCAMERAEXTENSIONS_T3394190193_H
#define UNITYCAMERAEXTENSIONS_T3394190193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.UnityCameraExtensions
struct  UnityCameraExtensions_t3394190193  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYCAMERAEXTENSIONS_T3394190193_H
#ifndef TRACKER_T2709586299_H
#define TRACKER_T2709586299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Tracker
struct  Tracker_t2709586299  : public RuntimeObject
{
public:
	// System.Boolean Vuforia.Tracker::<IsActive>k__BackingField
	bool ___U3CIsActiveU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CIsActiveU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Tracker_t2709586299, ___U3CIsActiveU3Ek__BackingField_0)); }
	inline bool get_U3CIsActiveU3Ek__BackingField_0() const { return ___U3CIsActiveU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CIsActiveU3Ek__BackingField_0() { return &___U3CIsActiveU3Ek__BackingField_0; }
	inline void set_U3CIsActiveU3Ek__BackingField_0(bool value)
	{
		___U3CIsActiveU3Ek__BackingField_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKER_T2709586299_H
#ifndef NULLHOLOLENSAPIABSTRACTION_T2968904009_H
#define NULLHOLOLENSAPIABSTRACTION_T2968904009_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullHoloLensApiAbstraction
struct  NullHoloLensApiAbstraction_t2968904009  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLHOLOLENSAPIABSTRACTION_T2968904009_H
#ifndef CAMERADEVICE_T960297568_H
#define CAMERADEVICE_T960297568_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice
struct  CameraDevice_t960297568  : public RuntimeObject
{
public:

public:
};

struct CameraDevice_t960297568_StaticFields
{
public:
	// Vuforia.CameraDevice Vuforia.CameraDevice::mInstance
	CameraDevice_t960297568 * ___mInstance_0;

public:
	inline static int32_t get_offset_of_mInstance_0() { return static_cast<int32_t>(offsetof(CameraDevice_t960297568_StaticFields, ___mInstance_0)); }
	inline CameraDevice_t960297568 * get_mInstance_0() const { return ___mInstance_0; }
	inline CameraDevice_t960297568 ** get_address_of_mInstance_0() { return &___mInstance_0; }
	inline void set_mInstance_0(CameraDevice_t960297568 * value)
	{
		___mInstance_0 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICE_T960297568_H
#ifndef ATTRIBUTE_T861562559_H
#define ATTRIBUTE_T861562559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t861562559  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T861562559_H
#ifndef ANALYTICSSESSIONINFO_T2322308579_H
#define ANALYTICSSESSIONINFO_T2322308579_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo
struct  AnalyticsSessionInfo_t2322308579  : public RuntimeObject
{
public:

public:
};

struct AnalyticsSessionInfo_t2322308579_StaticFields
{
public:
	// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged UnityEngine.Analytics.AnalyticsSessionInfo::sessionStateChanged
	SessionStateChanged_t3163629820 * ___sessionStateChanged_0;

public:
	inline static int32_t get_offset_of_sessionStateChanged_0() { return static_cast<int32_t>(offsetof(AnalyticsSessionInfo_t2322308579_StaticFields, ___sessionStateChanged_0)); }
	inline SessionStateChanged_t3163629820 * get_sessionStateChanged_0() const { return ___sessionStateChanged_0; }
	inline SessionStateChanged_t3163629820 ** get_address_of_sessionStateChanged_0() { return &___sessionStateChanged_0; }
	inline void set_sessionStateChanged_0(SessionStateChanged_t3163629820 * value)
	{
		___sessionStateChanged_0 = value;
		Il2CppCodeGenWriteBarrier((&___sessionStateChanged_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONINFO_T2322308579_H
#ifndef WEBREQUESTUTILS_T3541624225_H
#define WEBREQUESTUTILS_T3541624225_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.WebRequestUtils
struct  WebRequestUtils_t3541624225  : public RuntimeObject
{
public:

public:
};

struct WebRequestUtils_t3541624225_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_t3657309853 * ___domainRegex_0;

public:
	inline static int32_t get_offset_of_domainRegex_0() { return static_cast<int32_t>(offsetof(WebRequestUtils_t3541624225_StaticFields, ___domainRegex_0)); }
	inline Regex_t3657309853 * get_domainRegex_0() const { return ___domainRegex_0; }
	inline Regex_t3657309853 ** get_address_of_domainRegex_0() { return &___domainRegex_0; }
	inline void set_domainRegex_0(Regex_t3657309853 * value)
	{
		___domainRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___domainRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTUTILS_T3541624225_H
#ifndef TRACKABLEIMPL_T3595316917_H
#define TRACKABLEIMPL_T3595316917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableImpl
struct  TrackableImpl_t3595316917  : public RuntimeObject
{
public:
	// System.String Vuforia.TrackableImpl::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Int32 Vuforia.TrackableImpl::<ID>k__BackingField
	int32_t ___U3CIDU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TrackableImpl_t3595316917, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CIDU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TrackableImpl_t3595316917, ___U3CIDU3Ek__BackingField_1)); }
	inline int32_t get_U3CIDU3Ek__BackingField_1() const { return ___U3CIDU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIDU3Ek__BackingField_1() { return &___U3CIDU3Ek__BackingField_1; }
	inline void set_U3CIDU3Ek__BackingField_1(int32_t value)
	{
		___U3CIDU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEIMPL_T3595316917_H
#ifndef ARCONTROLLER_T116632334_H
#define ARCONTROLLER_T116632334_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController
struct  ARController_t116632334  : public RuntimeObject
{
public:
	// Vuforia.VuforiaAbstractBehaviour Vuforia.ARController::mVuforiaBehaviour
	VuforiaAbstractBehaviour_t3510878193 * ___mVuforiaBehaviour_0;

public:
	inline static int32_t get_offset_of_mVuforiaBehaviour_0() { return static_cast<int32_t>(offsetof(ARController_t116632334, ___mVuforiaBehaviour_0)); }
	inline VuforiaAbstractBehaviour_t3510878193 * get_mVuforiaBehaviour_0() const { return ___mVuforiaBehaviour_0; }
	inline VuforiaAbstractBehaviour_t3510878193 ** get_address_of_mVuforiaBehaviour_0() { return &___mVuforiaBehaviour_0; }
	inline void set_mVuforiaBehaviour_0(VuforiaAbstractBehaviour_t3510878193 * value)
	{
		___mVuforiaBehaviour_0 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARCONTROLLER_T116632334_H
#ifndef VUMARKMANAGER_T2982459596_H
#define VUMARKMANAGER_T2982459596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkManager
struct  VuMarkManager_t2982459596  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKMANAGER_T2982459596_H
#ifndef U3CU3EC__DISPLAYCLASS11_0_T2669575632_H
#define U3CU3EC__DISPLAYCLASS11_0_T2669575632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ARController/<>c__DisplayClass11_0
struct  U3CU3Ec__DisplayClass11_0_t2669575632  : public RuntimeObject
{
public:
	// Vuforia.ARController Vuforia.ARController/<>c__DisplayClass11_0::controller
	ARController_t116632334 * ___controller_0;

public:
	inline static int32_t get_offset_of_controller_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t2669575632, ___controller_0)); }
	inline ARController_t116632334 * get_controller_0() const { return ___controller_0; }
	inline ARController_t116632334 ** get_address_of_controller_0() { return &___controller_0; }
	inline void set_controller_0(ARController_t116632334 * value)
	{
		___controller_0 = value;
		Il2CppCodeGenWriteBarrier((&___controller_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS11_0_T2669575632_H
#ifndef VEC2I_T3527036565_H
#define VEC2I_T3527036565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/Vec2I
#pragma pack(push, tp, 1)
struct  Vec2I_t3527036565 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::x
	int32_t ___x_0;
	// System.Int32 Vuforia.VuforiaRenderer/Vec2I::y
	int32_t ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vec2I_t3527036565, ___y_1)); }
	inline int32_t get_y_1() const { return ___y_1; }
	inline int32_t* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(int32_t value)
	{
		___y_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VEC2I_T3527036565_H
#ifndef VIDEOMODEDATA_T2066817255_H
#define VIDEOMODEDATA_T2066817255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/VideoModeData
#pragma pack(push, tp, 1)
struct  VideoModeData_t2066817255 
{
public:
	// System.Int32 Vuforia.CameraDevice/VideoModeData::width
	int32_t ___width_0;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::height
	int32_t ___height_1;
	// System.Single Vuforia.CameraDevice/VideoModeData::frameRate
	float ___frameRate_2;
	// System.Int32 Vuforia.CameraDevice/VideoModeData::unused
	int32_t ___unused_3;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_frameRate_2() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___frameRate_2)); }
	inline float get_frameRate_2() const { return ___frameRate_2; }
	inline float* get_address_of_frameRate_2() { return &___frameRate_2; }
	inline void set_frameRate_2(float value)
	{
		___frameRate_2 = value;
	}

	inline static int32_t get_offset_of_unused_3() { return static_cast<int32_t>(offsetof(VideoModeData_t2066817255, ___unused_3)); }
	inline int32_t get_unused_3() const { return ___unused_3; }
	inline int32_t* get_address_of_unused_3() { return &___unused_3; }
	inline void set_unused_3(int32_t value)
	{
		___unused_3 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOMODEDATA_T2066817255_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef FACTORYSETTER_T2184571743_H
#define FACTORYSETTER_T2184571743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.FactorySetter
struct  FactorySetter_t2184571743  : public Attribute_t861562559
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYSETTER_T2184571743_H
#ifndef EYEWEARDEVICE_T3223385723_H
#define EYEWEARDEVICE_T3223385723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.EyewearDevice
struct  EyewearDevice_t3223385723  : public Device_t64880687
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARDEVICE_T3223385723_H
#ifndef VUMARKMANAGERIMPL_T1545617414_H
#define VUMARKMANAGERIMPL_T1545617414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkManagerImpl
struct  VuMarkManagerImpl_t1545617414  : public VuMarkManager_t2982459596
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour>> Vuforia.VuMarkManagerImpl::mBehaviours
	Dictionary_2_t941439618 * ___mBehaviours_0;
	// System.Collections.Generic.List`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManagerImpl::mActiveVuMarkTargets
	List_1_t2601648545 * ___mActiveVuMarkTargets_1;
	// System.Collections.Generic.List`1<Vuforia.VuMarkAbstractBehaviour> Vuforia.VuMarkManagerImpl::mDestroyedBehaviours
	List_1_t2052726287 * ___mDestroyedBehaviours_2;
	// System.Action`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManagerImpl::mOnVuMarkDetected
	Action_1_t1302041398 * ___mOnVuMarkDetected_3;
	// System.Action`1<Vuforia.VuMarkTarget> Vuforia.VuMarkManagerImpl::mOnVuMarkLost
	Action_1_t1302041398 * ___mOnVuMarkLost_4;
	// System.Action`1<Vuforia.VuMarkAbstractBehaviour> Vuforia.VuMarkManagerImpl::mOnVuMarkBehaviourDetected
	Action_1_t753119140 * ___mOnVuMarkBehaviourDetected_5;

public:
	inline static int32_t get_offset_of_mBehaviours_0() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t1545617414, ___mBehaviours_0)); }
	inline Dictionary_2_t941439618 * get_mBehaviours_0() const { return ___mBehaviours_0; }
	inline Dictionary_2_t941439618 ** get_address_of_mBehaviours_0() { return &___mBehaviours_0; }
	inline void set_mBehaviours_0(Dictionary_2_t941439618 * value)
	{
		___mBehaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___mBehaviours_0), value);
	}

	inline static int32_t get_offset_of_mActiveVuMarkTargets_1() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t1545617414, ___mActiveVuMarkTargets_1)); }
	inline List_1_t2601648545 * get_mActiveVuMarkTargets_1() const { return ___mActiveVuMarkTargets_1; }
	inline List_1_t2601648545 ** get_address_of_mActiveVuMarkTargets_1() { return &___mActiveVuMarkTargets_1; }
	inline void set_mActiveVuMarkTargets_1(List_1_t2601648545 * value)
	{
		___mActiveVuMarkTargets_1 = value;
		Il2CppCodeGenWriteBarrier((&___mActiveVuMarkTargets_1), value);
	}

	inline static int32_t get_offset_of_mDestroyedBehaviours_2() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t1545617414, ___mDestroyedBehaviours_2)); }
	inline List_1_t2052726287 * get_mDestroyedBehaviours_2() const { return ___mDestroyedBehaviours_2; }
	inline List_1_t2052726287 ** get_address_of_mDestroyedBehaviours_2() { return &___mDestroyedBehaviours_2; }
	inline void set_mDestroyedBehaviours_2(List_1_t2052726287 * value)
	{
		___mDestroyedBehaviours_2 = value;
		Il2CppCodeGenWriteBarrier((&___mDestroyedBehaviours_2), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkDetected_3() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t1545617414, ___mOnVuMarkDetected_3)); }
	inline Action_1_t1302041398 * get_mOnVuMarkDetected_3() const { return ___mOnVuMarkDetected_3; }
	inline Action_1_t1302041398 ** get_address_of_mOnVuMarkDetected_3() { return &___mOnVuMarkDetected_3; }
	inline void set_mOnVuMarkDetected_3(Action_1_t1302041398 * value)
	{
		___mOnVuMarkDetected_3 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkDetected_3), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkLost_4() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t1545617414, ___mOnVuMarkLost_4)); }
	inline Action_1_t1302041398 * get_mOnVuMarkLost_4() const { return ___mOnVuMarkLost_4; }
	inline Action_1_t1302041398 ** get_address_of_mOnVuMarkLost_4() { return &___mOnVuMarkLost_4; }
	inline void set_mOnVuMarkLost_4(Action_1_t1302041398 * value)
	{
		___mOnVuMarkLost_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkLost_4), value);
	}

	inline static int32_t get_offset_of_mOnVuMarkBehaviourDetected_5() { return static_cast<int32_t>(offsetof(VuMarkManagerImpl_t1545617414, ___mOnVuMarkBehaviourDetected_5)); }
	inline Action_1_t753119140 * get_mOnVuMarkBehaviourDetected_5() const { return ___mOnVuMarkBehaviourDetected_5; }
	inline Action_1_t753119140 ** get_address_of_mOnVuMarkBehaviourDetected_5() { return &___mOnVuMarkBehaviourDetected_5; }
	inline void set_mOnVuMarkBehaviourDetected_5(Action_1_t753119140 * value)
	{
		___mOnVuMarkBehaviourDetected_5 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVuMarkBehaviourDetected_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKMANAGERIMPL_T1545617414_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT64_T3736567304_H
#define INT64_T3736567304_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int64
struct  Int64_t3736567304 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int64_t3736567304, ___m_value_2)); }
	inline int64_t get_m_value_2() const { return ___m_value_2; }
	inline int64_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int64_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT64_T3736567304_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DEVICETRACKER_T2315692373_H
#define DEVICETRACKER_T2315692373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTracker
struct  DeviceTracker_t2315692373  : public Tracker_t2709586299
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKER_T2315692373_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_13;

public:
	inline static int32_t get_offset_of_m_value_13() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_13)); }
	inline double get_m_value_13() const { return ___m_value_13; }
	inline double* get_address_of_m_value_13() { return &___m_value_13; }
	inline void set_m_value_13(double value)
	{
		___m_value_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef QUATERNION_T2301928331_H
#define QUATERNION_T2301928331_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t2301928331 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t2301928331_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t2301928331  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t2301928331_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t2301928331  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t2301928331 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t2301928331  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T2301928331_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef VECTOR4_T3319028937_H
#define VECTOR4_T3319028937_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t3319028937 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t3319028937, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t3319028937_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t3319028937  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t3319028937  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t3319028937  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t3319028937  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___zeroVector_5)); }
	inline Vector4_t3319028937  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t3319028937 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t3319028937  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___oneVector_6)); }
	inline Vector4_t3319028937  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t3319028937 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t3319028937  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t3319028937  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t3319028937 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t3319028937  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t3319028937_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t3319028937  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t3319028937 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t3319028937  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T3319028937_H
#ifndef CAMERADIRECTION_T637748435_H
#define CAMERADIRECTION_T637748435_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDirection
struct  CameraDirection_t637748435 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDirection_t637748435, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADIRECTION_T637748435_H
#ifndef VIDEOBACKGROUNDREFLECTION_T736962841_H
#define VIDEOBACKGROUNDREFLECTION_T736962841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoBackgroundReflection
struct  VideoBackgroundReflection_t736962841 
{
public:
	// System.Int32 Vuforia.VuforiaRenderer/VideoBackgroundReflection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VideoBackgroundReflection_t736962841, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDREFLECTION_T736962841_H
#ifndef RECONSTRUCTIONIMPL_T3350922794_H
#define RECONSTRUCTIONIMPL_T3350922794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionImpl
struct  ReconstructionImpl_t3350922794  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ReconstructionImpl::mNativeReconstructionPtr
	intptr_t ___mNativeReconstructionPtr_0;
	// System.Boolean Vuforia.ReconstructionImpl::mMaximumAreaIsSet
	bool ___mMaximumAreaIsSet_1;
	// UnityEngine.Rect Vuforia.ReconstructionImpl::mMaximumArea
	Rect_t2360479859  ___mMaximumArea_2;
	// System.Single Vuforia.ReconstructionImpl::mNavMeshPadding
	float ___mNavMeshPadding_3;
	// System.Boolean Vuforia.ReconstructionImpl::mNavMeshUpdatesEnabled
	bool ___mNavMeshUpdatesEnabled_4;

public:
	inline static int32_t get_offset_of_mNativeReconstructionPtr_0() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t3350922794, ___mNativeReconstructionPtr_0)); }
	inline intptr_t get_mNativeReconstructionPtr_0() const { return ___mNativeReconstructionPtr_0; }
	inline intptr_t* get_address_of_mNativeReconstructionPtr_0() { return &___mNativeReconstructionPtr_0; }
	inline void set_mNativeReconstructionPtr_0(intptr_t value)
	{
		___mNativeReconstructionPtr_0 = value;
	}

	inline static int32_t get_offset_of_mMaximumAreaIsSet_1() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t3350922794, ___mMaximumAreaIsSet_1)); }
	inline bool get_mMaximumAreaIsSet_1() const { return ___mMaximumAreaIsSet_1; }
	inline bool* get_address_of_mMaximumAreaIsSet_1() { return &___mMaximumAreaIsSet_1; }
	inline void set_mMaximumAreaIsSet_1(bool value)
	{
		___mMaximumAreaIsSet_1 = value;
	}

	inline static int32_t get_offset_of_mMaximumArea_2() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t3350922794, ___mMaximumArea_2)); }
	inline Rect_t2360479859  get_mMaximumArea_2() const { return ___mMaximumArea_2; }
	inline Rect_t2360479859 * get_address_of_mMaximumArea_2() { return &___mMaximumArea_2; }
	inline void set_mMaximumArea_2(Rect_t2360479859  value)
	{
		___mMaximumArea_2 = value;
	}

	inline static int32_t get_offset_of_mNavMeshPadding_3() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t3350922794, ___mNavMeshPadding_3)); }
	inline float get_mNavMeshPadding_3() const { return ___mNavMeshPadding_3; }
	inline float* get_address_of_mNavMeshPadding_3() { return &___mNavMeshPadding_3; }
	inline void set_mNavMeshPadding_3(float value)
	{
		___mNavMeshPadding_3 = value;
	}

	inline static int32_t get_offset_of_mNavMeshUpdatesEnabled_4() { return static_cast<int32_t>(offsetof(ReconstructionImpl_t3350922794, ___mNavMeshUpdatesEnabled_4)); }
	inline bool get_mNavMeshUpdatesEnabled_4() const { return ___mNavMeshUpdatesEnabled_4; }
	inline bool* get_address_of_mNavMeshUpdatesEnabled_4() { return &___mNavMeshUpdatesEnabled_4; }
	inline void set_mNavMeshUpdatesEnabled_4(bool value)
	{
		___mNavMeshUpdatesEnabled_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONIMPL_T3350922794_H
#ifndef CLIPPING_MODE_T259981078_H
#define CLIPPING_MODE_T259981078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE
struct  CLIPPING_MODE_t259981078 
{
public:
	// System.Int32 Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CLIPPING_MODE_t259981078, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPING_MODE_T259981078_H
#ifndef OBJECTTARGETIMPL_T3614635090_H
#define OBJECTTARGETIMPL_T3614635090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetImpl
struct  ObjectTargetImpl_t3614635090  : public TrackableImpl_t3595316917
{
public:
	// UnityEngine.Vector3 Vuforia.ObjectTargetImpl::mSize
	Vector3_t3722313464  ___mSize_2;
	// Vuforia.DataSetImpl Vuforia.ObjectTargetImpl::mDataSet
	DataSetImpl_t2094717509 * ___mDataSet_3;

public:
	inline static int32_t get_offset_of_mSize_2() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_t3614635090, ___mSize_2)); }
	inline Vector3_t3722313464  get_mSize_2() const { return ___mSize_2; }
	inline Vector3_t3722313464 * get_address_of_mSize_2() { return &___mSize_2; }
	inline void set_mSize_2(Vector3_t3722313464  value)
	{
		___mSize_2 = value;
	}

	inline static int32_t get_offset_of_mDataSet_3() { return static_cast<int32_t>(offsetof(ObjectTargetImpl_t3614635090, ___mDataSet_3)); }
	inline DataSetImpl_t2094717509 * get_mDataSet_3() const { return ___mDataSet_3; }
	inline DataSetImpl_t2094717509 ** get_address_of_mDataSet_3() { return &___mDataSet_3; }
	inline void set_mDataSet_3(DataSetImpl_t2094717509 * value)
	{
		___mDataSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETIMPL_T3614635090_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef CAMERADEVICEMODE_T2478715656_H
#define CAMERADEVICEMODE_T2478715656_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraDevice/CameraDeviceMode
struct  CameraDeviceMode_t2478715656 
{
public:
	// System.Int32 Vuforia.CameraDevice/CameraDeviceMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraDeviceMode_t2478715656, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADEVICEMODE_T2478715656_H
#ifndef MODE_T3830573374_H
#define MODE_T3830573374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.Device/Mode
struct  Mode_t3830573374 
{
public:
	// System.Int32 Vuforia.Device/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3830573374, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3830573374_H
#ifndef VIEWERBUTTONTYPE_T3221680132_H
#define VIEWERBUTTONTYPE_T3221680132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerButtonType
struct  ViewerButtonType_t3221680132 
{
public:
	// System.Int32 Vuforia.ViewerButtonType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewerButtonType_t3221680132, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERBUTTONTYPE_T3221680132_H
#ifndef VIEWERTRAYALIGNMENT_T2810797062_H
#define VIEWERTRAYALIGNMENT_T2810797062_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerTrayAlignment
struct  ViewerTrayAlignment_t2810797062 
{
public:
	// System.Int32 Vuforia.ViewerTrayAlignment::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ViewerTrayAlignment_t2810797062, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERTRAYALIGNMENT_T2810797062_H
#ifndef VIDEOTEXTUREINFO_T1805965052_H
#define VIDEOTEXTUREINFO_T1805965052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaRenderer/VideoTextureInfo
#pragma pack(push, tp, 1)
struct  VideoTextureInfo_t1805965052 
{
public:
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::textureSize
	Vec2I_t3527036565  ___textureSize_0;
	// Vuforia.VuforiaRenderer/Vec2I Vuforia.VuforiaRenderer/VideoTextureInfo::imageSize
	Vec2I_t3527036565  ___imageSize_1;

public:
	inline static int32_t get_offset_of_textureSize_0() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t1805965052, ___textureSize_0)); }
	inline Vec2I_t3527036565  get_textureSize_0() const { return ___textureSize_0; }
	inline Vec2I_t3527036565 * get_address_of_textureSize_0() { return &___textureSize_0; }
	inline void set_textureSize_0(Vec2I_t3527036565  value)
	{
		___textureSize_0 = value;
	}

	inline static int32_t get_offset_of_imageSize_1() { return static_cast<int32_t>(offsetof(VideoTextureInfo_t1805965052, ___imageSize_1)); }
	inline Vec2I_t3527036565  get_imageSize_1() const { return ___imageSize_1; }
	inline Vec2I_t3527036565 * get_address_of_imageSize_1() { return &___imageSize_1; }
	inline void set_imageSize_1(Vec2I_t3527036565  value)
	{
		___imageSize_1 = value;
	}
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOTEXTUREINFO_T1805965052_H
#ifndef INSTANCEIDTYPE_T420283664_H
#define INSTANCEIDTYPE_T420283664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdType
struct  InstanceIdType_t420283664 
{
public:
	// System.Int32 Vuforia.InstanceIdType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(InstanceIdType_t420283664, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDTYPE_T420283664_H
#ifndef WORLDCENTERMODE_T3672819471_H
#define WORLDCENTERMODE_T3672819471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaARController/WorldCenterMode
struct  WorldCenterMode_t3672819471 
{
public:
	// System.Int32 Vuforia.VuforiaARController/WorldCenterMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(WorldCenterMode_t3672819471, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WORLDCENTERMODE_T3672819471_H
#ifndef PLAYABLEHANDLE_T1095853803_H
#define PLAYABLEHANDLE_T1095853803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t1095853803 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t1095853803, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T1095853803_H
#ifndef VIEW_T3879626884_H
#define VIEW_T3879626884_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.View
struct  View_t3879626884 
{
public:
	// System.Int32 Vuforia.View::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(View_t3879626884, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEW_T3879626884_H
#ifndef STATUS_T1100905814_H
#define STATUS_T1100905814_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour/Status
struct  Status_t1100905814 
{
public:
	// System.Int32 Vuforia.TrackableBehaviour/Status::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Status_t1100905814, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATUS_T1100905814_H
#ifndef DEDICATEDEYEWEARDEVICE_T2070131990_H
#define DEDICATEDEYEWEARDEVICE_T2070131990_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DedicatedEyewearDevice
struct  DedicatedEyewearDevice_t2070131990  : public EyewearDevice_t3223385723
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEDICATEDEYEWEARDEVICE_T2070131990_H
#ifndef DEVICETRACKINGMANAGER_T3849131975_H
#define DEVICETRACKINGMANAGER_T3849131975_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTrackingManager
struct  DeviceTrackingManager_t3849131975  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Vuforia.DeviceTrackingManager::mDeviceTrackerPositonOffset
	Vector3_t3722313464  ___mDeviceTrackerPositonOffset_0;
	// UnityEngine.Quaternion Vuforia.DeviceTrackingManager::mDeviceTrackerRotationOffset
	Quaternion_t2301928331  ___mDeviceTrackerRotationOffset_1;
	// System.Action Vuforia.DeviceTrackingManager::mBeforeDevicePoseUpdated
	Action_t1264377477 * ___mBeforeDevicePoseUpdated_2;
	// System.Action Vuforia.DeviceTrackingManager::mAfterDevicePoseUpdated
	Action_t1264377477 * ___mAfterDevicePoseUpdated_3;

public:
	inline static int32_t get_offset_of_mDeviceTrackerPositonOffset_0() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t3849131975, ___mDeviceTrackerPositonOffset_0)); }
	inline Vector3_t3722313464  get_mDeviceTrackerPositonOffset_0() const { return ___mDeviceTrackerPositonOffset_0; }
	inline Vector3_t3722313464 * get_address_of_mDeviceTrackerPositonOffset_0() { return &___mDeviceTrackerPositonOffset_0; }
	inline void set_mDeviceTrackerPositonOffset_0(Vector3_t3722313464  value)
	{
		___mDeviceTrackerPositonOffset_0 = value;
	}

	inline static int32_t get_offset_of_mDeviceTrackerRotationOffset_1() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t3849131975, ___mDeviceTrackerRotationOffset_1)); }
	inline Quaternion_t2301928331  get_mDeviceTrackerRotationOffset_1() const { return ___mDeviceTrackerRotationOffset_1; }
	inline Quaternion_t2301928331 * get_address_of_mDeviceTrackerRotationOffset_1() { return &___mDeviceTrackerRotationOffset_1; }
	inline void set_mDeviceTrackerRotationOffset_1(Quaternion_t2301928331  value)
	{
		___mDeviceTrackerRotationOffset_1 = value;
	}

	inline static int32_t get_offset_of_mBeforeDevicePoseUpdated_2() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t3849131975, ___mBeforeDevicePoseUpdated_2)); }
	inline Action_t1264377477 * get_mBeforeDevicePoseUpdated_2() const { return ___mBeforeDevicePoseUpdated_2; }
	inline Action_t1264377477 ** get_address_of_mBeforeDevicePoseUpdated_2() { return &___mBeforeDevicePoseUpdated_2; }
	inline void set_mBeforeDevicePoseUpdated_2(Action_t1264377477 * value)
	{
		___mBeforeDevicePoseUpdated_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBeforeDevicePoseUpdated_2), value);
	}

	inline static int32_t get_offset_of_mAfterDevicePoseUpdated_3() { return static_cast<int32_t>(offsetof(DeviceTrackingManager_t3849131975, ___mAfterDevicePoseUpdated_3)); }
	inline Action_t1264377477 * get_mAfterDevicePoseUpdated_3() const { return ___mAfterDevicePoseUpdated_3; }
	inline Action_t1264377477 ** get_address_of_mAfterDevicePoseUpdated_3() { return &___mAfterDevicePoseUpdated_3; }
	inline void set_mAfterDevicePoseUpdated_3(Action_t1264377477 * value)
	{
		___mAfterDevicePoseUpdated_3 = value;
		Il2CppCodeGenWriteBarrier((&___mAfterDevicePoseUpdated_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKINGMANAGER_T3849131975_H
#ifndef PLAYMODEEYEWEARDEVICE_T2403363459_H
#define PLAYMODEEYEWEARDEVICE_T2403363459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.PlayModeEyewearDevice
struct  PlayModeEyewearDevice_t2403363459  : public EyewearDevice_t3223385723
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYMODEEYEWEARDEVICE_T2403363459_H
#ifndef DISTORTIONRENDERINGMODE_T2592134457_H
#define DISTORTIONRENDERINGMODE_T2592134457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DistortionRenderingMode
struct  DistortionRenderingMode_t2592134457 
{
public:
	// System.Int32 Vuforia.DistortionRenderingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DistortionRenderingMode_t2592134457, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONRENDERINGMODE_T2592134457_H
#ifndef VIEWERPARAMETERSLIST_T3991990123_H
#define VIEWERPARAMETERSLIST_T3991990123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerParametersList
struct  ViewerParametersList_t3991990123  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ViewerParametersList::mNativeVPL
	intptr_t ___mNativeVPL_0;

public:
	inline static int32_t get_offset_of_mNativeVPL_0() { return static_cast<int32_t>(offsetof(ViewerParametersList_t3991990123, ___mNativeVPL_0)); }
	inline intptr_t get_mNativeVPL_0() const { return ___mNativeVPL_0; }
	inline intptr_t* get_address_of_mNativeVPL_0() { return &___mNativeVPL_0; }
	inline void set_mNativeVPL_0(intptr_t value)
	{
		___mNativeVPL_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPARAMETERSLIST_T3991990123_H
#ifndef ANALYTICSSESSIONSTATE_T681173134_H
#define ANALYTICSSESSIONSTATE_T681173134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionState
struct  AnalyticsSessionState_t681173134 
{
public:
	// System.Int32 UnityEngine.Analytics.AnalyticsSessionState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AnalyticsSessionState_t681173134, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANALYTICSSESSIONSTATE_T681173134_H
#ifndef MODEL_CORRECTION_MODE_T1953038946_H
#define MODEL_CORRECTION_MODE_T1953038946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE
struct  MODEL_CORRECTION_MODE_t1953038946 
{
public:
	// System.Int32 Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MODEL_CORRECTION_MODE_t1953038946, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODEL_CORRECTION_MODE_T1953038946_H
#ifndef STENCILHIDEEXCESSAREACLIPPING_T2657238862_H
#define STENCILHIDEEXCESSAREACLIPPING_T2657238862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StencilHideExcessAreaClipping
struct  StencilHideExcessAreaClipping_t2657238862  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Vuforia.StencilHideExcessAreaClipping::mGameObject
	GameObject_t1113636619 * ___mGameObject_0;
	// UnityEngine.Shader Vuforia.StencilHideExcessAreaClipping::mMatteShader
	Shader_t4151988712 * ___mMatteShader_1;
	// UnityEngine.GameObject Vuforia.StencilHideExcessAreaClipping::mClippingPlane
	GameObject_t1113636619 * ___mClippingPlane_2;
	// UnityEngine.Camera Vuforia.StencilHideExcessAreaClipping::mCamera
	Camera_t4157153871 * ___mCamera_3;
	// System.Single Vuforia.StencilHideExcessAreaClipping::mCameraNearPlane
	float ___mCameraNearPlane_4;
	// System.Single Vuforia.StencilHideExcessAreaClipping::mCameraFarPlane
	float ___mCameraFarPlane_5;
	// UnityEngine.Rect Vuforia.StencilHideExcessAreaClipping::mCameraPixelRect
	Rect_t2360479859  ___mCameraPixelRect_6;
	// System.Single Vuforia.StencilHideExcessAreaClipping::mCameraFieldOfView
	float ___mCameraFieldOfView_7;
	// System.Boolean Vuforia.StencilHideExcessAreaClipping::mPlanesActivated
	bool ___mPlanesActivated_8;
	// UnityEngine.GameObject Vuforia.StencilHideExcessAreaClipping::mBgPlane
	GameObject_t1113636619 * ___mBgPlane_9;
	// UnityEngine.Vector3 Vuforia.StencilHideExcessAreaClipping::mBgPlaneLocalPos
	Vector3_t3722313464  ___mBgPlaneLocalPos_10;
	// UnityEngine.Vector3 Vuforia.StencilHideExcessAreaClipping::mBgPlaneLocalScale
	Vector3_t3722313464  ___mBgPlaneLocalScale_11;

public:
	inline static int32_t get_offset_of_mGameObject_0() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mGameObject_0)); }
	inline GameObject_t1113636619 * get_mGameObject_0() const { return ___mGameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_mGameObject_0() { return &___mGameObject_0; }
	inline void set_mGameObject_0(GameObject_t1113636619 * value)
	{
		___mGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___mGameObject_0), value);
	}

	inline static int32_t get_offset_of_mMatteShader_1() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mMatteShader_1)); }
	inline Shader_t4151988712 * get_mMatteShader_1() const { return ___mMatteShader_1; }
	inline Shader_t4151988712 ** get_address_of_mMatteShader_1() { return &___mMatteShader_1; }
	inline void set_mMatteShader_1(Shader_t4151988712 * value)
	{
		___mMatteShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_1), value);
	}

	inline static int32_t get_offset_of_mClippingPlane_2() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mClippingPlane_2)); }
	inline GameObject_t1113636619 * get_mClippingPlane_2() const { return ___mClippingPlane_2; }
	inline GameObject_t1113636619 ** get_address_of_mClippingPlane_2() { return &___mClippingPlane_2; }
	inline void set_mClippingPlane_2(GameObject_t1113636619 * value)
	{
		___mClippingPlane_2 = value;
		Il2CppCodeGenWriteBarrier((&___mClippingPlane_2), value);
	}

	inline static int32_t get_offset_of_mCamera_3() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mCamera_3)); }
	inline Camera_t4157153871 * get_mCamera_3() const { return ___mCamera_3; }
	inline Camera_t4157153871 ** get_address_of_mCamera_3() { return &___mCamera_3; }
	inline void set_mCamera_3(Camera_t4157153871 * value)
	{
		___mCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_3), value);
	}

	inline static int32_t get_offset_of_mCameraNearPlane_4() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mCameraNearPlane_4)); }
	inline float get_mCameraNearPlane_4() const { return ___mCameraNearPlane_4; }
	inline float* get_address_of_mCameraNearPlane_4() { return &___mCameraNearPlane_4; }
	inline void set_mCameraNearPlane_4(float value)
	{
		___mCameraNearPlane_4 = value;
	}

	inline static int32_t get_offset_of_mCameraFarPlane_5() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mCameraFarPlane_5)); }
	inline float get_mCameraFarPlane_5() const { return ___mCameraFarPlane_5; }
	inline float* get_address_of_mCameraFarPlane_5() { return &___mCameraFarPlane_5; }
	inline void set_mCameraFarPlane_5(float value)
	{
		___mCameraFarPlane_5 = value;
	}

	inline static int32_t get_offset_of_mCameraPixelRect_6() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mCameraPixelRect_6)); }
	inline Rect_t2360479859  get_mCameraPixelRect_6() const { return ___mCameraPixelRect_6; }
	inline Rect_t2360479859 * get_address_of_mCameraPixelRect_6() { return &___mCameraPixelRect_6; }
	inline void set_mCameraPixelRect_6(Rect_t2360479859  value)
	{
		___mCameraPixelRect_6 = value;
	}

	inline static int32_t get_offset_of_mCameraFieldOfView_7() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mCameraFieldOfView_7)); }
	inline float get_mCameraFieldOfView_7() const { return ___mCameraFieldOfView_7; }
	inline float* get_address_of_mCameraFieldOfView_7() { return &___mCameraFieldOfView_7; }
	inline void set_mCameraFieldOfView_7(float value)
	{
		___mCameraFieldOfView_7 = value;
	}

	inline static int32_t get_offset_of_mPlanesActivated_8() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mPlanesActivated_8)); }
	inline bool get_mPlanesActivated_8() const { return ___mPlanesActivated_8; }
	inline bool* get_address_of_mPlanesActivated_8() { return &___mPlanesActivated_8; }
	inline void set_mPlanesActivated_8(bool value)
	{
		___mPlanesActivated_8 = value;
	}

	inline static int32_t get_offset_of_mBgPlane_9() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mBgPlane_9)); }
	inline GameObject_t1113636619 * get_mBgPlane_9() const { return ___mBgPlane_9; }
	inline GameObject_t1113636619 ** get_address_of_mBgPlane_9() { return &___mBgPlane_9; }
	inline void set_mBgPlane_9(GameObject_t1113636619 * value)
	{
		___mBgPlane_9 = value;
		Il2CppCodeGenWriteBarrier((&___mBgPlane_9), value);
	}

	inline static int32_t get_offset_of_mBgPlaneLocalPos_10() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mBgPlaneLocalPos_10)); }
	inline Vector3_t3722313464  get_mBgPlaneLocalPos_10() const { return ___mBgPlaneLocalPos_10; }
	inline Vector3_t3722313464 * get_address_of_mBgPlaneLocalPos_10() { return &___mBgPlaneLocalPos_10; }
	inline void set_mBgPlaneLocalPos_10(Vector3_t3722313464  value)
	{
		___mBgPlaneLocalPos_10 = value;
	}

	inline static int32_t get_offset_of_mBgPlaneLocalScale_11() { return static_cast<int32_t>(offsetof(StencilHideExcessAreaClipping_t2657238862, ___mBgPlaneLocalScale_11)); }
	inline Vector3_t3722313464  get_mBgPlaneLocalScale_11() const { return ___mBgPlaneLocalScale_11; }
	inline Vector3_t3722313464 * get_address_of_mBgPlaneLocalScale_11() { return &___mBgPlaneLocalScale_11; }
	inline void set_mBgPlaneLocalScale_11(Vector3_t3722313464  value)
	{
		___mBgPlaneLocalScale_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STENCILHIDEEXCESSAREACLIPPING_T2657238862_H
#ifndef SEETHROUGHCONFIGURATION_T568665021_H
#define SEETHROUGHCONFIGURATION_T568665021_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/SeeThroughConfiguration
struct  SeeThroughConfiguration_t568665021 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/SeeThroughConfiguration::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SeeThroughConfiguration_t568665021, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEETHROUGHCONFIGURATION_T568665021_H
#ifndef STEREOFRAMEWORK_T3144873991_H
#define STEREOFRAMEWORK_T3144873991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/StereoFramework
struct  StereoFramework_t3144873991 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/StereoFramework::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StereoFramework_t3144873991, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOFRAMEWORK_T3144873991_H
#ifndef EYEWEARTYPE_T2277580470_H
#define EYEWEARTYPE_T2277580470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/EyewearType
struct  EyewearType_t2277580470 
{
public:
	// System.Int32 Vuforia.DigitalEyewearARController/EyewearType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyewearType_t2277580470, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEWEARTYPE_T2277580470_H
#ifndef POSEINFO_T1612729179_H
#define POSEINFO_T1612729179_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager/PoseInfo
struct  PoseInfo_t1612729179 
{
public:
	// UnityEngine.Vector3 Vuforia.HoloLensExtendedTrackingManager/PoseInfo::Position
	Vector3_t3722313464  ___Position_0;
	// UnityEngine.Quaternion Vuforia.HoloLensExtendedTrackingManager/PoseInfo::Rotation
	Quaternion_t2301928331  ___Rotation_1;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager/PoseInfo::NumFramesPoseWasOff
	int32_t ___NumFramesPoseWasOff_2;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(PoseInfo_t1612729179, ___Position_0)); }
	inline Vector3_t3722313464  get_Position_0() const { return ___Position_0; }
	inline Vector3_t3722313464 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t3722313464  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(PoseInfo_t1612729179, ___Rotation_1)); }
	inline Quaternion_t2301928331  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t2301928331  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_NumFramesPoseWasOff_2() { return static_cast<int32_t>(offsetof(PoseInfo_t1612729179, ___NumFramesPoseWasOff_2)); }
	inline int32_t get_NumFramesPoseWasOff_2() const { return ___NumFramesPoseWasOff_2; }
	inline int32_t* get_address_of_NumFramesPoseWasOff_2() { return &___NumFramesPoseWasOff_2; }
	inline void set_NumFramesPoseWasOff_2(int32_t value)
	{
		___NumFramesPoseWasOff_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEINFO_T1612729179_H
#ifndef ROTATIONALDEVICETRACKER_T2847210804_H
#define ROTATIONALDEVICETRACKER_T2847210804_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTracker
struct  RotationalDeviceTracker_t2847210804  : public DeviceTracker_t2315692373
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALDEVICETRACKER_T2847210804_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef VIEWERPARAMETERS_T3396315024_H
#define VIEWERPARAMETERS_T3396315024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ViewerParameters
struct  ViewerParameters_t3396315024  : public RuntimeObject
{
public:
	// System.IntPtr Vuforia.ViewerParameters::mNativeVP
	intptr_t ___mNativeVP_0;

public:
	inline static int32_t get_offset_of_mNativeVP_0() { return static_cast<int32_t>(offsetof(ViewerParameters_t3396315024, ___mNativeVP_0)); }
	inline intptr_t get_mNativeVP_0() const { return ___mNativeVP_0; }
	inline intptr_t* get_address_of_mNativeVP_0() { return &___mNativeVP_0; }
	inline void set_mNativeVP_0(intptr_t value)
	{
		___mNativeVP_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIEWERPARAMETERS_T3396315024_H
#ifndef LEGACYHIDEEXCESSAREACLIPPING_T1318580222_H
#define LEGACYHIDEEXCESSAREACLIPPING_T1318580222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.LegacyHideExcessAreaClipping
struct  LegacyHideExcessAreaClipping_t1318580222  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mGameObject
	GameObject_t1113636619 * ___mGameObject_0;
	// UnityEngine.Shader Vuforia.LegacyHideExcessAreaClipping::mMatteShader
	Shader_t4151988712 * ___mMatteShader_1;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mBgPlane
	GameObject_t1113636619 * ___mBgPlane_2;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mLeftPlane
	GameObject_t1113636619 * ___mLeftPlane_3;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mRightPlane
	GameObject_t1113636619 * ___mRightPlane_4;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mTopPlane
	GameObject_t1113636619 * ___mTopPlane_5;
	// UnityEngine.GameObject Vuforia.LegacyHideExcessAreaClipping::mBottomPlane
	GameObject_t1113636619 * ___mBottomPlane_6;
	// UnityEngine.Camera Vuforia.LegacyHideExcessAreaClipping::mCamera
	Camera_t4157153871 * ___mCamera_7;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mBgPlaneLocalPos
	Vector3_t3722313464  ___mBgPlaneLocalPos_8;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mBgPlaneLocalScale
	Vector3_t3722313464  ___mBgPlaneLocalScale_9;
	// System.Single Vuforia.LegacyHideExcessAreaClipping::mCameraNearPlane
	float ___mCameraNearPlane_10;
	// UnityEngine.Rect Vuforia.LegacyHideExcessAreaClipping::mCameraPixelRect
	Rect_t2360479859  ___mCameraPixelRect_11;
	// System.Single Vuforia.LegacyHideExcessAreaClipping::mCameraFieldOFView
	float ___mCameraFieldOFView_12;
	// Vuforia.HideExcessAreaAbstractBehaviour[] Vuforia.LegacyHideExcessAreaClipping::mHideBehaviours
	HideExcessAreaAbstractBehaviourU5BU5D_t2893788201* ___mHideBehaviours_13;
	// System.Collections.Generic.List`1<Vuforia.HideExcessAreaAbstractBehaviour> Vuforia.LegacyHideExcessAreaClipping::mDeactivatedHideBehaviours
	List_1_t546497774 * ___mDeactivatedHideBehaviours_14;
	// System.Boolean Vuforia.LegacyHideExcessAreaClipping::mPlanesActivated
	bool ___mPlanesActivated_15;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mLeftPlaneCachedScale
	Vector3_t3722313464  ___mLeftPlaneCachedScale_16;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mRightPlaneCachedScale
	Vector3_t3722313464  ___mRightPlaneCachedScale_17;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mBottomPlaneCachedScale
	Vector3_t3722313464  ___mBottomPlaneCachedScale_18;
	// UnityEngine.Vector3 Vuforia.LegacyHideExcessAreaClipping::mTopPlaneCachedScale
	Vector3_t3722313464  ___mTopPlaneCachedScale_19;

public:
	inline static int32_t get_offset_of_mGameObject_0() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mGameObject_0)); }
	inline GameObject_t1113636619 * get_mGameObject_0() const { return ___mGameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_mGameObject_0() { return &___mGameObject_0; }
	inline void set_mGameObject_0(GameObject_t1113636619 * value)
	{
		___mGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___mGameObject_0), value);
	}

	inline static int32_t get_offset_of_mMatteShader_1() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mMatteShader_1)); }
	inline Shader_t4151988712 * get_mMatteShader_1() const { return ___mMatteShader_1; }
	inline Shader_t4151988712 ** get_address_of_mMatteShader_1() { return &___mMatteShader_1; }
	inline void set_mMatteShader_1(Shader_t4151988712 * value)
	{
		___mMatteShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___mMatteShader_1), value);
	}

	inline static int32_t get_offset_of_mBgPlane_2() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mBgPlane_2)); }
	inline GameObject_t1113636619 * get_mBgPlane_2() const { return ___mBgPlane_2; }
	inline GameObject_t1113636619 ** get_address_of_mBgPlane_2() { return &___mBgPlane_2; }
	inline void set_mBgPlane_2(GameObject_t1113636619 * value)
	{
		___mBgPlane_2 = value;
		Il2CppCodeGenWriteBarrier((&___mBgPlane_2), value);
	}

	inline static int32_t get_offset_of_mLeftPlane_3() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mLeftPlane_3)); }
	inline GameObject_t1113636619 * get_mLeftPlane_3() const { return ___mLeftPlane_3; }
	inline GameObject_t1113636619 ** get_address_of_mLeftPlane_3() { return &___mLeftPlane_3; }
	inline void set_mLeftPlane_3(GameObject_t1113636619 * value)
	{
		___mLeftPlane_3 = value;
		Il2CppCodeGenWriteBarrier((&___mLeftPlane_3), value);
	}

	inline static int32_t get_offset_of_mRightPlane_4() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mRightPlane_4)); }
	inline GameObject_t1113636619 * get_mRightPlane_4() const { return ___mRightPlane_4; }
	inline GameObject_t1113636619 ** get_address_of_mRightPlane_4() { return &___mRightPlane_4; }
	inline void set_mRightPlane_4(GameObject_t1113636619 * value)
	{
		___mRightPlane_4 = value;
		Il2CppCodeGenWriteBarrier((&___mRightPlane_4), value);
	}

	inline static int32_t get_offset_of_mTopPlane_5() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mTopPlane_5)); }
	inline GameObject_t1113636619 * get_mTopPlane_5() const { return ___mTopPlane_5; }
	inline GameObject_t1113636619 ** get_address_of_mTopPlane_5() { return &___mTopPlane_5; }
	inline void set_mTopPlane_5(GameObject_t1113636619 * value)
	{
		___mTopPlane_5 = value;
		Il2CppCodeGenWriteBarrier((&___mTopPlane_5), value);
	}

	inline static int32_t get_offset_of_mBottomPlane_6() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mBottomPlane_6)); }
	inline GameObject_t1113636619 * get_mBottomPlane_6() const { return ___mBottomPlane_6; }
	inline GameObject_t1113636619 ** get_address_of_mBottomPlane_6() { return &___mBottomPlane_6; }
	inline void set_mBottomPlane_6(GameObject_t1113636619 * value)
	{
		___mBottomPlane_6 = value;
		Il2CppCodeGenWriteBarrier((&___mBottomPlane_6), value);
	}

	inline static int32_t get_offset_of_mCamera_7() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mCamera_7)); }
	inline Camera_t4157153871 * get_mCamera_7() const { return ___mCamera_7; }
	inline Camera_t4157153871 ** get_address_of_mCamera_7() { return &___mCamera_7; }
	inline void set_mCamera_7(Camera_t4157153871 * value)
	{
		___mCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_7), value);
	}

	inline static int32_t get_offset_of_mBgPlaneLocalPos_8() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mBgPlaneLocalPos_8)); }
	inline Vector3_t3722313464  get_mBgPlaneLocalPos_8() const { return ___mBgPlaneLocalPos_8; }
	inline Vector3_t3722313464 * get_address_of_mBgPlaneLocalPos_8() { return &___mBgPlaneLocalPos_8; }
	inline void set_mBgPlaneLocalPos_8(Vector3_t3722313464  value)
	{
		___mBgPlaneLocalPos_8 = value;
	}

	inline static int32_t get_offset_of_mBgPlaneLocalScale_9() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mBgPlaneLocalScale_9)); }
	inline Vector3_t3722313464  get_mBgPlaneLocalScale_9() const { return ___mBgPlaneLocalScale_9; }
	inline Vector3_t3722313464 * get_address_of_mBgPlaneLocalScale_9() { return &___mBgPlaneLocalScale_9; }
	inline void set_mBgPlaneLocalScale_9(Vector3_t3722313464  value)
	{
		___mBgPlaneLocalScale_9 = value;
	}

	inline static int32_t get_offset_of_mCameraNearPlane_10() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mCameraNearPlane_10)); }
	inline float get_mCameraNearPlane_10() const { return ___mCameraNearPlane_10; }
	inline float* get_address_of_mCameraNearPlane_10() { return &___mCameraNearPlane_10; }
	inline void set_mCameraNearPlane_10(float value)
	{
		___mCameraNearPlane_10 = value;
	}

	inline static int32_t get_offset_of_mCameraPixelRect_11() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mCameraPixelRect_11)); }
	inline Rect_t2360479859  get_mCameraPixelRect_11() const { return ___mCameraPixelRect_11; }
	inline Rect_t2360479859 * get_address_of_mCameraPixelRect_11() { return &___mCameraPixelRect_11; }
	inline void set_mCameraPixelRect_11(Rect_t2360479859  value)
	{
		___mCameraPixelRect_11 = value;
	}

	inline static int32_t get_offset_of_mCameraFieldOFView_12() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mCameraFieldOFView_12)); }
	inline float get_mCameraFieldOFView_12() const { return ___mCameraFieldOFView_12; }
	inline float* get_address_of_mCameraFieldOFView_12() { return &___mCameraFieldOFView_12; }
	inline void set_mCameraFieldOFView_12(float value)
	{
		___mCameraFieldOFView_12 = value;
	}

	inline static int32_t get_offset_of_mHideBehaviours_13() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mHideBehaviours_13)); }
	inline HideExcessAreaAbstractBehaviourU5BU5D_t2893788201* get_mHideBehaviours_13() const { return ___mHideBehaviours_13; }
	inline HideExcessAreaAbstractBehaviourU5BU5D_t2893788201** get_address_of_mHideBehaviours_13() { return &___mHideBehaviours_13; }
	inline void set_mHideBehaviours_13(HideExcessAreaAbstractBehaviourU5BU5D_t2893788201* value)
	{
		___mHideBehaviours_13 = value;
		Il2CppCodeGenWriteBarrier((&___mHideBehaviours_13), value);
	}

	inline static int32_t get_offset_of_mDeactivatedHideBehaviours_14() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mDeactivatedHideBehaviours_14)); }
	inline List_1_t546497774 * get_mDeactivatedHideBehaviours_14() const { return ___mDeactivatedHideBehaviours_14; }
	inline List_1_t546497774 ** get_address_of_mDeactivatedHideBehaviours_14() { return &___mDeactivatedHideBehaviours_14; }
	inline void set_mDeactivatedHideBehaviours_14(List_1_t546497774 * value)
	{
		___mDeactivatedHideBehaviours_14 = value;
		Il2CppCodeGenWriteBarrier((&___mDeactivatedHideBehaviours_14), value);
	}

	inline static int32_t get_offset_of_mPlanesActivated_15() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mPlanesActivated_15)); }
	inline bool get_mPlanesActivated_15() const { return ___mPlanesActivated_15; }
	inline bool* get_address_of_mPlanesActivated_15() { return &___mPlanesActivated_15; }
	inline void set_mPlanesActivated_15(bool value)
	{
		___mPlanesActivated_15 = value;
	}

	inline static int32_t get_offset_of_mLeftPlaneCachedScale_16() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mLeftPlaneCachedScale_16)); }
	inline Vector3_t3722313464  get_mLeftPlaneCachedScale_16() const { return ___mLeftPlaneCachedScale_16; }
	inline Vector3_t3722313464 * get_address_of_mLeftPlaneCachedScale_16() { return &___mLeftPlaneCachedScale_16; }
	inline void set_mLeftPlaneCachedScale_16(Vector3_t3722313464  value)
	{
		___mLeftPlaneCachedScale_16 = value;
	}

	inline static int32_t get_offset_of_mRightPlaneCachedScale_17() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mRightPlaneCachedScale_17)); }
	inline Vector3_t3722313464  get_mRightPlaneCachedScale_17() const { return ___mRightPlaneCachedScale_17; }
	inline Vector3_t3722313464 * get_address_of_mRightPlaneCachedScale_17() { return &___mRightPlaneCachedScale_17; }
	inline void set_mRightPlaneCachedScale_17(Vector3_t3722313464  value)
	{
		___mRightPlaneCachedScale_17 = value;
	}

	inline static int32_t get_offset_of_mBottomPlaneCachedScale_18() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mBottomPlaneCachedScale_18)); }
	inline Vector3_t3722313464  get_mBottomPlaneCachedScale_18() const { return ___mBottomPlaneCachedScale_18; }
	inline Vector3_t3722313464 * get_address_of_mBottomPlaneCachedScale_18() { return &___mBottomPlaneCachedScale_18; }
	inline void set_mBottomPlaneCachedScale_18(Vector3_t3722313464  value)
	{
		___mBottomPlaneCachedScale_18 = value;
	}

	inline static int32_t get_offset_of_mTopPlaneCachedScale_19() { return static_cast<int32_t>(offsetof(LegacyHideExcessAreaClipping_t1318580222, ___mTopPlaneCachedScale_19)); }
	inline Vector3_t3722313464  get_mTopPlaneCachedScale_19() const { return ___mTopPlaneCachedScale_19; }
	inline Vector3_t3722313464 * get_address_of_mTopPlaneCachedScale_19() { return &___mTopPlaneCachedScale_19; }
	inline void set_mTopPlaneCachedScale_19(Vector3_t3722313464  value)
	{
		___mTopPlaneCachedScale_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEGACYHIDEEXCESSAREACLIPPING_T1318580222_H
#ifndef CAMERACONFIGURATIONUTILITY_T1452827745_H
#define CAMERACONFIGURATIONUTILITY_T1452827745_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CameraConfigurationUtility
struct  CameraConfigurationUtility_t1452827745  : public RuntimeObject
{
public:

public:
};

struct CameraConfigurationUtility_t1452827745_StaticFields
{
public:
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MIN_CENTER
	Vector4_t3319028937  ___MIN_CENTER_0;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_CENTER
	Vector4_t3319028937  ___MAX_CENTER_1;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_BOTTOM
	Vector4_t3319028937  ___MAX_BOTTOM_2;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_TOP
	Vector4_t3319028937  ___MAX_TOP_3;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_LEFT
	Vector4_t3319028937  ___MAX_LEFT_4;
	// UnityEngine.Vector4 Vuforia.CameraConfigurationUtility::MAX_RIGHT
	Vector4_t3319028937  ___MAX_RIGHT_5;

public:
	inline static int32_t get_offset_of_MIN_CENTER_0() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t1452827745_StaticFields, ___MIN_CENTER_0)); }
	inline Vector4_t3319028937  get_MIN_CENTER_0() const { return ___MIN_CENTER_0; }
	inline Vector4_t3319028937 * get_address_of_MIN_CENTER_0() { return &___MIN_CENTER_0; }
	inline void set_MIN_CENTER_0(Vector4_t3319028937  value)
	{
		___MIN_CENTER_0 = value;
	}

	inline static int32_t get_offset_of_MAX_CENTER_1() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t1452827745_StaticFields, ___MAX_CENTER_1)); }
	inline Vector4_t3319028937  get_MAX_CENTER_1() const { return ___MAX_CENTER_1; }
	inline Vector4_t3319028937 * get_address_of_MAX_CENTER_1() { return &___MAX_CENTER_1; }
	inline void set_MAX_CENTER_1(Vector4_t3319028937  value)
	{
		___MAX_CENTER_1 = value;
	}

	inline static int32_t get_offset_of_MAX_BOTTOM_2() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t1452827745_StaticFields, ___MAX_BOTTOM_2)); }
	inline Vector4_t3319028937  get_MAX_BOTTOM_2() const { return ___MAX_BOTTOM_2; }
	inline Vector4_t3319028937 * get_address_of_MAX_BOTTOM_2() { return &___MAX_BOTTOM_2; }
	inline void set_MAX_BOTTOM_2(Vector4_t3319028937  value)
	{
		___MAX_BOTTOM_2 = value;
	}

	inline static int32_t get_offset_of_MAX_TOP_3() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t1452827745_StaticFields, ___MAX_TOP_3)); }
	inline Vector4_t3319028937  get_MAX_TOP_3() const { return ___MAX_TOP_3; }
	inline Vector4_t3319028937 * get_address_of_MAX_TOP_3() { return &___MAX_TOP_3; }
	inline void set_MAX_TOP_3(Vector4_t3319028937  value)
	{
		___MAX_TOP_3 = value;
	}

	inline static int32_t get_offset_of_MAX_LEFT_4() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t1452827745_StaticFields, ___MAX_LEFT_4)); }
	inline Vector4_t3319028937  get_MAX_LEFT_4() const { return ___MAX_LEFT_4; }
	inline Vector4_t3319028937 * get_address_of_MAX_LEFT_4() { return &___MAX_LEFT_4; }
	inline void set_MAX_LEFT_4(Vector4_t3319028937  value)
	{
		___MAX_LEFT_4 = value;
	}

	inline static int32_t get_offset_of_MAX_RIGHT_5() { return static_cast<int32_t>(offsetof(CameraConfigurationUtility_t1452827745_StaticFields, ___MAX_RIGHT_5)); }
	inline Vector4_t3319028937  get_MAX_RIGHT_5() const { return ___MAX_RIGHT_5; }
	inline Vector4_t3319028937 * get_address_of_MAX_RIGHT_5() { return &___MAX_RIGHT_5; }
	inline void set_MAX_RIGHT_5(Vector4_t3319028937  value)
	{
		___MAX_RIGHT_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONFIGURATIONUTILITY_T1452827745_H
#ifndef BASECAMERACONFIGURATION_T3118151474_H
#define BASECAMERACONFIGURATION_T3118151474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BaseCameraConfiguration
struct  BaseCameraConfiguration_t3118151474  : public RuntimeObject
{
public:
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.BaseCameraConfiguration::mCameraDeviceMode
	int32_t ___mCameraDeviceMode_0;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.BaseCameraConfiguration::mLastVideoBackGroundMirroredFromSDK
	int32_t ___mLastVideoBackGroundMirroredFromSDK_1;
	// System.Action Vuforia.BaseCameraConfiguration::mOnVideoBackgroundConfigChanged
	Action_t1264377477 * ___mOnVideoBackgroundConfigChanged_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Camera,Vuforia.VideoBackgroundAbstractBehaviour> Vuforia.BaseCameraConfiguration::mVideoBackgroundBehaviours
	Dictionary_2_t507033335 * ___mVideoBackgroundBehaviours_3;
	// UnityEngine.Rect Vuforia.BaseCameraConfiguration::mVideoBackgroundViewportRect
	Rect_t2360479859  ___mVideoBackgroundViewportRect_4;
	// System.Boolean Vuforia.BaseCameraConfiguration::mRenderVideoBackground
	bool ___mRenderVideoBackground_5;
	// UnityEngine.ScreenOrientation Vuforia.BaseCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_6;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.BaseCameraConfiguration::mInitialReflection
	int32_t ___mInitialReflection_7;
	// Vuforia.BackgroundPlaneAbstractBehaviour Vuforia.BaseCameraConfiguration::mBackgroundPlaneBehaviour
	BackgroundPlaneAbstractBehaviour_t4147679365 * ___mBackgroundPlaneBehaviour_8;
	// System.Boolean Vuforia.BaseCameraConfiguration::mCameraParameterChanged
	bool ___mCameraParameterChanged_9;

public:
	inline static int32_t get_offset_of_mCameraDeviceMode_0() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mCameraDeviceMode_0)); }
	inline int32_t get_mCameraDeviceMode_0() const { return ___mCameraDeviceMode_0; }
	inline int32_t* get_address_of_mCameraDeviceMode_0() { return &___mCameraDeviceMode_0; }
	inline void set_mCameraDeviceMode_0(int32_t value)
	{
		___mCameraDeviceMode_0 = value;
	}

	inline static int32_t get_offset_of_mLastVideoBackGroundMirroredFromSDK_1() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mLastVideoBackGroundMirroredFromSDK_1)); }
	inline int32_t get_mLastVideoBackGroundMirroredFromSDK_1() const { return ___mLastVideoBackGroundMirroredFromSDK_1; }
	inline int32_t* get_address_of_mLastVideoBackGroundMirroredFromSDK_1() { return &___mLastVideoBackGroundMirroredFromSDK_1; }
	inline void set_mLastVideoBackGroundMirroredFromSDK_1(int32_t value)
	{
		___mLastVideoBackGroundMirroredFromSDK_1 = value;
	}

	inline static int32_t get_offset_of_mOnVideoBackgroundConfigChanged_2() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mOnVideoBackgroundConfigChanged_2)); }
	inline Action_t1264377477 * get_mOnVideoBackgroundConfigChanged_2() const { return ___mOnVideoBackgroundConfigChanged_2; }
	inline Action_t1264377477 ** get_address_of_mOnVideoBackgroundConfigChanged_2() { return &___mOnVideoBackgroundConfigChanged_2; }
	inline void set_mOnVideoBackgroundConfigChanged_2(Action_t1264377477 * value)
	{
		___mOnVideoBackgroundConfigChanged_2 = value;
		Il2CppCodeGenWriteBarrier((&___mOnVideoBackgroundConfigChanged_2), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundBehaviours_3() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mVideoBackgroundBehaviours_3)); }
	inline Dictionary_2_t507033335 * get_mVideoBackgroundBehaviours_3() const { return ___mVideoBackgroundBehaviours_3; }
	inline Dictionary_2_t507033335 ** get_address_of_mVideoBackgroundBehaviours_3() { return &___mVideoBackgroundBehaviours_3; }
	inline void set_mVideoBackgroundBehaviours_3(Dictionary_2_t507033335 * value)
	{
		___mVideoBackgroundBehaviours_3 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBackgroundBehaviours_3), value);
	}

	inline static int32_t get_offset_of_mVideoBackgroundViewportRect_4() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mVideoBackgroundViewportRect_4)); }
	inline Rect_t2360479859  get_mVideoBackgroundViewportRect_4() const { return ___mVideoBackgroundViewportRect_4; }
	inline Rect_t2360479859 * get_address_of_mVideoBackgroundViewportRect_4() { return &___mVideoBackgroundViewportRect_4; }
	inline void set_mVideoBackgroundViewportRect_4(Rect_t2360479859  value)
	{
		___mVideoBackgroundViewportRect_4 = value;
	}

	inline static int32_t get_offset_of_mRenderVideoBackground_5() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mRenderVideoBackground_5)); }
	inline bool get_mRenderVideoBackground_5() const { return ___mRenderVideoBackground_5; }
	inline bool* get_address_of_mRenderVideoBackground_5() { return &___mRenderVideoBackground_5; }
	inline void set_mRenderVideoBackground_5(bool value)
	{
		___mRenderVideoBackground_5 = value;
	}

	inline static int32_t get_offset_of_mProjectionOrientation_6() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mProjectionOrientation_6)); }
	inline int32_t get_mProjectionOrientation_6() const { return ___mProjectionOrientation_6; }
	inline int32_t* get_address_of_mProjectionOrientation_6() { return &___mProjectionOrientation_6; }
	inline void set_mProjectionOrientation_6(int32_t value)
	{
		___mProjectionOrientation_6 = value;
	}

	inline static int32_t get_offset_of_mInitialReflection_7() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mInitialReflection_7)); }
	inline int32_t get_mInitialReflection_7() const { return ___mInitialReflection_7; }
	inline int32_t* get_address_of_mInitialReflection_7() { return &___mInitialReflection_7; }
	inline void set_mInitialReflection_7(int32_t value)
	{
		___mInitialReflection_7 = value;
	}

	inline static int32_t get_offset_of_mBackgroundPlaneBehaviour_8() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mBackgroundPlaneBehaviour_8)); }
	inline BackgroundPlaneAbstractBehaviour_t4147679365 * get_mBackgroundPlaneBehaviour_8() const { return ___mBackgroundPlaneBehaviour_8; }
	inline BackgroundPlaneAbstractBehaviour_t4147679365 ** get_address_of_mBackgroundPlaneBehaviour_8() { return &___mBackgroundPlaneBehaviour_8; }
	inline void set_mBackgroundPlaneBehaviour_8(BackgroundPlaneAbstractBehaviour_t4147679365 * value)
	{
		___mBackgroundPlaneBehaviour_8 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlaneBehaviour_8), value);
	}

	inline static int32_t get_offset_of_mCameraParameterChanged_9() { return static_cast<int32_t>(offsetof(BaseCameraConfiguration_t3118151474, ___mCameraParameterChanged_9)); }
	inline bool get_mCameraParameterChanged_9() const { return ___mCameraParameterChanged_9; }
	inline bool* get_address_of_mCameraParameterChanged_9() { return &___mCameraParameterChanged_9; }
	inline void set_mCameraParameterChanged_9(bool value)
	{
		___mCameraParameterChanged_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASECAMERACONFIGURATION_T3118151474_H
#ifndef DEVICETRACKERARCONTROLLER_T1095592542_H
#define DEVICETRACKERARCONTROLLER_T1095592542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DeviceTrackerARController
struct  DeviceTrackerARController_t1095592542  : public ARController_t116632334
{
public:
	// System.Boolean Vuforia.DeviceTrackerARController::mAutoInitTracker
	bool ___mAutoInitTracker_3;
	// System.Boolean Vuforia.DeviceTrackerARController::mAutoStartTracker
	bool ___mAutoStartTracker_4;
	// System.Boolean Vuforia.DeviceTrackerARController::mPosePrediction
	bool ___mPosePrediction_5;
	// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE Vuforia.DeviceTrackerARController::mModelCorrectionMode
	int32_t ___mModelCorrectionMode_6;
	// System.Boolean Vuforia.DeviceTrackerARController::mModelTransformEnabled
	bool ___mModelTransformEnabled_7;
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::mModelTransform
	Vector3_t3722313464  ___mModelTransform_8;
	// System.Action Vuforia.DeviceTrackerARController::mTrackerStarted
	Action_t1264377477 * ___mTrackerStarted_9;
	// System.Boolean Vuforia.DeviceTrackerARController::mTrackerWasActiveBeforePause
	bool ___mTrackerWasActiveBeforePause_10;
	// System.Boolean Vuforia.DeviceTrackerARController::mTrackerWasActiveBeforeDisabling
	bool ___mTrackerWasActiveBeforeDisabling_11;

public:
	inline static int32_t get_offset_of_mAutoInitTracker_3() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mAutoInitTracker_3)); }
	inline bool get_mAutoInitTracker_3() const { return ___mAutoInitTracker_3; }
	inline bool* get_address_of_mAutoInitTracker_3() { return &___mAutoInitTracker_3; }
	inline void set_mAutoInitTracker_3(bool value)
	{
		___mAutoInitTracker_3 = value;
	}

	inline static int32_t get_offset_of_mAutoStartTracker_4() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mAutoStartTracker_4)); }
	inline bool get_mAutoStartTracker_4() const { return ___mAutoStartTracker_4; }
	inline bool* get_address_of_mAutoStartTracker_4() { return &___mAutoStartTracker_4; }
	inline void set_mAutoStartTracker_4(bool value)
	{
		___mAutoStartTracker_4 = value;
	}

	inline static int32_t get_offset_of_mPosePrediction_5() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mPosePrediction_5)); }
	inline bool get_mPosePrediction_5() const { return ___mPosePrediction_5; }
	inline bool* get_address_of_mPosePrediction_5() { return &___mPosePrediction_5; }
	inline void set_mPosePrediction_5(bool value)
	{
		___mPosePrediction_5 = value;
	}

	inline static int32_t get_offset_of_mModelCorrectionMode_6() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mModelCorrectionMode_6)); }
	inline int32_t get_mModelCorrectionMode_6() const { return ___mModelCorrectionMode_6; }
	inline int32_t* get_address_of_mModelCorrectionMode_6() { return &___mModelCorrectionMode_6; }
	inline void set_mModelCorrectionMode_6(int32_t value)
	{
		___mModelCorrectionMode_6 = value;
	}

	inline static int32_t get_offset_of_mModelTransformEnabled_7() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mModelTransformEnabled_7)); }
	inline bool get_mModelTransformEnabled_7() const { return ___mModelTransformEnabled_7; }
	inline bool* get_address_of_mModelTransformEnabled_7() { return &___mModelTransformEnabled_7; }
	inline void set_mModelTransformEnabled_7(bool value)
	{
		___mModelTransformEnabled_7 = value;
	}

	inline static int32_t get_offset_of_mModelTransform_8() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mModelTransform_8)); }
	inline Vector3_t3722313464  get_mModelTransform_8() const { return ___mModelTransform_8; }
	inline Vector3_t3722313464 * get_address_of_mModelTransform_8() { return &___mModelTransform_8; }
	inline void set_mModelTransform_8(Vector3_t3722313464  value)
	{
		___mModelTransform_8 = value;
	}

	inline static int32_t get_offset_of_mTrackerStarted_9() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mTrackerStarted_9)); }
	inline Action_t1264377477 * get_mTrackerStarted_9() const { return ___mTrackerStarted_9; }
	inline Action_t1264377477 ** get_address_of_mTrackerStarted_9() { return &___mTrackerStarted_9; }
	inline void set_mTrackerStarted_9(Action_t1264377477 * value)
	{
		___mTrackerStarted_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackerStarted_9), value);
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforePause_10() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mTrackerWasActiveBeforePause_10)); }
	inline bool get_mTrackerWasActiveBeforePause_10() const { return ___mTrackerWasActiveBeforePause_10; }
	inline bool* get_address_of_mTrackerWasActiveBeforePause_10() { return &___mTrackerWasActiveBeforePause_10; }
	inline void set_mTrackerWasActiveBeforePause_10(bool value)
	{
		___mTrackerWasActiveBeforePause_10 = value;
	}

	inline static int32_t get_offset_of_mTrackerWasActiveBeforeDisabling_11() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542, ___mTrackerWasActiveBeforeDisabling_11)); }
	inline bool get_mTrackerWasActiveBeforeDisabling_11() const { return ___mTrackerWasActiveBeforeDisabling_11; }
	inline bool* get_address_of_mTrackerWasActiveBeforeDisabling_11() { return &___mTrackerWasActiveBeforeDisabling_11; }
	inline void set_mTrackerWasActiveBeforeDisabling_11(bool value)
	{
		___mTrackerWasActiveBeforeDisabling_11 = value;
	}
};

struct DeviceTrackerARController_t1095592542_StaticFields
{
public:
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::DEFAULT_HEAD_PIVOT
	Vector3_t3722313464  ___DEFAULT_HEAD_PIVOT_1;
	// UnityEngine.Vector3 Vuforia.DeviceTrackerARController::DEFAULT_HANDHELD_PIVOT
	Vector3_t3722313464  ___DEFAULT_HANDHELD_PIVOT_2;
	// Vuforia.DeviceTrackerARController Vuforia.DeviceTrackerARController::mInstance
	DeviceTrackerARController_t1095592542 * ___mInstance_12;
	// System.Object Vuforia.DeviceTrackerARController::mPadlock
	RuntimeObject * ___mPadlock_13;

public:
	inline static int32_t get_offset_of_DEFAULT_HEAD_PIVOT_1() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542_StaticFields, ___DEFAULT_HEAD_PIVOT_1)); }
	inline Vector3_t3722313464  get_DEFAULT_HEAD_PIVOT_1() const { return ___DEFAULT_HEAD_PIVOT_1; }
	inline Vector3_t3722313464 * get_address_of_DEFAULT_HEAD_PIVOT_1() { return &___DEFAULT_HEAD_PIVOT_1; }
	inline void set_DEFAULT_HEAD_PIVOT_1(Vector3_t3722313464  value)
	{
		___DEFAULT_HEAD_PIVOT_1 = value;
	}

	inline static int32_t get_offset_of_DEFAULT_HANDHELD_PIVOT_2() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542_StaticFields, ___DEFAULT_HANDHELD_PIVOT_2)); }
	inline Vector3_t3722313464  get_DEFAULT_HANDHELD_PIVOT_2() const { return ___DEFAULT_HANDHELD_PIVOT_2; }
	inline Vector3_t3722313464 * get_address_of_DEFAULT_HANDHELD_PIVOT_2() { return &___DEFAULT_HANDHELD_PIVOT_2; }
	inline void set_DEFAULT_HANDHELD_PIVOT_2(Vector3_t3722313464  value)
	{
		___DEFAULT_HANDHELD_PIVOT_2 = value;
	}

	inline static int32_t get_offset_of_mInstance_12() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542_StaticFields, ___mInstance_12)); }
	inline DeviceTrackerARController_t1095592542 * get_mInstance_12() const { return ___mInstance_12; }
	inline DeviceTrackerARController_t1095592542 ** get_address_of_mInstance_12() { return &___mInstance_12; }
	inline void set_mInstance_12(DeviceTrackerARController_t1095592542 * value)
	{
		___mInstance_12 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_12), value);
	}

	inline static int32_t get_offset_of_mPadlock_13() { return static_cast<int32_t>(offsetof(DeviceTrackerARController_t1095592542_StaticFields, ___mPadlock_13)); }
	inline RuntimeObject * get_mPadlock_13() const { return ___mPadlock_13; }
	inline RuntimeObject ** get_address_of_mPadlock_13() { return &___mPadlock_13; }
	inline void set_mPadlock_13(RuntimeObject * value)
	{
		___mPadlock_13 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEVICETRACKERARCONTROLLER_T1095592542_H
#ifndef SERIALIZABLEVIEWERPARAMETERS_T2043332680_H
#define SERIALIZABLEVIEWERPARAMETERS_T2043332680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController/SerializableViewerParameters
struct  SerializableViewerParameters_t2043332680  : public RuntimeObject
{
public:
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::Version
	float ___Version_0;
	// System.String Vuforia.DigitalEyewearARController/SerializableViewerParameters::Name
	String_t* ___Name_1;
	// System.String Vuforia.DigitalEyewearARController/SerializableViewerParameters::Manufacturer
	String_t* ___Manufacturer_2;
	// Vuforia.ViewerButtonType Vuforia.DigitalEyewearARController/SerializableViewerParameters::ButtonType
	int32_t ___ButtonType_3;
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::ScreenToLensDistance
	float ___ScreenToLensDistance_4;
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::InterLensDistance
	float ___InterLensDistance_5;
	// Vuforia.ViewerTrayAlignment Vuforia.DigitalEyewearARController/SerializableViewerParameters::TrayAlignment
	int32_t ___TrayAlignment_6;
	// System.Single Vuforia.DigitalEyewearARController/SerializableViewerParameters::LensCenterToTrayDistance
	float ___LensCenterToTrayDistance_7;
	// UnityEngine.Vector2 Vuforia.DigitalEyewearARController/SerializableViewerParameters::DistortionCoefficients
	Vector2_t2156229523  ___DistortionCoefficients_8;
	// UnityEngine.Vector4 Vuforia.DigitalEyewearARController/SerializableViewerParameters::FieldOfView
	Vector4_t3319028937  ___FieldOfView_9;
	// System.Boolean Vuforia.DigitalEyewearARController/SerializableViewerParameters::ContainsMagnet
	bool ___ContainsMagnet_10;

public:
	inline static int32_t get_offset_of_Version_0() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___Version_0)); }
	inline float get_Version_0() const { return ___Version_0; }
	inline float* get_address_of_Version_0() { return &___Version_0; }
	inline void set_Version_0(float value)
	{
		___Version_0 = value;
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((&___Name_1), value);
	}

	inline static int32_t get_offset_of_Manufacturer_2() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___Manufacturer_2)); }
	inline String_t* get_Manufacturer_2() const { return ___Manufacturer_2; }
	inline String_t** get_address_of_Manufacturer_2() { return &___Manufacturer_2; }
	inline void set_Manufacturer_2(String_t* value)
	{
		___Manufacturer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Manufacturer_2), value);
	}

	inline static int32_t get_offset_of_ButtonType_3() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___ButtonType_3)); }
	inline int32_t get_ButtonType_3() const { return ___ButtonType_3; }
	inline int32_t* get_address_of_ButtonType_3() { return &___ButtonType_3; }
	inline void set_ButtonType_3(int32_t value)
	{
		___ButtonType_3 = value;
	}

	inline static int32_t get_offset_of_ScreenToLensDistance_4() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___ScreenToLensDistance_4)); }
	inline float get_ScreenToLensDistance_4() const { return ___ScreenToLensDistance_4; }
	inline float* get_address_of_ScreenToLensDistance_4() { return &___ScreenToLensDistance_4; }
	inline void set_ScreenToLensDistance_4(float value)
	{
		___ScreenToLensDistance_4 = value;
	}

	inline static int32_t get_offset_of_InterLensDistance_5() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___InterLensDistance_5)); }
	inline float get_InterLensDistance_5() const { return ___InterLensDistance_5; }
	inline float* get_address_of_InterLensDistance_5() { return &___InterLensDistance_5; }
	inline void set_InterLensDistance_5(float value)
	{
		___InterLensDistance_5 = value;
	}

	inline static int32_t get_offset_of_TrayAlignment_6() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___TrayAlignment_6)); }
	inline int32_t get_TrayAlignment_6() const { return ___TrayAlignment_6; }
	inline int32_t* get_address_of_TrayAlignment_6() { return &___TrayAlignment_6; }
	inline void set_TrayAlignment_6(int32_t value)
	{
		___TrayAlignment_6 = value;
	}

	inline static int32_t get_offset_of_LensCenterToTrayDistance_7() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___LensCenterToTrayDistance_7)); }
	inline float get_LensCenterToTrayDistance_7() const { return ___LensCenterToTrayDistance_7; }
	inline float* get_address_of_LensCenterToTrayDistance_7() { return &___LensCenterToTrayDistance_7; }
	inline void set_LensCenterToTrayDistance_7(float value)
	{
		___LensCenterToTrayDistance_7 = value;
	}

	inline static int32_t get_offset_of_DistortionCoefficients_8() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___DistortionCoefficients_8)); }
	inline Vector2_t2156229523  get_DistortionCoefficients_8() const { return ___DistortionCoefficients_8; }
	inline Vector2_t2156229523 * get_address_of_DistortionCoefficients_8() { return &___DistortionCoefficients_8; }
	inline void set_DistortionCoefficients_8(Vector2_t2156229523  value)
	{
		___DistortionCoefficients_8 = value;
	}

	inline static int32_t get_offset_of_FieldOfView_9() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___FieldOfView_9)); }
	inline Vector4_t3319028937  get_FieldOfView_9() const { return ___FieldOfView_9; }
	inline Vector4_t3319028937 * get_address_of_FieldOfView_9() { return &___FieldOfView_9; }
	inline void set_FieldOfView_9(Vector4_t3319028937  value)
	{
		___FieldOfView_9 = value;
	}

	inline static int32_t get_offset_of_ContainsMagnet_10() { return static_cast<int32_t>(offsetof(SerializableViewerParameters_t2043332680, ___ContainsMagnet_10)); }
	inline bool get_ContainsMagnet_10() const { return ___ContainsMagnet_10; }
	inline bool* get_address_of_ContainsMagnet_10() { return &___ContainsMagnet_10; }
	inline void set_ContainsMagnet_10(bool value)
	{
		___ContainsMagnet_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZABLEVIEWERPARAMETERS_T2043332680_H
#ifndef DIGITALEYEWEARARCONTROLLER_T1054226036_H
#define DIGITALEYEWEARARCONTROLLER_T1054226036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DigitalEyewearARController
struct  DigitalEyewearARController_t1054226036  : public ARController_t116632334
{
public:
	// System.Single Vuforia.DigitalEyewearARController::mCameraOffset
	float ___mCameraOffset_7;
	// Vuforia.DistortionRenderingMode Vuforia.DigitalEyewearARController::mDistortionRenderingMode
	int32_t ___mDistortionRenderingMode_8;
	// System.Int32 Vuforia.DigitalEyewearARController::mDistortionRenderingLayer
	int32_t ___mDistortionRenderingLayer_9;
	// Vuforia.DigitalEyewearARController/EyewearType Vuforia.DigitalEyewearARController::mEyewearType
	int32_t ___mEyewearType_10;
	// Vuforia.DigitalEyewearARController/StereoFramework Vuforia.DigitalEyewearARController::mStereoFramework
	int32_t ___mStereoFramework_11;
	// Vuforia.DigitalEyewearARController/SeeThroughConfiguration Vuforia.DigitalEyewearARController::mSeeThroughConfiguration
	int32_t ___mSeeThroughConfiguration_12;
	// System.String Vuforia.DigitalEyewearARController::mViewerName
	String_t* ___mViewerName_13;
	// System.String Vuforia.DigitalEyewearARController::mViewerManufacturer
	String_t* ___mViewerManufacturer_14;
	// System.Boolean Vuforia.DigitalEyewearARController::mUseCustomViewer
	bool ___mUseCustomViewer_15;
	// Vuforia.DigitalEyewearARController/SerializableViewerParameters Vuforia.DigitalEyewearARController::mCustomViewer
	SerializableViewerParameters_t2043332680 * ___mCustomViewer_16;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mCentralAnchorPoint
	Transform_t3600365921 * ___mCentralAnchorPoint_17;
	// UnityEngine.Transform Vuforia.DigitalEyewearARController::mParentAnchorPoint
	Transform_t3600365921 * ___mParentAnchorPoint_18;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mPrimaryCamera
	Camera_t4157153871 * ___mPrimaryCamera_19;
	// UnityEngine.Rect Vuforia.DigitalEyewearARController::mPrimaryCameraOriginalRect
	Rect_t2360479859  ___mPrimaryCameraOriginalRect_20;
	// UnityEngine.Camera Vuforia.DigitalEyewearARController::mSecondaryCamera
	Camera_t4157153871 * ___mSecondaryCamera_21;
	// UnityEngine.Rect Vuforia.DigitalEyewearARController::mSecondaryCameraOriginalRect
	Rect_t2360479859  ___mSecondaryCameraOriginalRect_22;
	// System.Boolean Vuforia.DigitalEyewearARController::mSecondaryCameraDisabledLocally
	bool ___mSecondaryCameraDisabledLocally_23;
	// Vuforia.VuforiaARController Vuforia.DigitalEyewearARController::mVuforiaBehaviour
	VuforiaARController_t1876945237 * ___mVuforiaBehaviour_24;
	// Vuforia.DistortionRenderingBehaviour Vuforia.DigitalEyewearARController::mDistortionRenderingBhvr
	DistortionRenderingBehaviour_t1858983148 * ___mDistortionRenderingBhvr_25;
	// System.Boolean Vuforia.DigitalEyewearARController::mSetFocusPlaneAutomatically
	bool ___mSetFocusPlaneAutomatically_26;

public:
	inline static int32_t get_offset_of_mCameraOffset_7() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mCameraOffset_7)); }
	inline float get_mCameraOffset_7() const { return ___mCameraOffset_7; }
	inline float* get_address_of_mCameraOffset_7() { return &___mCameraOffset_7; }
	inline void set_mCameraOffset_7(float value)
	{
		___mCameraOffset_7 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingMode_8() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mDistortionRenderingMode_8)); }
	inline int32_t get_mDistortionRenderingMode_8() const { return ___mDistortionRenderingMode_8; }
	inline int32_t* get_address_of_mDistortionRenderingMode_8() { return &___mDistortionRenderingMode_8; }
	inline void set_mDistortionRenderingMode_8(int32_t value)
	{
		___mDistortionRenderingMode_8 = value;
	}

	inline static int32_t get_offset_of_mDistortionRenderingLayer_9() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mDistortionRenderingLayer_9)); }
	inline int32_t get_mDistortionRenderingLayer_9() const { return ___mDistortionRenderingLayer_9; }
	inline int32_t* get_address_of_mDistortionRenderingLayer_9() { return &___mDistortionRenderingLayer_9; }
	inline void set_mDistortionRenderingLayer_9(int32_t value)
	{
		___mDistortionRenderingLayer_9 = value;
	}

	inline static int32_t get_offset_of_mEyewearType_10() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mEyewearType_10)); }
	inline int32_t get_mEyewearType_10() const { return ___mEyewearType_10; }
	inline int32_t* get_address_of_mEyewearType_10() { return &___mEyewearType_10; }
	inline void set_mEyewearType_10(int32_t value)
	{
		___mEyewearType_10 = value;
	}

	inline static int32_t get_offset_of_mStereoFramework_11() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mStereoFramework_11)); }
	inline int32_t get_mStereoFramework_11() const { return ___mStereoFramework_11; }
	inline int32_t* get_address_of_mStereoFramework_11() { return &___mStereoFramework_11; }
	inline void set_mStereoFramework_11(int32_t value)
	{
		___mStereoFramework_11 = value;
	}

	inline static int32_t get_offset_of_mSeeThroughConfiguration_12() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mSeeThroughConfiguration_12)); }
	inline int32_t get_mSeeThroughConfiguration_12() const { return ___mSeeThroughConfiguration_12; }
	inline int32_t* get_address_of_mSeeThroughConfiguration_12() { return &___mSeeThroughConfiguration_12; }
	inline void set_mSeeThroughConfiguration_12(int32_t value)
	{
		___mSeeThroughConfiguration_12 = value;
	}

	inline static int32_t get_offset_of_mViewerName_13() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mViewerName_13)); }
	inline String_t* get_mViewerName_13() const { return ___mViewerName_13; }
	inline String_t** get_address_of_mViewerName_13() { return &___mViewerName_13; }
	inline void set_mViewerName_13(String_t* value)
	{
		___mViewerName_13 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerName_13), value);
	}

	inline static int32_t get_offset_of_mViewerManufacturer_14() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mViewerManufacturer_14)); }
	inline String_t* get_mViewerManufacturer_14() const { return ___mViewerManufacturer_14; }
	inline String_t** get_address_of_mViewerManufacturer_14() { return &___mViewerManufacturer_14; }
	inline void set_mViewerManufacturer_14(String_t* value)
	{
		___mViewerManufacturer_14 = value;
		Il2CppCodeGenWriteBarrier((&___mViewerManufacturer_14), value);
	}

	inline static int32_t get_offset_of_mUseCustomViewer_15() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mUseCustomViewer_15)); }
	inline bool get_mUseCustomViewer_15() const { return ___mUseCustomViewer_15; }
	inline bool* get_address_of_mUseCustomViewer_15() { return &___mUseCustomViewer_15; }
	inline void set_mUseCustomViewer_15(bool value)
	{
		___mUseCustomViewer_15 = value;
	}

	inline static int32_t get_offset_of_mCustomViewer_16() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mCustomViewer_16)); }
	inline SerializableViewerParameters_t2043332680 * get_mCustomViewer_16() const { return ___mCustomViewer_16; }
	inline SerializableViewerParameters_t2043332680 ** get_address_of_mCustomViewer_16() { return &___mCustomViewer_16; }
	inline void set_mCustomViewer_16(SerializableViewerParameters_t2043332680 * value)
	{
		___mCustomViewer_16 = value;
		Il2CppCodeGenWriteBarrier((&___mCustomViewer_16), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_17() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mCentralAnchorPoint_17)); }
	inline Transform_t3600365921 * get_mCentralAnchorPoint_17() const { return ___mCentralAnchorPoint_17; }
	inline Transform_t3600365921 ** get_address_of_mCentralAnchorPoint_17() { return &___mCentralAnchorPoint_17; }
	inline void set_mCentralAnchorPoint_17(Transform_t3600365921 * value)
	{
		___mCentralAnchorPoint_17 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_17), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_18() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mParentAnchorPoint_18)); }
	inline Transform_t3600365921 * get_mParentAnchorPoint_18() const { return ___mParentAnchorPoint_18; }
	inline Transform_t3600365921 ** get_address_of_mParentAnchorPoint_18() { return &___mParentAnchorPoint_18; }
	inline void set_mParentAnchorPoint_18(Transform_t3600365921 * value)
	{
		___mParentAnchorPoint_18 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_18), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_19() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mPrimaryCamera_19)); }
	inline Camera_t4157153871 * get_mPrimaryCamera_19() const { return ___mPrimaryCamera_19; }
	inline Camera_t4157153871 ** get_address_of_mPrimaryCamera_19() { return &___mPrimaryCamera_19; }
	inline void set_mPrimaryCamera_19(Camera_t4157153871 * value)
	{
		___mPrimaryCamera_19 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_19), value);
	}

	inline static int32_t get_offset_of_mPrimaryCameraOriginalRect_20() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mPrimaryCameraOriginalRect_20)); }
	inline Rect_t2360479859  get_mPrimaryCameraOriginalRect_20() const { return ___mPrimaryCameraOriginalRect_20; }
	inline Rect_t2360479859 * get_address_of_mPrimaryCameraOriginalRect_20() { return &___mPrimaryCameraOriginalRect_20; }
	inline void set_mPrimaryCameraOriginalRect_20(Rect_t2360479859  value)
	{
		___mPrimaryCameraOriginalRect_20 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCamera_21() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mSecondaryCamera_21)); }
	inline Camera_t4157153871 * get_mSecondaryCamera_21() const { return ___mSecondaryCamera_21; }
	inline Camera_t4157153871 ** get_address_of_mSecondaryCamera_21() { return &___mSecondaryCamera_21; }
	inline void set_mSecondaryCamera_21(Camera_t4157153871 * value)
	{
		___mSecondaryCamera_21 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_21), value);
	}

	inline static int32_t get_offset_of_mSecondaryCameraOriginalRect_22() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mSecondaryCameraOriginalRect_22)); }
	inline Rect_t2360479859  get_mSecondaryCameraOriginalRect_22() const { return ___mSecondaryCameraOriginalRect_22; }
	inline Rect_t2360479859 * get_address_of_mSecondaryCameraOriginalRect_22() { return &___mSecondaryCameraOriginalRect_22; }
	inline void set_mSecondaryCameraOriginalRect_22(Rect_t2360479859  value)
	{
		___mSecondaryCameraOriginalRect_22 = value;
	}

	inline static int32_t get_offset_of_mSecondaryCameraDisabledLocally_23() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mSecondaryCameraDisabledLocally_23)); }
	inline bool get_mSecondaryCameraDisabledLocally_23() const { return ___mSecondaryCameraDisabledLocally_23; }
	inline bool* get_address_of_mSecondaryCameraDisabledLocally_23() { return &___mSecondaryCameraDisabledLocally_23; }
	inline void set_mSecondaryCameraDisabledLocally_23(bool value)
	{
		___mSecondaryCameraDisabledLocally_23 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_24() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mVuforiaBehaviour_24)); }
	inline VuforiaARController_t1876945237 * get_mVuforiaBehaviour_24() const { return ___mVuforiaBehaviour_24; }
	inline VuforiaARController_t1876945237 ** get_address_of_mVuforiaBehaviour_24() { return &___mVuforiaBehaviour_24; }
	inline void set_mVuforiaBehaviour_24(VuforiaARController_t1876945237 * value)
	{
		___mVuforiaBehaviour_24 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_24), value);
	}

	inline static int32_t get_offset_of_mDistortionRenderingBhvr_25() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mDistortionRenderingBhvr_25)); }
	inline DistortionRenderingBehaviour_t1858983148 * get_mDistortionRenderingBhvr_25() const { return ___mDistortionRenderingBhvr_25; }
	inline DistortionRenderingBehaviour_t1858983148 ** get_address_of_mDistortionRenderingBhvr_25() { return &___mDistortionRenderingBhvr_25; }
	inline void set_mDistortionRenderingBhvr_25(DistortionRenderingBehaviour_t1858983148 * value)
	{
		___mDistortionRenderingBhvr_25 = value;
		Il2CppCodeGenWriteBarrier((&___mDistortionRenderingBhvr_25), value);
	}

	inline static int32_t get_offset_of_mSetFocusPlaneAutomatically_26() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036, ___mSetFocusPlaneAutomatically_26)); }
	inline bool get_mSetFocusPlaneAutomatically_26() const { return ___mSetFocusPlaneAutomatically_26; }
	inline bool* get_address_of_mSetFocusPlaneAutomatically_26() { return &___mSetFocusPlaneAutomatically_26; }
	inline void set_mSetFocusPlaneAutomatically_26(bool value)
	{
		___mSetFocusPlaneAutomatically_26 = value;
	}
};

struct DigitalEyewearARController_t1054226036_StaticFields
{
public:
	// Vuforia.DigitalEyewearARController Vuforia.DigitalEyewearARController::mInstance
	DigitalEyewearARController_t1054226036 * ___mInstance_27;
	// System.Object Vuforia.DigitalEyewearARController::mPadlock
	RuntimeObject * ___mPadlock_28;

public:
	inline static int32_t get_offset_of_mInstance_27() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036_StaticFields, ___mInstance_27)); }
	inline DigitalEyewearARController_t1054226036 * get_mInstance_27() const { return ___mInstance_27; }
	inline DigitalEyewearARController_t1054226036 ** get_address_of_mInstance_27() { return &___mInstance_27; }
	inline void set_mInstance_27(DigitalEyewearARController_t1054226036 * value)
	{
		___mInstance_27 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_27), value);
	}

	inline static int32_t get_offset_of_mPadlock_28() { return static_cast<int32_t>(offsetof(DigitalEyewearARController_t1054226036_StaticFields, ___mPadlock_28)); }
	inline RuntimeObject * get_mPadlock_28() const { return ___mPadlock_28; }
	inline RuntimeObject ** get_address_of_mPadlock_28() { return &___mPadlock_28; }
	inline void set_mPadlock_28(RuntimeObject * value)
	{
		___mPadlock_28 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARARCONTROLLER_T1054226036_H
#ifndef VIDEOCLIPPLAYABLE_T2598186649_H
#define VIDEOCLIPPLAYABLE_T2598186649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Video.VideoClipPlayable
struct  VideoClipPlayable_t2598186649 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t1095853803  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(VideoClipPlayable_t2598186649, ___m_Handle_0)); }
	inline PlayableHandle_t1095853803  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t1095853803 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t1095853803  value)
	{
		___m_Handle_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCLIPPLAYABLE_T2598186649_H
#ifndef POSEAGEENTRY_T2181165958_H
#define POSEAGEENTRY_T2181165958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry
struct  PoseAgeEntry_t2181165958 
{
public:
	// Vuforia.HoloLensExtendedTrackingManager/PoseInfo Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::Pose
	PoseInfo_t1612729179  ___Pose_0;
	// Vuforia.HoloLensExtendedTrackingManager/PoseInfo Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::CameraPose
	PoseInfo_t1612729179  ___CameraPose_1;
	// System.Int32 Vuforia.HoloLensExtendedTrackingManager/PoseAgeEntry::Age
	int32_t ___Age_2;

public:
	inline static int32_t get_offset_of_Pose_0() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t2181165958, ___Pose_0)); }
	inline PoseInfo_t1612729179  get_Pose_0() const { return ___Pose_0; }
	inline PoseInfo_t1612729179 * get_address_of_Pose_0() { return &___Pose_0; }
	inline void set_Pose_0(PoseInfo_t1612729179  value)
	{
		___Pose_0 = value;
	}

	inline static int32_t get_offset_of_CameraPose_1() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t2181165958, ___CameraPose_1)); }
	inline PoseInfo_t1612729179  get_CameraPose_1() const { return ___CameraPose_1; }
	inline PoseInfo_t1612729179 * get_address_of_CameraPose_1() { return &___CameraPose_1; }
	inline void set_CameraPose_1(PoseInfo_t1612729179  value)
	{
		___CameraPose_1 = value;
	}

	inline static int32_t get_offset_of_Age_2() { return static_cast<int32_t>(offsetof(PoseAgeEntry_t2181165958, ___Age_2)); }
	inline int32_t get_Age_2() const { return ___Age_2; }
	inline int32_t* get_address_of_Age_2() { return &___Age_2; }
	inline void set_Age_2(int32_t value)
	{
		___Age_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSEAGEENTRY_T2181165958_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef VUMARKTEMPLATEIMPL_T667343433_H
#define VUMARKTEMPLATEIMPL_T667343433_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuMarkTemplateImpl
struct  VuMarkTemplateImpl_t667343433  : public ObjectTargetImpl_t3614635090
{
public:
	// UnityEngine.Vector2 Vuforia.VuMarkTemplateImpl::mOrigin
	Vector2_t2156229523  ___mOrigin_4;
	// System.Boolean Vuforia.VuMarkTemplateImpl::mTrackingFromRuntimeAppearance
	bool ___mTrackingFromRuntimeAppearance_5;

public:
	inline static int32_t get_offset_of_mOrigin_4() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t667343433, ___mOrigin_4)); }
	inline Vector2_t2156229523  get_mOrigin_4() const { return ___mOrigin_4; }
	inline Vector2_t2156229523 * get_address_of_mOrigin_4() { return &___mOrigin_4; }
	inline void set_mOrigin_4(Vector2_t2156229523  value)
	{
		___mOrigin_4 = value;
	}

	inline static int32_t get_offset_of_mTrackingFromRuntimeAppearance_5() { return static_cast<int32_t>(offsetof(VuMarkTemplateImpl_t667343433, ___mTrackingFromRuntimeAppearance_5)); }
	inline bool get_mTrackingFromRuntimeAppearance_5() const { return ___mTrackingFromRuntimeAppearance_5; }
	inline bool* get_address_of_mTrackingFromRuntimeAppearance_5() { return &___mTrackingFromRuntimeAppearance_5; }
	inline void set_mTrackingFromRuntimeAppearance_5(bool value)
	{
		___mTrackingFromRuntimeAppearance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUMARKTEMPLATEIMPL_T667343433_H
#ifndef RECONSTRUCTIONFROMTARGETIMPL_T676137874_H
#define RECONSTRUCTIONFROMTARGETIMPL_T676137874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ReconstructionFromTargetImpl
struct  ReconstructionFromTargetImpl_t676137874  : public ReconstructionImpl_t3350922794
{
public:
	// UnityEngine.Vector3 Vuforia.ReconstructionFromTargetImpl::mOccluderMin
	Vector3_t3722313464  ___mOccluderMin_5;
	// UnityEngine.Vector3 Vuforia.ReconstructionFromTargetImpl::mOccluderMax
	Vector3_t3722313464  ___mOccluderMax_6;
	// UnityEngine.Vector3 Vuforia.ReconstructionFromTargetImpl::mOccluderOffset
	Vector3_t3722313464  ___mOccluderOffset_7;
	// UnityEngine.Quaternion Vuforia.ReconstructionFromTargetImpl::mOccluderRotation
	Quaternion_t2301928331  ___mOccluderRotation_8;
	// Vuforia.Trackable Vuforia.ReconstructionFromTargetImpl::mInitializationTarget
	RuntimeObject* ___mInitializationTarget_9;
	// System.Boolean Vuforia.ReconstructionFromTargetImpl::mCanAutoSetInitializationTarget
	bool ___mCanAutoSetInitializationTarget_10;

public:
	inline static int32_t get_offset_of_mOccluderMin_5() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t676137874, ___mOccluderMin_5)); }
	inline Vector3_t3722313464  get_mOccluderMin_5() const { return ___mOccluderMin_5; }
	inline Vector3_t3722313464 * get_address_of_mOccluderMin_5() { return &___mOccluderMin_5; }
	inline void set_mOccluderMin_5(Vector3_t3722313464  value)
	{
		___mOccluderMin_5 = value;
	}

	inline static int32_t get_offset_of_mOccluderMax_6() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t676137874, ___mOccluderMax_6)); }
	inline Vector3_t3722313464  get_mOccluderMax_6() const { return ___mOccluderMax_6; }
	inline Vector3_t3722313464 * get_address_of_mOccluderMax_6() { return &___mOccluderMax_6; }
	inline void set_mOccluderMax_6(Vector3_t3722313464  value)
	{
		___mOccluderMax_6 = value;
	}

	inline static int32_t get_offset_of_mOccluderOffset_7() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t676137874, ___mOccluderOffset_7)); }
	inline Vector3_t3722313464  get_mOccluderOffset_7() const { return ___mOccluderOffset_7; }
	inline Vector3_t3722313464 * get_address_of_mOccluderOffset_7() { return &___mOccluderOffset_7; }
	inline void set_mOccluderOffset_7(Vector3_t3722313464  value)
	{
		___mOccluderOffset_7 = value;
	}

	inline static int32_t get_offset_of_mOccluderRotation_8() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t676137874, ___mOccluderRotation_8)); }
	inline Quaternion_t2301928331  get_mOccluderRotation_8() const { return ___mOccluderRotation_8; }
	inline Quaternion_t2301928331 * get_address_of_mOccluderRotation_8() { return &___mOccluderRotation_8; }
	inline void set_mOccluderRotation_8(Quaternion_t2301928331  value)
	{
		___mOccluderRotation_8 = value;
	}

	inline static int32_t get_offset_of_mInitializationTarget_9() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t676137874, ___mInitializationTarget_9)); }
	inline RuntimeObject* get_mInitializationTarget_9() const { return ___mInitializationTarget_9; }
	inline RuntimeObject** get_address_of_mInitializationTarget_9() { return &___mInitializationTarget_9; }
	inline void set_mInitializationTarget_9(RuntimeObject* value)
	{
		___mInitializationTarget_9 = value;
		Il2CppCodeGenWriteBarrier((&___mInitializationTarget_9), value);
	}

	inline static int32_t get_offset_of_mCanAutoSetInitializationTarget_10() { return static_cast<int32_t>(offsetof(ReconstructionFromTargetImpl_t676137874, ___mCanAutoSetInitializationTarget_10)); }
	inline bool get_mCanAutoSetInitializationTarget_10() const { return ___mCanAutoSetInitializationTarget_10; }
	inline bool* get_address_of_mCanAutoSetInitializationTarget_10() { return &___mCanAutoSetInitializationTarget_10; }
	inline void set_mCanAutoSetInitializationTarget_10(bool value)
	{
		___mCanAutoSetInitializationTarget_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECONSTRUCTIONFROMTARGETIMPL_T676137874_H
#ifndef VIDEOBACKGROUNDCONFIGURATION_T1174662728_H
#define VIDEOBACKGROUNDCONFIGURATION_T1174662728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration
struct  VideoBackgroundConfiguration_t1174662728  : public RuntimeObject
{
public:
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration::clippingMode
	int32_t ___clippingMode_0;
	// UnityEngine.Shader Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration::matteShader
	Shader_t4151988712 * ___matteShader_1;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration::videoBackgroundEnabled
	bool ___videoBackgroundEnabled_2;

public:
	inline static int32_t get_offset_of_clippingMode_0() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_t1174662728, ___clippingMode_0)); }
	inline int32_t get_clippingMode_0() const { return ___clippingMode_0; }
	inline int32_t* get_address_of_clippingMode_0() { return &___clippingMode_0; }
	inline void set_clippingMode_0(int32_t value)
	{
		___clippingMode_0 = value;
	}

	inline static int32_t get_offset_of_matteShader_1() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_t1174662728, ___matteShader_1)); }
	inline Shader_t4151988712 * get_matteShader_1() const { return ___matteShader_1; }
	inline Shader_t4151988712 ** get_address_of_matteShader_1() { return &___matteShader_1; }
	inline void set_matteShader_1(Shader_t4151988712 * value)
	{
		___matteShader_1 = value;
		Il2CppCodeGenWriteBarrier((&___matteShader_1), value);
	}

	inline static int32_t get_offset_of_videoBackgroundEnabled_2() { return static_cast<int32_t>(offsetof(VideoBackgroundConfiguration_t1174662728, ___videoBackgroundEnabled_2)); }
	inline bool get_videoBackgroundEnabled_2() const { return ___videoBackgroundEnabled_2; }
	inline bool* get_address_of_videoBackgroundEnabled_2() { return &___videoBackgroundEnabled_2; }
	inline void set_videoBackgroundEnabled_2(bool value)
	{
		___videoBackgroundEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOBACKGROUNDCONFIGURATION_T1174662728_H
#ifndef DIGITALEYEWEARCONFIGURATION_T2828783036_H
#define DIGITALEYEWEARCONFIGURATION_T2828783036_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration
struct  DigitalEyewearConfiguration_t2828783036  : public RuntimeObject
{
public:
	// System.Single Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::cameraOffset
	float ___cameraOffset_0;
	// Vuforia.DistortionRenderingMode Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::distortionRenderingMode
	int32_t ___distortionRenderingMode_1;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::distortionRenderingLayer
	int32_t ___distortionRenderingLayer_2;
	// Vuforia.DigitalEyewearARController/EyewearType Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::eyewearType
	int32_t ___eyewearType_3;
	// Vuforia.DigitalEyewearARController/StereoFramework Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::stereoFramework
	int32_t ___stereoFramework_4;
	// Vuforia.DigitalEyewearARController/SeeThroughConfiguration Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::seeThroughConfiguration
	int32_t ___seeThroughConfiguration_5;
	// System.String Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::viewerName
	String_t* ___viewerName_6;
	// System.String Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::viewerManufacturer
	String_t* ___viewerManufacturer_7;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::useCustomViewer
	bool ___useCustomViewer_8;
	// Vuforia.DigitalEyewearARController/SerializableViewerParameters Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration::customViewer
	SerializableViewerParameters_t2043332680 * ___customViewer_9;

public:
	inline static int32_t get_offset_of_cameraOffset_0() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___cameraOffset_0)); }
	inline float get_cameraOffset_0() const { return ___cameraOffset_0; }
	inline float* get_address_of_cameraOffset_0() { return &___cameraOffset_0; }
	inline void set_cameraOffset_0(float value)
	{
		___cameraOffset_0 = value;
	}

	inline static int32_t get_offset_of_distortionRenderingMode_1() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___distortionRenderingMode_1)); }
	inline int32_t get_distortionRenderingMode_1() const { return ___distortionRenderingMode_1; }
	inline int32_t* get_address_of_distortionRenderingMode_1() { return &___distortionRenderingMode_1; }
	inline void set_distortionRenderingMode_1(int32_t value)
	{
		___distortionRenderingMode_1 = value;
	}

	inline static int32_t get_offset_of_distortionRenderingLayer_2() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___distortionRenderingLayer_2)); }
	inline int32_t get_distortionRenderingLayer_2() const { return ___distortionRenderingLayer_2; }
	inline int32_t* get_address_of_distortionRenderingLayer_2() { return &___distortionRenderingLayer_2; }
	inline void set_distortionRenderingLayer_2(int32_t value)
	{
		___distortionRenderingLayer_2 = value;
	}

	inline static int32_t get_offset_of_eyewearType_3() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___eyewearType_3)); }
	inline int32_t get_eyewearType_3() const { return ___eyewearType_3; }
	inline int32_t* get_address_of_eyewearType_3() { return &___eyewearType_3; }
	inline void set_eyewearType_3(int32_t value)
	{
		___eyewearType_3 = value;
	}

	inline static int32_t get_offset_of_stereoFramework_4() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___stereoFramework_4)); }
	inline int32_t get_stereoFramework_4() const { return ___stereoFramework_4; }
	inline int32_t* get_address_of_stereoFramework_4() { return &___stereoFramework_4; }
	inline void set_stereoFramework_4(int32_t value)
	{
		___stereoFramework_4 = value;
	}

	inline static int32_t get_offset_of_seeThroughConfiguration_5() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___seeThroughConfiguration_5)); }
	inline int32_t get_seeThroughConfiguration_5() const { return ___seeThroughConfiguration_5; }
	inline int32_t* get_address_of_seeThroughConfiguration_5() { return &___seeThroughConfiguration_5; }
	inline void set_seeThroughConfiguration_5(int32_t value)
	{
		___seeThroughConfiguration_5 = value;
	}

	inline static int32_t get_offset_of_viewerName_6() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___viewerName_6)); }
	inline String_t* get_viewerName_6() const { return ___viewerName_6; }
	inline String_t** get_address_of_viewerName_6() { return &___viewerName_6; }
	inline void set_viewerName_6(String_t* value)
	{
		___viewerName_6 = value;
		Il2CppCodeGenWriteBarrier((&___viewerName_6), value);
	}

	inline static int32_t get_offset_of_viewerManufacturer_7() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___viewerManufacturer_7)); }
	inline String_t* get_viewerManufacturer_7() const { return ___viewerManufacturer_7; }
	inline String_t** get_address_of_viewerManufacturer_7() { return &___viewerManufacturer_7; }
	inline void set_viewerManufacturer_7(String_t* value)
	{
		___viewerManufacturer_7 = value;
		Il2CppCodeGenWriteBarrier((&___viewerManufacturer_7), value);
	}

	inline static int32_t get_offset_of_useCustomViewer_8() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___useCustomViewer_8)); }
	inline bool get_useCustomViewer_8() const { return ___useCustomViewer_8; }
	inline bool* get_address_of_useCustomViewer_8() { return &___useCustomViewer_8; }
	inline void set_useCustomViewer_8(bool value)
	{
		___useCustomViewer_8 = value;
	}

	inline static int32_t get_offset_of_customViewer_9() { return static_cast<int32_t>(offsetof(DigitalEyewearConfiguration_t2828783036, ___customViewer_9)); }
	inline SerializableViewerParameters_t2043332680 * get_customViewer_9() const { return ___customViewer_9; }
	inline SerializableViewerParameters_t2043332680 ** get_address_of_customViewer_9() { return &___customViewer_9; }
	inline void set_customViewer_9(SerializableViewerParameters_t2043332680 * value)
	{
		___customViewer_9 = value;
		Il2CppCodeGenWriteBarrier((&___customViewer_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITALEYEWEARCONFIGURATION_T2828783036_H
#ifndef GENERICVUFORIACONFIGURATION_T1909783692_H
#define GENERICVUFORIACONFIGURATION_T1909783692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration
struct  GenericVuforiaConfiguration_t1909783692  : public RuntimeObject
{
public:
	// System.String Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::vuforiaLicenseKey
	String_t* ___vuforiaLicenseKey_0;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::delayedInitialization
	bool ___delayedInitialization_1;
	// Vuforia.CameraDevice/CameraDeviceMode Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::cameraDeviceModeSetting
	int32_t ___cameraDeviceModeSetting_2;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::maxSimultaneousImageTargets
	int32_t ___maxSimultaneousImageTargets_3;
	// System.Int32 Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::maxSimultaneousObjectTargets
	int32_t ___maxSimultaneousObjectTargets_4;
	// System.Boolean Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::useDelayedLoadingObjectTargets
	bool ___useDelayedLoadingObjectTargets_5;
	// Vuforia.CameraDevice/CameraDirection Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::cameraDirection
	int32_t ___cameraDirection_6;
	// Vuforia.VuforiaRenderer/VideoBackgroundReflection Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration::mirrorVideoBackground
	int32_t ___mirrorVideoBackground_7;

public:
	inline static int32_t get_offset_of_vuforiaLicenseKey_0() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___vuforiaLicenseKey_0)); }
	inline String_t* get_vuforiaLicenseKey_0() const { return ___vuforiaLicenseKey_0; }
	inline String_t** get_address_of_vuforiaLicenseKey_0() { return &___vuforiaLicenseKey_0; }
	inline void set_vuforiaLicenseKey_0(String_t* value)
	{
		___vuforiaLicenseKey_0 = value;
		Il2CppCodeGenWriteBarrier((&___vuforiaLicenseKey_0), value);
	}

	inline static int32_t get_offset_of_delayedInitialization_1() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___delayedInitialization_1)); }
	inline bool get_delayedInitialization_1() const { return ___delayedInitialization_1; }
	inline bool* get_address_of_delayedInitialization_1() { return &___delayedInitialization_1; }
	inline void set_delayedInitialization_1(bool value)
	{
		___delayedInitialization_1 = value;
	}

	inline static int32_t get_offset_of_cameraDeviceModeSetting_2() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___cameraDeviceModeSetting_2)); }
	inline int32_t get_cameraDeviceModeSetting_2() const { return ___cameraDeviceModeSetting_2; }
	inline int32_t* get_address_of_cameraDeviceModeSetting_2() { return &___cameraDeviceModeSetting_2; }
	inline void set_cameraDeviceModeSetting_2(int32_t value)
	{
		___cameraDeviceModeSetting_2 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousImageTargets_3() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___maxSimultaneousImageTargets_3)); }
	inline int32_t get_maxSimultaneousImageTargets_3() const { return ___maxSimultaneousImageTargets_3; }
	inline int32_t* get_address_of_maxSimultaneousImageTargets_3() { return &___maxSimultaneousImageTargets_3; }
	inline void set_maxSimultaneousImageTargets_3(int32_t value)
	{
		___maxSimultaneousImageTargets_3 = value;
	}

	inline static int32_t get_offset_of_maxSimultaneousObjectTargets_4() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___maxSimultaneousObjectTargets_4)); }
	inline int32_t get_maxSimultaneousObjectTargets_4() const { return ___maxSimultaneousObjectTargets_4; }
	inline int32_t* get_address_of_maxSimultaneousObjectTargets_4() { return &___maxSimultaneousObjectTargets_4; }
	inline void set_maxSimultaneousObjectTargets_4(int32_t value)
	{
		___maxSimultaneousObjectTargets_4 = value;
	}

	inline static int32_t get_offset_of_useDelayedLoadingObjectTargets_5() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___useDelayedLoadingObjectTargets_5)); }
	inline bool get_useDelayedLoadingObjectTargets_5() const { return ___useDelayedLoadingObjectTargets_5; }
	inline bool* get_address_of_useDelayedLoadingObjectTargets_5() { return &___useDelayedLoadingObjectTargets_5; }
	inline void set_useDelayedLoadingObjectTargets_5(bool value)
	{
		___useDelayedLoadingObjectTargets_5 = value;
	}

	inline static int32_t get_offset_of_cameraDirection_6() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___cameraDirection_6)); }
	inline int32_t get_cameraDirection_6() const { return ___cameraDirection_6; }
	inline int32_t* get_address_of_cameraDirection_6() { return &___cameraDirection_6; }
	inline void set_cameraDirection_6(int32_t value)
	{
		___cameraDirection_6 = value;
	}

	inline static int32_t get_offset_of_mirrorVideoBackground_7() { return static_cast<int32_t>(offsetof(GenericVuforiaConfiguration_t1909783692, ___mirrorVideoBackground_7)); }
	inline int32_t get_mirrorVideoBackground_7() const { return ___mirrorVideoBackground_7; }
	inline int32_t* get_address_of_mirrorVideoBackground_7() { return &___mirrorVideoBackground_7; }
	inline void set_mirrorVideoBackground_7(int32_t value)
	{
		___mirrorVideoBackground_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICVUFORIACONFIGURATION_T1909783692_H
#ifndef NULLCAMERACONFIGURATION_T2773452281_H
#define NULLCAMERACONFIGURATION_T2773452281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.NullCameraConfiguration
struct  NullCameraConfiguration_t2773452281  : public RuntimeObject
{
public:
	// UnityEngine.ScreenOrientation Vuforia.NullCameraConfiguration::mProjectionOrientation
	int32_t ___mProjectionOrientation_0;

public:
	inline static int32_t get_offset_of_mProjectionOrientation_0() { return static_cast<int32_t>(offsetof(NullCameraConfiguration_t2773452281, ___mProjectionOrientation_0)); }
	inline int32_t get_mProjectionOrientation_0() const { return ___mProjectionOrientation_0; }
	inline int32_t* get_address_of_mProjectionOrientation_0() { return &___mProjectionOrientation_0; }
	inline void set_mProjectionOrientation_0(int32_t value)
	{
		___mProjectionOrientation_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLCAMERACONFIGURATION_T2773452281_H
#ifndef CUSTOMVIEWERPARAMETERS_T463762113_H
#define CUSTOMVIEWERPARAMETERS_T463762113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CustomViewerParameters
struct  CustomViewerParameters_t463762113  : public ViewerParameters_t3396315024
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMVIEWERPARAMETERS_T463762113_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef INSTANCEIDIMPL_T2824054591_H
#define INSTANCEIDIMPL_T2824054591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.InstanceIdImpl
struct  InstanceIdImpl_t2824054591  : public RuntimeObject
{
public:
	// Vuforia.InstanceIdType Vuforia.InstanceIdImpl::mDataType
	int32_t ___mDataType_0;
	// System.Byte[] Vuforia.InstanceIdImpl::mBuffer
	ByteU5BU5D_t4116647657* ___mBuffer_1;
	// System.UInt64 Vuforia.InstanceIdImpl::mNumericValue
	uint64_t ___mNumericValue_2;
	// System.UInt32 Vuforia.InstanceIdImpl::mDataLength
	uint32_t ___mDataLength_3;
	// System.String Vuforia.InstanceIdImpl::mCachedStringValue
	String_t* ___mCachedStringValue_4;

public:
	inline static int32_t get_offset_of_mDataType_0() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2824054591, ___mDataType_0)); }
	inline int32_t get_mDataType_0() const { return ___mDataType_0; }
	inline int32_t* get_address_of_mDataType_0() { return &___mDataType_0; }
	inline void set_mDataType_0(int32_t value)
	{
		___mDataType_0 = value;
	}

	inline static int32_t get_offset_of_mBuffer_1() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2824054591, ___mBuffer_1)); }
	inline ByteU5BU5D_t4116647657* get_mBuffer_1() const { return ___mBuffer_1; }
	inline ByteU5BU5D_t4116647657** get_address_of_mBuffer_1() { return &___mBuffer_1; }
	inline void set_mBuffer_1(ByteU5BU5D_t4116647657* value)
	{
		___mBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___mBuffer_1), value);
	}

	inline static int32_t get_offset_of_mNumericValue_2() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2824054591, ___mNumericValue_2)); }
	inline uint64_t get_mNumericValue_2() const { return ___mNumericValue_2; }
	inline uint64_t* get_address_of_mNumericValue_2() { return &___mNumericValue_2; }
	inline void set_mNumericValue_2(uint64_t value)
	{
		___mNumericValue_2 = value;
	}

	inline static int32_t get_offset_of_mDataLength_3() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2824054591, ___mDataLength_3)); }
	inline uint32_t get_mDataLength_3() const { return ___mDataLength_3; }
	inline uint32_t* get_address_of_mDataLength_3() { return &___mDataLength_3; }
	inline void set_mDataLength_3(uint32_t value)
	{
		___mDataLength_3 = value;
	}

	inline static int32_t get_offset_of_mCachedStringValue_4() { return static_cast<int32_t>(offsetof(InstanceIdImpl_t2824054591, ___mCachedStringValue_4)); }
	inline String_t* get_mCachedStringValue_4() const { return ___mCachedStringValue_4; }
	inline String_t** get_address_of_mCachedStringValue_4() { return &___mCachedStringValue_4; }
	inline void set_mCachedStringValue_4(String_t* value)
	{
		___mCachedStringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCachedStringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEIDIMPL_T2824054591_H
#ifndef ROTATIONALPLAYMODEDEVICETRACKERIMPL_T4048275232_H
#define ROTATIONALPLAYMODEDEVICETRACKERIMPL_T4048275232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalPlayModeDeviceTrackerImpl
struct  RotationalPlayModeDeviceTrackerImpl_t4048275232  : public RotationalDeviceTracker_t2847210804
{
public:
	// UnityEngine.Vector3 Vuforia.RotationalPlayModeDeviceTrackerImpl::mRotation
	Vector3_t3722313464  ___mRotation_1;
	// UnityEngine.Vector3 Vuforia.RotationalPlayModeDeviceTrackerImpl::mModelCorrectionTransform
	Vector3_t3722313464  ___mModelCorrectionTransform_2;
	// Vuforia.RotationalDeviceTracker/MODEL_CORRECTION_MODE Vuforia.RotationalPlayModeDeviceTrackerImpl::mModelCorrection
	int32_t ___mModelCorrection_3;

public:
	inline static int32_t get_offset_of_mRotation_1() { return static_cast<int32_t>(offsetof(RotationalPlayModeDeviceTrackerImpl_t4048275232, ___mRotation_1)); }
	inline Vector3_t3722313464  get_mRotation_1() const { return ___mRotation_1; }
	inline Vector3_t3722313464 * get_address_of_mRotation_1() { return &___mRotation_1; }
	inline void set_mRotation_1(Vector3_t3722313464  value)
	{
		___mRotation_1 = value;
	}

	inline static int32_t get_offset_of_mModelCorrectionTransform_2() { return static_cast<int32_t>(offsetof(RotationalPlayModeDeviceTrackerImpl_t4048275232, ___mModelCorrectionTransform_2)); }
	inline Vector3_t3722313464  get_mModelCorrectionTransform_2() const { return ___mModelCorrectionTransform_2; }
	inline Vector3_t3722313464 * get_address_of_mModelCorrectionTransform_2() { return &___mModelCorrectionTransform_2; }
	inline void set_mModelCorrectionTransform_2(Vector3_t3722313464  value)
	{
		___mModelCorrectionTransform_2 = value;
	}

	inline static int32_t get_offset_of_mModelCorrection_3() { return static_cast<int32_t>(offsetof(RotationalPlayModeDeviceTrackerImpl_t4048275232, ___mModelCorrection_3)); }
	inline int32_t get_mModelCorrection_3() const { return ___mModelCorrection_3; }
	inline int32_t* get_address_of_mModelCorrection_3() { return &___mModelCorrection_3; }
	inline void set_mModelCorrection_3(int32_t value)
	{
		___mModelCorrection_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALPLAYMODEDEVICETRACKERIMPL_T4048275232_H
#ifndef ROTATIONALDEVICETRACKERIMPL_T1573407597_H
#define ROTATIONALDEVICETRACKERIMPL_T1573407597_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.RotationalDeviceTrackerImpl
struct  RotationalDeviceTrackerImpl_t1573407597  : public RotationalDeviceTracker_t2847210804
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONALDEVICETRACKERIMPL_T1573407597_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef TIMEEVENTHANDLER_T445758600_H
#define TIMEEVENTHANDLER_T445758600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct  TimeEventHandler_t445758600  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEEVENTHANDLER_T445758600_H
#ifndef MONOCAMERACONFIGURATION_T112386736_H
#define MONOCAMERACONFIGURATION_T112386736_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.MonoCameraConfiguration
struct  MonoCameraConfiguration_t112386736  : public BaseCameraConfiguration_t3118151474
{
public:
	// UnityEngine.Camera Vuforia.MonoCameraConfiguration::mPrimaryCamera
	Camera_t4157153871 * ___mPrimaryCamera_10;
	// System.Int32 Vuforia.MonoCameraConfiguration::mCameraViewPortWidth
	int32_t ___mCameraViewPortWidth_11;
	// System.Int32 Vuforia.MonoCameraConfiguration::mCameraViewPortHeight
	int32_t ___mCameraViewPortHeight_12;
	// System.Single Vuforia.MonoCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_13;
	// System.Single Vuforia.MonoCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_14;
	// System.Single Vuforia.MonoCameraConfiguration::mLastAppliedFoV
	float ___mLastAppliedFoV_15;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_10() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t112386736, ___mPrimaryCamera_10)); }
	inline Camera_t4157153871 * get_mPrimaryCamera_10() const { return ___mPrimaryCamera_10; }
	inline Camera_t4157153871 ** get_address_of_mPrimaryCamera_10() { return &___mPrimaryCamera_10; }
	inline void set_mPrimaryCamera_10(Camera_t4157153871 * value)
	{
		___mPrimaryCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_10), value);
	}

	inline static int32_t get_offset_of_mCameraViewPortWidth_11() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t112386736, ___mCameraViewPortWidth_11)); }
	inline int32_t get_mCameraViewPortWidth_11() const { return ___mCameraViewPortWidth_11; }
	inline int32_t* get_address_of_mCameraViewPortWidth_11() { return &___mCameraViewPortWidth_11; }
	inline void set_mCameraViewPortWidth_11(int32_t value)
	{
		___mCameraViewPortWidth_11 = value;
	}

	inline static int32_t get_offset_of_mCameraViewPortHeight_12() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t112386736, ___mCameraViewPortHeight_12)); }
	inline int32_t get_mCameraViewPortHeight_12() const { return ___mCameraViewPortHeight_12; }
	inline int32_t* get_address_of_mCameraViewPortHeight_12() { return &___mCameraViewPortHeight_12; }
	inline void set_mCameraViewPortHeight_12(int32_t value)
	{
		___mCameraViewPortHeight_12 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedNearClipPlane_13() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t112386736, ___mLastAppliedNearClipPlane_13)); }
	inline float get_mLastAppliedNearClipPlane_13() const { return ___mLastAppliedNearClipPlane_13; }
	inline float* get_address_of_mLastAppliedNearClipPlane_13() { return &___mLastAppliedNearClipPlane_13; }
	inline void set_mLastAppliedNearClipPlane_13(float value)
	{
		___mLastAppliedNearClipPlane_13 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFarClipPlane_14() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t112386736, ___mLastAppliedFarClipPlane_14)); }
	inline float get_mLastAppliedFarClipPlane_14() const { return ___mLastAppliedFarClipPlane_14; }
	inline float* get_address_of_mLastAppliedFarClipPlane_14() { return &___mLastAppliedFarClipPlane_14; }
	inline void set_mLastAppliedFarClipPlane_14(float value)
	{
		___mLastAppliedFarClipPlane_14 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFoV_15() { return static_cast<int32_t>(offsetof(MonoCameraConfiguration_t112386736, ___mLastAppliedFoV_15)); }
	inline float get_mLastAppliedFoV_15() const { return ___mLastAppliedFoV_15; }
	inline float* get_address_of_mLastAppliedFoV_15() { return &___mLastAppliedFoV_15; }
	inline void set_mLastAppliedFoV_15(float value)
	{
		___mLastAppliedFoV_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOCAMERACONFIGURATION_T112386736_H
#ifndef UPDATEDEVENTHANDLER_T1027848393_H
#define UPDATEDEVENTHANDLER_T1027848393_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RemoteSettings/UpdatedEventHandler
struct  UpdatedEventHandler_t1027848393  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEDEVENTHANDLER_T1027848393_H
#ifndef DEDICATEDEYEWEARCAMERACONFIGURATION_T2854098828_H
#define DEDICATEDEYEWEARCAMERACONFIGURATION_T2854098828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DedicatedEyewearCameraConfiguration
struct  DedicatedEyewearCameraConfiguration_t2854098828  : public BaseCameraConfiguration_t3118151474
{
public:
	// UnityEngine.Camera Vuforia.DedicatedEyewearCameraConfiguration::mPrimaryCamera
	Camera_t4157153871 * ___mPrimaryCamera_10;
	// UnityEngine.Camera Vuforia.DedicatedEyewearCameraConfiguration::mSecondaryCamera
	Camera_t4157153871 * ___mSecondaryCamera_11;
	// System.Int32 Vuforia.DedicatedEyewearCameraConfiguration::mScreenWidth
	int32_t ___mScreenWidth_12;
	// System.Int32 Vuforia.DedicatedEyewearCameraConfiguration::mScreenHeight
	int32_t ___mScreenHeight_13;
	// System.Boolean Vuforia.DedicatedEyewearCameraConfiguration::mNeedToCheckStereo
	bool ___mNeedToCheckStereo_14;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mLastAppliedNearClipPlane
	float ___mLastAppliedNearClipPlane_15;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mLastAppliedFarClipPlane
	float ___mLastAppliedFarClipPlane_16;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mNewNearClipPlane
	float ___mNewNearClipPlane_17;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mNewFarClipPlane
	float ___mNewFarClipPlane_18;
	// System.Single Vuforia.DedicatedEyewearCameraConfiguration::mNewVirtualFoV
	float ___mNewVirtualFoV_19;
	// Vuforia.EyewearDevice Vuforia.DedicatedEyewearCameraConfiguration::mEyewearDevice
	EyewearDevice_t3223385723 * ___mEyewearDevice_20;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_10() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mPrimaryCamera_10)); }
	inline Camera_t4157153871 * get_mPrimaryCamera_10() const { return ___mPrimaryCamera_10; }
	inline Camera_t4157153871 ** get_address_of_mPrimaryCamera_10() { return &___mPrimaryCamera_10; }
	inline void set_mPrimaryCamera_10(Camera_t4157153871 * value)
	{
		___mPrimaryCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_10), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_11() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mSecondaryCamera_11)); }
	inline Camera_t4157153871 * get_mSecondaryCamera_11() const { return ___mSecondaryCamera_11; }
	inline Camera_t4157153871 ** get_address_of_mSecondaryCamera_11() { return &___mSecondaryCamera_11; }
	inline void set_mSecondaryCamera_11(Camera_t4157153871 * value)
	{
		___mSecondaryCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_11), value);
	}

	inline static int32_t get_offset_of_mScreenWidth_12() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mScreenWidth_12)); }
	inline int32_t get_mScreenWidth_12() const { return ___mScreenWidth_12; }
	inline int32_t* get_address_of_mScreenWidth_12() { return &___mScreenWidth_12; }
	inline void set_mScreenWidth_12(int32_t value)
	{
		___mScreenWidth_12 = value;
	}

	inline static int32_t get_offset_of_mScreenHeight_13() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mScreenHeight_13)); }
	inline int32_t get_mScreenHeight_13() const { return ___mScreenHeight_13; }
	inline int32_t* get_address_of_mScreenHeight_13() { return &___mScreenHeight_13; }
	inline void set_mScreenHeight_13(int32_t value)
	{
		___mScreenHeight_13 = value;
	}

	inline static int32_t get_offset_of_mNeedToCheckStereo_14() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mNeedToCheckStereo_14)); }
	inline bool get_mNeedToCheckStereo_14() const { return ___mNeedToCheckStereo_14; }
	inline bool* get_address_of_mNeedToCheckStereo_14() { return &___mNeedToCheckStereo_14; }
	inline void set_mNeedToCheckStereo_14(bool value)
	{
		___mNeedToCheckStereo_14 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedNearClipPlane_15() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mLastAppliedNearClipPlane_15)); }
	inline float get_mLastAppliedNearClipPlane_15() const { return ___mLastAppliedNearClipPlane_15; }
	inline float* get_address_of_mLastAppliedNearClipPlane_15() { return &___mLastAppliedNearClipPlane_15; }
	inline void set_mLastAppliedNearClipPlane_15(float value)
	{
		___mLastAppliedNearClipPlane_15 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedFarClipPlane_16() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mLastAppliedFarClipPlane_16)); }
	inline float get_mLastAppliedFarClipPlane_16() const { return ___mLastAppliedFarClipPlane_16; }
	inline float* get_address_of_mLastAppliedFarClipPlane_16() { return &___mLastAppliedFarClipPlane_16; }
	inline void set_mLastAppliedFarClipPlane_16(float value)
	{
		___mLastAppliedFarClipPlane_16 = value;
	}

	inline static int32_t get_offset_of_mNewNearClipPlane_17() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mNewNearClipPlane_17)); }
	inline float get_mNewNearClipPlane_17() const { return ___mNewNearClipPlane_17; }
	inline float* get_address_of_mNewNearClipPlane_17() { return &___mNewNearClipPlane_17; }
	inline void set_mNewNearClipPlane_17(float value)
	{
		___mNewNearClipPlane_17 = value;
	}

	inline static int32_t get_offset_of_mNewFarClipPlane_18() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mNewFarClipPlane_18)); }
	inline float get_mNewFarClipPlane_18() const { return ___mNewFarClipPlane_18; }
	inline float* get_address_of_mNewFarClipPlane_18() { return &___mNewFarClipPlane_18; }
	inline void set_mNewFarClipPlane_18(float value)
	{
		___mNewFarClipPlane_18 = value;
	}

	inline static int32_t get_offset_of_mNewVirtualFoV_19() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mNewVirtualFoV_19)); }
	inline float get_mNewVirtualFoV_19() const { return ___mNewVirtualFoV_19; }
	inline float* get_address_of_mNewVirtualFoV_19() { return &___mNewVirtualFoV_19; }
	inline void set_mNewVirtualFoV_19(float value)
	{
		___mNewVirtualFoV_19 = value;
	}

	inline static int32_t get_offset_of_mEyewearDevice_20() { return static_cast<int32_t>(offsetof(DedicatedEyewearCameraConfiguration_t2854098828, ___mEyewearDevice_20)); }
	inline EyewearDevice_t3223385723 * get_mEyewearDevice_20() const { return ___mEyewearDevice_20; }
	inline EyewearDevice_t3223385723 ** get_address_of_mEyewearDevice_20() { return &___mEyewearDevice_20; }
	inline void set_mEyewearDevice_20(EyewearDevice_t3223385723 * value)
	{
		___mEyewearDevice_20 = value;
		Il2CppCodeGenWriteBarrier((&___mEyewearDevice_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEDICATEDEYEWEARCAMERACONFIGURATION_T2854098828_H
#ifndef FRAMEREADYEVENTHANDLER_T3848515759_H
#define FRAMEREADYEVENTHANDLER_T3848515759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct  FrameReadyEventHandler_t3848515759  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FRAMEREADYEVENTHANDLER_T3848515759_H
#ifndef SESSIONSTATECHANGED_T3163629820_H
#define SESSIONSTATECHANGED_T3163629820_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.AnalyticsSessionInfo/SessionStateChanged
struct  SessionStateChanged_t3163629820  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SESSIONSTATECHANGED_T3163629820_H
#ifndef VUFORIAABSTRACTCONFIGURATION_T2684447159_H
#define VUFORIAABSTRACTCONFIGURATION_T2684447159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractConfiguration
struct  VuforiaAbstractConfiguration_t2684447159  : public ScriptableObject_t2528358522
{
public:
	// Vuforia.VuforiaAbstractConfiguration/GenericVuforiaConfiguration Vuforia.VuforiaAbstractConfiguration::vuforia
	GenericVuforiaConfiguration_t1909783692 * ___vuforia_4;
	// Vuforia.VuforiaAbstractConfiguration/DigitalEyewearConfiguration Vuforia.VuforiaAbstractConfiguration::digitalEyewear
	DigitalEyewearConfiguration_t2828783036 * ___digitalEyewear_5;
	// Vuforia.VuforiaAbstractConfiguration/DatabaseLoadConfiguration Vuforia.VuforiaAbstractConfiguration::databaseLoad
	DatabaseLoadConfiguration_t3815674388 * ___databaseLoad_6;
	// Vuforia.VuforiaAbstractConfiguration/VideoBackgroundConfiguration Vuforia.VuforiaAbstractConfiguration::videoBackground
	VideoBackgroundConfiguration_t1174662728 * ___videoBackground_7;
	// Vuforia.VuforiaAbstractConfiguration/SmartTerrainTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::smartTerrainTracker
	SmartTerrainTrackerConfiguration_t3112457179 * ___smartTerrainTracker_8;
	// Vuforia.VuforiaAbstractConfiguration/DeviceTrackerConfiguration Vuforia.VuforiaAbstractConfiguration::deviceTracker
	DeviceTrackerConfiguration_t1841344395 * ___deviceTracker_9;
	// Vuforia.VuforiaAbstractConfiguration/WebCamConfiguration Vuforia.VuforiaAbstractConfiguration::webcam
	WebCamConfiguration_t802847339 * ___webcam_10;

public:
	inline static int32_t get_offset_of_vuforia_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___vuforia_4)); }
	inline GenericVuforiaConfiguration_t1909783692 * get_vuforia_4() const { return ___vuforia_4; }
	inline GenericVuforiaConfiguration_t1909783692 ** get_address_of_vuforia_4() { return &___vuforia_4; }
	inline void set_vuforia_4(GenericVuforiaConfiguration_t1909783692 * value)
	{
		___vuforia_4 = value;
		Il2CppCodeGenWriteBarrier((&___vuforia_4), value);
	}

	inline static int32_t get_offset_of_digitalEyewear_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___digitalEyewear_5)); }
	inline DigitalEyewearConfiguration_t2828783036 * get_digitalEyewear_5() const { return ___digitalEyewear_5; }
	inline DigitalEyewearConfiguration_t2828783036 ** get_address_of_digitalEyewear_5() { return &___digitalEyewear_5; }
	inline void set_digitalEyewear_5(DigitalEyewearConfiguration_t2828783036 * value)
	{
		___digitalEyewear_5 = value;
		Il2CppCodeGenWriteBarrier((&___digitalEyewear_5), value);
	}

	inline static int32_t get_offset_of_databaseLoad_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___databaseLoad_6)); }
	inline DatabaseLoadConfiguration_t3815674388 * get_databaseLoad_6() const { return ___databaseLoad_6; }
	inline DatabaseLoadConfiguration_t3815674388 ** get_address_of_databaseLoad_6() { return &___databaseLoad_6; }
	inline void set_databaseLoad_6(DatabaseLoadConfiguration_t3815674388 * value)
	{
		___databaseLoad_6 = value;
		Il2CppCodeGenWriteBarrier((&___databaseLoad_6), value);
	}

	inline static int32_t get_offset_of_videoBackground_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___videoBackground_7)); }
	inline VideoBackgroundConfiguration_t1174662728 * get_videoBackground_7() const { return ___videoBackground_7; }
	inline VideoBackgroundConfiguration_t1174662728 ** get_address_of_videoBackground_7() { return &___videoBackground_7; }
	inline void set_videoBackground_7(VideoBackgroundConfiguration_t1174662728 * value)
	{
		___videoBackground_7 = value;
		Il2CppCodeGenWriteBarrier((&___videoBackground_7), value);
	}

	inline static int32_t get_offset_of_smartTerrainTracker_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___smartTerrainTracker_8)); }
	inline SmartTerrainTrackerConfiguration_t3112457179 * get_smartTerrainTracker_8() const { return ___smartTerrainTracker_8; }
	inline SmartTerrainTrackerConfiguration_t3112457179 ** get_address_of_smartTerrainTracker_8() { return &___smartTerrainTracker_8; }
	inline void set_smartTerrainTracker_8(SmartTerrainTrackerConfiguration_t3112457179 * value)
	{
		___smartTerrainTracker_8 = value;
		Il2CppCodeGenWriteBarrier((&___smartTerrainTracker_8), value);
	}

	inline static int32_t get_offset_of_deviceTracker_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___deviceTracker_9)); }
	inline DeviceTrackerConfiguration_t1841344395 * get_deviceTracker_9() const { return ___deviceTracker_9; }
	inline DeviceTrackerConfiguration_t1841344395 ** get_address_of_deviceTracker_9() { return &___deviceTracker_9; }
	inline void set_deviceTracker_9(DeviceTrackerConfiguration_t1841344395 * value)
	{
		___deviceTracker_9 = value;
		Il2CppCodeGenWriteBarrier((&___deviceTracker_9), value);
	}

	inline static int32_t get_offset_of_webcam_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159, ___webcam_10)); }
	inline WebCamConfiguration_t802847339 * get_webcam_10() const { return ___webcam_10; }
	inline WebCamConfiguration_t802847339 ** get_address_of_webcam_10() { return &___webcam_10; }
	inline void set_webcam_10(WebCamConfiguration_t802847339 * value)
	{
		___webcam_10 = value;
		Il2CppCodeGenWriteBarrier((&___webcam_10), value);
	}
};

struct VuforiaAbstractConfiguration_t2684447159_StaticFields
{
public:
	// Vuforia.VuforiaAbstractConfiguration Vuforia.VuforiaAbstractConfiguration::mInstance
	VuforiaAbstractConfiguration_t2684447159 * ___mInstance_2;
	// System.Object Vuforia.VuforiaAbstractConfiguration::mPadlock
	RuntimeObject * ___mPadlock_3;

public:
	inline static int32_t get_offset_of_mInstance_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159_StaticFields, ___mInstance_2)); }
	inline VuforiaAbstractConfiguration_t2684447159 * get_mInstance_2() const { return ___mInstance_2; }
	inline VuforiaAbstractConfiguration_t2684447159 ** get_address_of_mInstance_2() { return &___mInstance_2; }
	inline void set_mInstance_2(VuforiaAbstractConfiguration_t2684447159 * value)
	{
		___mInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___mInstance_2), value);
	}

	inline static int32_t get_offset_of_mPadlock_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractConfiguration_t2684447159_StaticFields, ___mPadlock_3)); }
	inline RuntimeObject * get_mPadlock_3() const { return ___mPadlock_3; }
	inline RuntimeObject ** get_address_of_mPadlock_3() { return &___mPadlock_3; }
	inline void set_mPadlock_3(RuntimeObject * value)
	{
		___mPadlock_3 = value;
		Il2CppCodeGenWriteBarrier((&___mPadlock_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAABSTRACTCONFIGURATION_T2684447159_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef BASESTEREOVIEWERCAMERACONFIGURATION_T2611724717_H
#define BASESTEREOVIEWERCAMERACONFIGURATION_T2611724717_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BaseStereoViewerCameraConfiguration
struct  BaseStereoViewerCameraConfiguration_t2611724717  : public BaseCameraConfiguration_t3118151474
{
public:
	// UnityEngine.Camera Vuforia.BaseStereoViewerCameraConfiguration::mPrimaryCamera
	Camera_t4157153871 * ___mPrimaryCamera_10;
	// UnityEngine.Camera Vuforia.BaseStereoViewerCameraConfiguration::mSecondaryCamera
	Camera_t4157153871 * ___mSecondaryCamera_11;
	// System.Boolean Vuforia.BaseStereoViewerCameraConfiguration::mSkewFrustum
	bool ___mSkewFrustum_12;
	// System.Int32 Vuforia.BaseStereoViewerCameraConfiguration::mScreenWidth
	int32_t ___mScreenWidth_13;
	// System.Int32 Vuforia.BaseStereoViewerCameraConfiguration::mScreenHeight
	int32_t ___mScreenHeight_14;

public:
	inline static int32_t get_offset_of_mPrimaryCamera_10() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t2611724717, ___mPrimaryCamera_10)); }
	inline Camera_t4157153871 * get_mPrimaryCamera_10() const { return ___mPrimaryCamera_10; }
	inline Camera_t4157153871 ** get_address_of_mPrimaryCamera_10() { return &___mPrimaryCamera_10; }
	inline void set_mPrimaryCamera_10(Camera_t4157153871 * value)
	{
		___mPrimaryCamera_10 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_10), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_11() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t2611724717, ___mSecondaryCamera_11)); }
	inline Camera_t4157153871 * get_mSecondaryCamera_11() const { return ___mSecondaryCamera_11; }
	inline Camera_t4157153871 ** get_address_of_mSecondaryCamera_11() { return &___mSecondaryCamera_11; }
	inline void set_mSecondaryCamera_11(Camera_t4157153871 * value)
	{
		___mSecondaryCamera_11 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_11), value);
	}

	inline static int32_t get_offset_of_mSkewFrustum_12() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t2611724717, ___mSkewFrustum_12)); }
	inline bool get_mSkewFrustum_12() const { return ___mSkewFrustum_12; }
	inline bool* get_address_of_mSkewFrustum_12() { return &___mSkewFrustum_12; }
	inline void set_mSkewFrustum_12(bool value)
	{
		___mSkewFrustum_12 = value;
	}

	inline static int32_t get_offset_of_mScreenWidth_13() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t2611724717, ___mScreenWidth_13)); }
	inline int32_t get_mScreenWidth_13() const { return ___mScreenWidth_13; }
	inline int32_t* get_address_of_mScreenWidth_13() { return &___mScreenWidth_13; }
	inline void set_mScreenWidth_13(int32_t value)
	{
		___mScreenWidth_13 = value;
	}

	inline static int32_t get_offset_of_mScreenHeight_14() { return static_cast<int32_t>(offsetof(BaseStereoViewerCameraConfiguration_t2611724717, ___mScreenHeight_14)); }
	inline int32_t get_mScreenHeight_14() const { return ___mScreenHeight_14; }
	inline int32_t* get_address_of_mScreenHeight_14() { return &___mScreenHeight_14; }
	inline void set_mScreenHeight_14(int32_t value)
	{
		___mScreenHeight_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESTEREOVIEWERCAMERACONFIGURATION_T2611724717_H
#ifndef EVENTHANDLER_T3436254912_H
#define EVENTHANDLER_T3436254912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/EventHandler
struct  EventHandler_t3436254912  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTHANDLER_T3436254912_H
#ifndef ERROREVENTHANDLER_T3211687919_H
#define ERROREVENTHANDLER_T3211687919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct  ErrorEventHandler_t3211687919  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTHANDLER_T3211687919_H
#ifndef VIDEOPLAYER_T1683042537_H
#define VIDEOPLAYER_T1683042537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoPlayer
struct  VideoPlayer_t1683042537  : public Behaviour_t1437897464
{
public:
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t3436254912 * ___prepareCompleted_2;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t3436254912 * ___loopPointReached_3;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t3436254912 * ___started_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t3436254912 * ___frameDropped_5;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_t3211687919 * ___errorReceived_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t3436254912 * ___seekCompleted_7;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_t445758600 * ___clockResyncOccurred_8;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t3848515759 * ___frameReady_9;

public:
	inline static int32_t get_offset_of_prepareCompleted_2() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___prepareCompleted_2)); }
	inline EventHandler_t3436254912 * get_prepareCompleted_2() const { return ___prepareCompleted_2; }
	inline EventHandler_t3436254912 ** get_address_of_prepareCompleted_2() { return &___prepareCompleted_2; }
	inline void set_prepareCompleted_2(EventHandler_t3436254912 * value)
	{
		___prepareCompleted_2 = value;
		Il2CppCodeGenWriteBarrier((&___prepareCompleted_2), value);
	}

	inline static int32_t get_offset_of_loopPointReached_3() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___loopPointReached_3)); }
	inline EventHandler_t3436254912 * get_loopPointReached_3() const { return ___loopPointReached_3; }
	inline EventHandler_t3436254912 ** get_address_of_loopPointReached_3() { return &___loopPointReached_3; }
	inline void set_loopPointReached_3(EventHandler_t3436254912 * value)
	{
		___loopPointReached_3 = value;
		Il2CppCodeGenWriteBarrier((&___loopPointReached_3), value);
	}

	inline static int32_t get_offset_of_started_4() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___started_4)); }
	inline EventHandler_t3436254912 * get_started_4() const { return ___started_4; }
	inline EventHandler_t3436254912 ** get_address_of_started_4() { return &___started_4; }
	inline void set_started_4(EventHandler_t3436254912 * value)
	{
		___started_4 = value;
		Il2CppCodeGenWriteBarrier((&___started_4), value);
	}

	inline static int32_t get_offset_of_frameDropped_5() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameDropped_5)); }
	inline EventHandler_t3436254912 * get_frameDropped_5() const { return ___frameDropped_5; }
	inline EventHandler_t3436254912 ** get_address_of_frameDropped_5() { return &___frameDropped_5; }
	inline void set_frameDropped_5(EventHandler_t3436254912 * value)
	{
		___frameDropped_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameDropped_5), value);
	}

	inline static int32_t get_offset_of_errorReceived_6() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___errorReceived_6)); }
	inline ErrorEventHandler_t3211687919 * get_errorReceived_6() const { return ___errorReceived_6; }
	inline ErrorEventHandler_t3211687919 ** get_address_of_errorReceived_6() { return &___errorReceived_6; }
	inline void set_errorReceived_6(ErrorEventHandler_t3211687919 * value)
	{
		___errorReceived_6 = value;
		Il2CppCodeGenWriteBarrier((&___errorReceived_6), value);
	}

	inline static int32_t get_offset_of_seekCompleted_7() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___seekCompleted_7)); }
	inline EventHandler_t3436254912 * get_seekCompleted_7() const { return ___seekCompleted_7; }
	inline EventHandler_t3436254912 ** get_address_of_seekCompleted_7() { return &___seekCompleted_7; }
	inline void set_seekCompleted_7(EventHandler_t3436254912 * value)
	{
		___seekCompleted_7 = value;
		Il2CppCodeGenWriteBarrier((&___seekCompleted_7), value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_8() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___clockResyncOccurred_8)); }
	inline TimeEventHandler_t445758600 * get_clockResyncOccurred_8() const { return ___clockResyncOccurred_8; }
	inline TimeEventHandler_t445758600 ** get_address_of_clockResyncOccurred_8() { return &___clockResyncOccurred_8; }
	inline void set_clockResyncOccurred_8(TimeEventHandler_t445758600 * value)
	{
		___clockResyncOccurred_8 = value;
		Il2CppCodeGenWriteBarrier((&___clockResyncOccurred_8), value);
	}

	inline static int32_t get_offset_of_frameReady_9() { return static_cast<int32_t>(offsetof(VideoPlayer_t1683042537, ___frameReady_9)); }
	inline FrameReadyEventHandler_t3848515759 * get_frameReady_9() const { return ___frameReady_9; }
	inline FrameReadyEventHandler_t3848515759 ** get_address_of_frameReady_9() { return &___frameReady_9; }
	inline void set_frameReady_9(FrameReadyEventHandler_t3848515759 * value)
	{
		___frameReady_9 = value;
		Il2CppCodeGenWriteBarrier((&___frameReady_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOPLAYER_T1683042537_H
#ifndef STEREOVIEWERCAMERACONFIGURATION_T4050045721_H
#define STEREOVIEWERCAMERACONFIGURATION_T4050045721_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.StereoViewerCameraConfiguration
struct  StereoViewerCameraConfiguration_t4050045721  : public BaseStereoViewerCameraConfiguration_t2611724717
{
public:
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedLeftNearClipPlane
	float ___mLastAppliedLeftNearClipPlane_15;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedLeftFarClipPlane
	float ___mLastAppliedLeftFarClipPlane_16;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedRightNearClipPlane
	float ___mLastAppliedRightNearClipPlane_17;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mLastAppliedRightFarClipPlane
	float ___mLastAppliedRightFarClipPlane_18;
	// System.Single Vuforia.StereoViewerCameraConfiguration::mCameraOffset
	float ___mCameraOffset_19;
	// System.Boolean Vuforia.StereoViewerCameraConfiguration::mIsDistorted
	bool ___mIsDistorted_20;

public:
	inline static int32_t get_offset_of_mLastAppliedLeftNearClipPlane_15() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4050045721, ___mLastAppliedLeftNearClipPlane_15)); }
	inline float get_mLastAppliedLeftNearClipPlane_15() const { return ___mLastAppliedLeftNearClipPlane_15; }
	inline float* get_address_of_mLastAppliedLeftNearClipPlane_15() { return &___mLastAppliedLeftNearClipPlane_15; }
	inline void set_mLastAppliedLeftNearClipPlane_15(float value)
	{
		___mLastAppliedLeftNearClipPlane_15 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftFarClipPlane_16() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4050045721, ___mLastAppliedLeftFarClipPlane_16)); }
	inline float get_mLastAppliedLeftFarClipPlane_16() const { return ___mLastAppliedLeftFarClipPlane_16; }
	inline float* get_address_of_mLastAppliedLeftFarClipPlane_16() { return &___mLastAppliedLeftFarClipPlane_16; }
	inline void set_mLastAppliedLeftFarClipPlane_16(float value)
	{
		___mLastAppliedLeftFarClipPlane_16 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightNearClipPlane_17() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4050045721, ___mLastAppliedRightNearClipPlane_17)); }
	inline float get_mLastAppliedRightNearClipPlane_17() const { return ___mLastAppliedRightNearClipPlane_17; }
	inline float* get_address_of_mLastAppliedRightNearClipPlane_17() { return &___mLastAppliedRightNearClipPlane_17; }
	inline void set_mLastAppliedRightNearClipPlane_17(float value)
	{
		___mLastAppliedRightNearClipPlane_17 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightFarClipPlane_18() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4050045721, ___mLastAppliedRightFarClipPlane_18)); }
	inline float get_mLastAppliedRightFarClipPlane_18() const { return ___mLastAppliedRightFarClipPlane_18; }
	inline float* get_address_of_mLastAppliedRightFarClipPlane_18() { return &___mLastAppliedRightFarClipPlane_18; }
	inline void set_mLastAppliedRightFarClipPlane_18(float value)
	{
		___mLastAppliedRightFarClipPlane_18 = value;
	}

	inline static int32_t get_offset_of_mCameraOffset_19() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4050045721, ___mCameraOffset_19)); }
	inline float get_mCameraOffset_19() const { return ___mCameraOffset_19; }
	inline float* get_address_of_mCameraOffset_19() { return &___mCameraOffset_19; }
	inline void set_mCameraOffset_19(float value)
	{
		___mCameraOffset_19 = value;
	}

	inline static int32_t get_offset_of_mIsDistorted_20() { return static_cast<int32_t>(offsetof(StereoViewerCameraConfiguration_t4050045721, ___mIsDistorted_20)); }
	inline bool get_mIsDistorted_20() const { return ___mIsDistorted_20; }
	inline bool* get_address_of_mIsDistorted_20() { return &___mIsDistorted_20; }
	inline void set_mIsDistorted_20(bool value)
	{
		___mIsDistorted_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEREOVIEWERCAMERACONFIGURATION_T4050045721_H
#ifndef EXTERNALSTEREOCAMERACONFIGURATION_T3614889463_H
#define EXTERNALSTEREOCAMERACONFIGURATION_T3614889463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ExternalStereoCameraConfiguration
struct  ExternalStereoCameraConfiguration_t3614889463  : public BaseStereoViewerCameraConfiguration_t2611724717
{
public:
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftNearClipPlane
	float ___mLastAppliedLeftNearClipPlane_15;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftFarClipPlane
	float ___mLastAppliedLeftFarClipPlane_16;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightNearClipPlane
	float ___mLastAppliedRightNearClipPlane_17;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightFarClipPlane
	float ___mLastAppliedRightFarClipPlane_18;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftVerticalVirtualFoV
	float ___mLastAppliedLeftVerticalVirtualFoV_19;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftHorizontalVirtualFoV
	float ___mLastAppliedLeftHorizontalVirtualFoV_20;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightVerticalVirtualFoV
	float ___mLastAppliedRightVerticalVirtualFoV_21;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightHorizontalVirtualFoV
	float ___mLastAppliedRightHorizontalVirtualFoV_22;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mLastAppliedLeftProjection
	Matrix4x4_t1817901843  ___mLastAppliedLeftProjection_23;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mLastAppliedRightProjection
	Matrix4x4_t1817901843  ___mLastAppliedRightProjection_24;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftNearClipPlane
	float ___mNewLeftNearClipPlane_25;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftFarClipPlane
	float ___mNewLeftFarClipPlane_26;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightNearClipPlane
	float ___mNewRightNearClipPlane_27;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightFarClipPlane
	float ___mNewRightFarClipPlane_28;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftVerticalVirtualFoV
	float ___mNewLeftVerticalVirtualFoV_29;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewLeftHorizontalVirtualFoV
	float ___mNewLeftHorizontalVirtualFoV_30;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightVerticalVirtualFoV
	float ___mNewRightVerticalVirtualFoV_31;
	// System.Single Vuforia.ExternalStereoCameraConfiguration::mNewRightHorizontalVirtualFoV
	float ___mNewRightHorizontalVirtualFoV_32;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mExternallySetLeftMatrix
	Matrix4x4_t1817901843  ___mExternallySetLeftMatrix_33;
	// UnityEngine.Matrix4x4 Vuforia.ExternalStereoCameraConfiguration::mExternallySetRightMatrix
	Matrix4x4_t1817901843  ___mExternallySetRightMatrix_34;

public:
	inline static int32_t get_offset_of_mLastAppliedLeftNearClipPlane_15() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedLeftNearClipPlane_15)); }
	inline float get_mLastAppliedLeftNearClipPlane_15() const { return ___mLastAppliedLeftNearClipPlane_15; }
	inline float* get_address_of_mLastAppliedLeftNearClipPlane_15() { return &___mLastAppliedLeftNearClipPlane_15; }
	inline void set_mLastAppliedLeftNearClipPlane_15(float value)
	{
		___mLastAppliedLeftNearClipPlane_15 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftFarClipPlane_16() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedLeftFarClipPlane_16)); }
	inline float get_mLastAppliedLeftFarClipPlane_16() const { return ___mLastAppliedLeftFarClipPlane_16; }
	inline float* get_address_of_mLastAppliedLeftFarClipPlane_16() { return &___mLastAppliedLeftFarClipPlane_16; }
	inline void set_mLastAppliedLeftFarClipPlane_16(float value)
	{
		___mLastAppliedLeftFarClipPlane_16 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightNearClipPlane_17() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedRightNearClipPlane_17)); }
	inline float get_mLastAppliedRightNearClipPlane_17() const { return ___mLastAppliedRightNearClipPlane_17; }
	inline float* get_address_of_mLastAppliedRightNearClipPlane_17() { return &___mLastAppliedRightNearClipPlane_17; }
	inline void set_mLastAppliedRightNearClipPlane_17(float value)
	{
		___mLastAppliedRightNearClipPlane_17 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightFarClipPlane_18() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedRightFarClipPlane_18)); }
	inline float get_mLastAppliedRightFarClipPlane_18() const { return ___mLastAppliedRightFarClipPlane_18; }
	inline float* get_address_of_mLastAppliedRightFarClipPlane_18() { return &___mLastAppliedRightFarClipPlane_18; }
	inline void set_mLastAppliedRightFarClipPlane_18(float value)
	{
		___mLastAppliedRightFarClipPlane_18 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftVerticalVirtualFoV_19() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedLeftVerticalVirtualFoV_19)); }
	inline float get_mLastAppliedLeftVerticalVirtualFoV_19() const { return ___mLastAppliedLeftVerticalVirtualFoV_19; }
	inline float* get_address_of_mLastAppliedLeftVerticalVirtualFoV_19() { return &___mLastAppliedLeftVerticalVirtualFoV_19; }
	inline void set_mLastAppliedLeftVerticalVirtualFoV_19(float value)
	{
		___mLastAppliedLeftVerticalVirtualFoV_19 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_20() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedLeftHorizontalVirtualFoV_20)); }
	inline float get_mLastAppliedLeftHorizontalVirtualFoV_20() const { return ___mLastAppliedLeftHorizontalVirtualFoV_20; }
	inline float* get_address_of_mLastAppliedLeftHorizontalVirtualFoV_20() { return &___mLastAppliedLeftHorizontalVirtualFoV_20; }
	inline void set_mLastAppliedLeftHorizontalVirtualFoV_20(float value)
	{
		___mLastAppliedLeftHorizontalVirtualFoV_20 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightVerticalVirtualFoV_21() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedRightVerticalVirtualFoV_21)); }
	inline float get_mLastAppliedRightVerticalVirtualFoV_21() const { return ___mLastAppliedRightVerticalVirtualFoV_21; }
	inline float* get_address_of_mLastAppliedRightVerticalVirtualFoV_21() { return &___mLastAppliedRightVerticalVirtualFoV_21; }
	inline void set_mLastAppliedRightVerticalVirtualFoV_21(float value)
	{
		___mLastAppliedRightVerticalVirtualFoV_21 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightHorizontalVirtualFoV_22() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedRightHorizontalVirtualFoV_22)); }
	inline float get_mLastAppliedRightHorizontalVirtualFoV_22() const { return ___mLastAppliedRightHorizontalVirtualFoV_22; }
	inline float* get_address_of_mLastAppliedRightHorizontalVirtualFoV_22() { return &___mLastAppliedRightHorizontalVirtualFoV_22; }
	inline void set_mLastAppliedRightHorizontalVirtualFoV_22(float value)
	{
		___mLastAppliedRightHorizontalVirtualFoV_22 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedLeftProjection_23() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedLeftProjection_23)); }
	inline Matrix4x4_t1817901843  get_mLastAppliedLeftProjection_23() const { return ___mLastAppliedLeftProjection_23; }
	inline Matrix4x4_t1817901843 * get_address_of_mLastAppliedLeftProjection_23() { return &___mLastAppliedLeftProjection_23; }
	inline void set_mLastAppliedLeftProjection_23(Matrix4x4_t1817901843  value)
	{
		___mLastAppliedLeftProjection_23 = value;
	}

	inline static int32_t get_offset_of_mLastAppliedRightProjection_24() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mLastAppliedRightProjection_24)); }
	inline Matrix4x4_t1817901843  get_mLastAppliedRightProjection_24() const { return ___mLastAppliedRightProjection_24; }
	inline Matrix4x4_t1817901843 * get_address_of_mLastAppliedRightProjection_24() { return &___mLastAppliedRightProjection_24; }
	inline void set_mLastAppliedRightProjection_24(Matrix4x4_t1817901843  value)
	{
		___mLastAppliedRightProjection_24 = value;
	}

	inline static int32_t get_offset_of_mNewLeftNearClipPlane_25() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewLeftNearClipPlane_25)); }
	inline float get_mNewLeftNearClipPlane_25() const { return ___mNewLeftNearClipPlane_25; }
	inline float* get_address_of_mNewLeftNearClipPlane_25() { return &___mNewLeftNearClipPlane_25; }
	inline void set_mNewLeftNearClipPlane_25(float value)
	{
		___mNewLeftNearClipPlane_25 = value;
	}

	inline static int32_t get_offset_of_mNewLeftFarClipPlane_26() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewLeftFarClipPlane_26)); }
	inline float get_mNewLeftFarClipPlane_26() const { return ___mNewLeftFarClipPlane_26; }
	inline float* get_address_of_mNewLeftFarClipPlane_26() { return &___mNewLeftFarClipPlane_26; }
	inline void set_mNewLeftFarClipPlane_26(float value)
	{
		___mNewLeftFarClipPlane_26 = value;
	}

	inline static int32_t get_offset_of_mNewRightNearClipPlane_27() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewRightNearClipPlane_27)); }
	inline float get_mNewRightNearClipPlane_27() const { return ___mNewRightNearClipPlane_27; }
	inline float* get_address_of_mNewRightNearClipPlane_27() { return &___mNewRightNearClipPlane_27; }
	inline void set_mNewRightNearClipPlane_27(float value)
	{
		___mNewRightNearClipPlane_27 = value;
	}

	inline static int32_t get_offset_of_mNewRightFarClipPlane_28() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewRightFarClipPlane_28)); }
	inline float get_mNewRightFarClipPlane_28() const { return ___mNewRightFarClipPlane_28; }
	inline float* get_address_of_mNewRightFarClipPlane_28() { return &___mNewRightFarClipPlane_28; }
	inline void set_mNewRightFarClipPlane_28(float value)
	{
		___mNewRightFarClipPlane_28 = value;
	}

	inline static int32_t get_offset_of_mNewLeftVerticalVirtualFoV_29() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewLeftVerticalVirtualFoV_29)); }
	inline float get_mNewLeftVerticalVirtualFoV_29() const { return ___mNewLeftVerticalVirtualFoV_29; }
	inline float* get_address_of_mNewLeftVerticalVirtualFoV_29() { return &___mNewLeftVerticalVirtualFoV_29; }
	inline void set_mNewLeftVerticalVirtualFoV_29(float value)
	{
		___mNewLeftVerticalVirtualFoV_29 = value;
	}

	inline static int32_t get_offset_of_mNewLeftHorizontalVirtualFoV_30() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewLeftHorizontalVirtualFoV_30)); }
	inline float get_mNewLeftHorizontalVirtualFoV_30() const { return ___mNewLeftHorizontalVirtualFoV_30; }
	inline float* get_address_of_mNewLeftHorizontalVirtualFoV_30() { return &___mNewLeftHorizontalVirtualFoV_30; }
	inline void set_mNewLeftHorizontalVirtualFoV_30(float value)
	{
		___mNewLeftHorizontalVirtualFoV_30 = value;
	}

	inline static int32_t get_offset_of_mNewRightVerticalVirtualFoV_31() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewRightVerticalVirtualFoV_31)); }
	inline float get_mNewRightVerticalVirtualFoV_31() const { return ___mNewRightVerticalVirtualFoV_31; }
	inline float* get_address_of_mNewRightVerticalVirtualFoV_31() { return &___mNewRightVerticalVirtualFoV_31; }
	inline void set_mNewRightVerticalVirtualFoV_31(float value)
	{
		___mNewRightVerticalVirtualFoV_31 = value;
	}

	inline static int32_t get_offset_of_mNewRightHorizontalVirtualFoV_32() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mNewRightHorizontalVirtualFoV_32)); }
	inline float get_mNewRightHorizontalVirtualFoV_32() const { return ___mNewRightHorizontalVirtualFoV_32; }
	inline float* get_address_of_mNewRightHorizontalVirtualFoV_32() { return &___mNewRightHorizontalVirtualFoV_32; }
	inline void set_mNewRightHorizontalVirtualFoV_32(float value)
	{
		___mNewRightHorizontalVirtualFoV_32 = value;
	}

	inline static int32_t get_offset_of_mExternallySetLeftMatrix_33() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mExternallySetLeftMatrix_33)); }
	inline Matrix4x4_t1817901843  get_mExternallySetLeftMatrix_33() const { return ___mExternallySetLeftMatrix_33; }
	inline Matrix4x4_t1817901843 * get_address_of_mExternallySetLeftMatrix_33() { return &___mExternallySetLeftMatrix_33; }
	inline void set_mExternallySetLeftMatrix_33(Matrix4x4_t1817901843  value)
	{
		___mExternallySetLeftMatrix_33 = value;
	}

	inline static int32_t get_offset_of_mExternallySetRightMatrix_34() { return static_cast<int32_t>(offsetof(ExternalStereoCameraConfiguration_t3614889463, ___mExternallySetRightMatrix_34)); }
	inline Matrix4x4_t1817901843  get_mExternallySetRightMatrix_34() const { return ___mExternallySetRightMatrix_34; }
	inline Matrix4x4_t1817901843 * get_address_of_mExternallySetRightMatrix_34() { return &___mExternallySetRightMatrix_34; }
	inline void set_mExternallySetRightMatrix_34(Matrix4x4_t1817901843  value)
	{
		___mExternallySetRightMatrix_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTERNALSTEREOCAMERACONFIGURATION_T3614889463_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef DISTORTIONRENDERINGBEHAVIOUR_T1858983148_H
#define DISTORTIONRENDERINGBEHAVIOUR_T1858983148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DistortionRenderingBehaviour
struct  DistortionRenderingBehaviour_t1858983148  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Vuforia.DistortionRenderingBehaviour::mSingleTexture
	bool ___mSingleTexture_2;
	// System.Int32 Vuforia.DistortionRenderingBehaviour::mRenderLayer
	int32_t ___mRenderLayer_3;
	// System.Int32[] Vuforia.DistortionRenderingBehaviour::mOriginalCullingMasks
	Int32U5BU5D_t385246372* ___mOriginalCullingMasks_4;
	// UnityEngine.Camera[] Vuforia.DistortionRenderingBehaviour::mStereoCameras
	CameraU5BU5D_t1709987734* ___mStereoCameras_5;
	// UnityEngine.GameObject[] Vuforia.DistortionRenderingBehaviour::mMeshes
	GameObjectU5BU5D_t3328599146* ___mMeshes_6;
	// UnityEngine.RenderTexture[] Vuforia.DistortionRenderingBehaviour::mTextures
	RenderTextureU5BU5D_t4111643188* ___mTextures_7;
	// System.Boolean Vuforia.DistortionRenderingBehaviour::mStarted
	bool ___mStarted_8;
	// System.Boolean Vuforia.DistortionRenderingBehaviour::mVideoBackgroundChanged
	bool ___mVideoBackgroundChanged_9;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mOriginalLeftViewport
	Rect_t2360479859  ___mOriginalLeftViewport_10;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mOriginalRightViewport
	Rect_t2360479859  ___mOriginalRightViewport_11;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mDualTextureLeftViewport
	Rect_t2360479859  ___mDualTextureLeftViewport_12;
	// UnityEngine.Rect Vuforia.DistortionRenderingBehaviour::mDualTextureRightViewport
	Rect_t2360479859  ___mDualTextureRightViewport_13;

public:
	inline static int32_t get_offset_of_mSingleTexture_2() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mSingleTexture_2)); }
	inline bool get_mSingleTexture_2() const { return ___mSingleTexture_2; }
	inline bool* get_address_of_mSingleTexture_2() { return &___mSingleTexture_2; }
	inline void set_mSingleTexture_2(bool value)
	{
		___mSingleTexture_2 = value;
	}

	inline static int32_t get_offset_of_mRenderLayer_3() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mRenderLayer_3)); }
	inline int32_t get_mRenderLayer_3() const { return ___mRenderLayer_3; }
	inline int32_t* get_address_of_mRenderLayer_3() { return &___mRenderLayer_3; }
	inline void set_mRenderLayer_3(int32_t value)
	{
		___mRenderLayer_3 = value;
	}

	inline static int32_t get_offset_of_mOriginalCullingMasks_4() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mOriginalCullingMasks_4)); }
	inline Int32U5BU5D_t385246372* get_mOriginalCullingMasks_4() const { return ___mOriginalCullingMasks_4; }
	inline Int32U5BU5D_t385246372** get_address_of_mOriginalCullingMasks_4() { return &___mOriginalCullingMasks_4; }
	inline void set_mOriginalCullingMasks_4(Int32U5BU5D_t385246372* value)
	{
		___mOriginalCullingMasks_4 = value;
		Il2CppCodeGenWriteBarrier((&___mOriginalCullingMasks_4), value);
	}

	inline static int32_t get_offset_of_mStereoCameras_5() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mStereoCameras_5)); }
	inline CameraU5BU5D_t1709987734* get_mStereoCameras_5() const { return ___mStereoCameras_5; }
	inline CameraU5BU5D_t1709987734** get_address_of_mStereoCameras_5() { return &___mStereoCameras_5; }
	inline void set_mStereoCameras_5(CameraU5BU5D_t1709987734* value)
	{
		___mStereoCameras_5 = value;
		Il2CppCodeGenWriteBarrier((&___mStereoCameras_5), value);
	}

	inline static int32_t get_offset_of_mMeshes_6() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mMeshes_6)); }
	inline GameObjectU5BU5D_t3328599146* get_mMeshes_6() const { return ___mMeshes_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_mMeshes_6() { return &___mMeshes_6; }
	inline void set_mMeshes_6(GameObjectU5BU5D_t3328599146* value)
	{
		___mMeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___mMeshes_6), value);
	}

	inline static int32_t get_offset_of_mTextures_7() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mTextures_7)); }
	inline RenderTextureU5BU5D_t4111643188* get_mTextures_7() const { return ___mTextures_7; }
	inline RenderTextureU5BU5D_t4111643188** get_address_of_mTextures_7() { return &___mTextures_7; }
	inline void set_mTextures_7(RenderTextureU5BU5D_t4111643188* value)
	{
		___mTextures_7 = value;
		Il2CppCodeGenWriteBarrier((&___mTextures_7), value);
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}

	inline static int32_t get_offset_of_mVideoBackgroundChanged_9() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mVideoBackgroundChanged_9)); }
	inline bool get_mVideoBackgroundChanged_9() const { return ___mVideoBackgroundChanged_9; }
	inline bool* get_address_of_mVideoBackgroundChanged_9() { return &___mVideoBackgroundChanged_9; }
	inline void set_mVideoBackgroundChanged_9(bool value)
	{
		___mVideoBackgroundChanged_9 = value;
	}

	inline static int32_t get_offset_of_mOriginalLeftViewport_10() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mOriginalLeftViewport_10)); }
	inline Rect_t2360479859  get_mOriginalLeftViewport_10() const { return ___mOriginalLeftViewport_10; }
	inline Rect_t2360479859 * get_address_of_mOriginalLeftViewport_10() { return &___mOriginalLeftViewport_10; }
	inline void set_mOriginalLeftViewport_10(Rect_t2360479859  value)
	{
		___mOriginalLeftViewport_10 = value;
	}

	inline static int32_t get_offset_of_mOriginalRightViewport_11() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mOriginalRightViewport_11)); }
	inline Rect_t2360479859  get_mOriginalRightViewport_11() const { return ___mOriginalRightViewport_11; }
	inline Rect_t2360479859 * get_address_of_mOriginalRightViewport_11() { return &___mOriginalRightViewport_11; }
	inline void set_mOriginalRightViewport_11(Rect_t2360479859  value)
	{
		___mOriginalRightViewport_11 = value;
	}

	inline static int32_t get_offset_of_mDualTextureLeftViewport_12() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mDualTextureLeftViewport_12)); }
	inline Rect_t2360479859  get_mDualTextureLeftViewport_12() const { return ___mDualTextureLeftViewport_12; }
	inline Rect_t2360479859 * get_address_of_mDualTextureLeftViewport_12() { return &___mDualTextureLeftViewport_12; }
	inline void set_mDualTextureLeftViewport_12(Rect_t2360479859  value)
	{
		___mDualTextureLeftViewport_12 = value;
	}

	inline static int32_t get_offset_of_mDualTextureRightViewport_13() { return static_cast<int32_t>(offsetof(DistortionRenderingBehaviour_t1858983148, ___mDualTextureRightViewport_13)); }
	inline Rect_t2360479859  get_mDualTextureRightViewport_13() const { return ___mDualTextureRightViewport_13; }
	inline Rect_t2360479859 * get_address_of_mDualTextureRightViewport_13() { return &___mDualTextureRightViewport_13; }
	inline void set_mDualTextureRightViewport_13(Rect_t2360479859  value)
	{
		___mDualTextureRightViewport_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISTORTIONRENDERINGBEHAVIOUR_T1858983148_H
#ifndef VUFORIAABSTRACTBEHAVIOUR_T3510878193_H
#define VUFORIAABSTRACTBEHAVIOUR_T3510878193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.VuforiaAbstractBehaviour
struct  VuforiaAbstractBehaviour_t3510878193  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.VuforiaARController/WorldCenterMode Vuforia.VuforiaAbstractBehaviour::mWorldCenterMode
	int32_t ___mWorldCenterMode_2;
	// Vuforia.TrackableBehaviour Vuforia.VuforiaAbstractBehaviour::mWorldCenter
	TrackableBehaviour_t1113559212 * ___mWorldCenter_3;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mCentralAnchorPoint
	Transform_t3600365921 * ___mCentralAnchorPoint_4;
	// UnityEngine.Transform Vuforia.VuforiaAbstractBehaviour::mParentAnchorPoint
	Transform_t3600365921 * ___mParentAnchorPoint_5;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mPrimaryCamera
	Camera_t4157153871 * ___mPrimaryCamera_6;
	// UnityEngine.Camera Vuforia.VuforiaAbstractBehaviour::mSecondaryCamera
	Camera_t4157153871 * ___mSecondaryCamera_7;
	// System.Boolean Vuforia.VuforiaAbstractBehaviour::mWereBindingFieldsExposed
	bool ___mWereBindingFieldsExposed_8;
	// System.Action Vuforia.VuforiaAbstractBehaviour::AwakeEvent
	Action_t1264377477 * ___AwakeEvent_11;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnEnableEvent
	Action_t1264377477 * ___OnEnableEvent_12;
	// System.Action Vuforia.VuforiaAbstractBehaviour::StartEvent
	Action_t1264377477 * ___StartEvent_13;
	// System.Action Vuforia.VuforiaAbstractBehaviour::UpdateEvent
	Action_t1264377477 * ___UpdateEvent_14;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnLevelWasLoadedEvent
	Action_t1264377477 * ___OnLevelWasLoadedEvent_15;
	// System.Action`1<System.Boolean> Vuforia.VuforiaAbstractBehaviour::OnApplicationPauseEvent
	Action_1_t269755560 * ___OnApplicationPauseEvent_16;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnDisableEvent
	Action_t1264377477 * ___OnDisableEvent_17;
	// System.Action Vuforia.VuforiaAbstractBehaviour::OnDestroyEvent
	Action_t1264377477 * ___OnDestroyEvent_18;

public:
	inline static int32_t get_offset_of_mWorldCenterMode_2() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mWorldCenterMode_2)); }
	inline int32_t get_mWorldCenterMode_2() const { return ___mWorldCenterMode_2; }
	inline int32_t* get_address_of_mWorldCenterMode_2() { return &___mWorldCenterMode_2; }
	inline void set_mWorldCenterMode_2(int32_t value)
	{
		___mWorldCenterMode_2 = value;
	}

	inline static int32_t get_offset_of_mWorldCenter_3() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mWorldCenter_3)); }
	inline TrackableBehaviour_t1113559212 * get_mWorldCenter_3() const { return ___mWorldCenter_3; }
	inline TrackableBehaviour_t1113559212 ** get_address_of_mWorldCenter_3() { return &___mWorldCenter_3; }
	inline void set_mWorldCenter_3(TrackableBehaviour_t1113559212 * value)
	{
		___mWorldCenter_3 = value;
		Il2CppCodeGenWriteBarrier((&___mWorldCenter_3), value);
	}

	inline static int32_t get_offset_of_mCentralAnchorPoint_4() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mCentralAnchorPoint_4)); }
	inline Transform_t3600365921 * get_mCentralAnchorPoint_4() const { return ___mCentralAnchorPoint_4; }
	inline Transform_t3600365921 ** get_address_of_mCentralAnchorPoint_4() { return &___mCentralAnchorPoint_4; }
	inline void set_mCentralAnchorPoint_4(Transform_t3600365921 * value)
	{
		___mCentralAnchorPoint_4 = value;
		Il2CppCodeGenWriteBarrier((&___mCentralAnchorPoint_4), value);
	}

	inline static int32_t get_offset_of_mParentAnchorPoint_5() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mParentAnchorPoint_5)); }
	inline Transform_t3600365921 * get_mParentAnchorPoint_5() const { return ___mParentAnchorPoint_5; }
	inline Transform_t3600365921 ** get_address_of_mParentAnchorPoint_5() { return &___mParentAnchorPoint_5; }
	inline void set_mParentAnchorPoint_5(Transform_t3600365921 * value)
	{
		___mParentAnchorPoint_5 = value;
		Il2CppCodeGenWriteBarrier((&___mParentAnchorPoint_5), value);
	}

	inline static int32_t get_offset_of_mPrimaryCamera_6() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mPrimaryCamera_6)); }
	inline Camera_t4157153871 * get_mPrimaryCamera_6() const { return ___mPrimaryCamera_6; }
	inline Camera_t4157153871 ** get_address_of_mPrimaryCamera_6() { return &___mPrimaryCamera_6; }
	inline void set_mPrimaryCamera_6(Camera_t4157153871 * value)
	{
		___mPrimaryCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mPrimaryCamera_6), value);
	}

	inline static int32_t get_offset_of_mSecondaryCamera_7() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mSecondaryCamera_7)); }
	inline Camera_t4157153871 * get_mSecondaryCamera_7() const { return ___mSecondaryCamera_7; }
	inline Camera_t4157153871 ** get_address_of_mSecondaryCamera_7() { return &___mSecondaryCamera_7; }
	inline void set_mSecondaryCamera_7(Camera_t4157153871 * value)
	{
		___mSecondaryCamera_7 = value;
		Il2CppCodeGenWriteBarrier((&___mSecondaryCamera_7), value);
	}

	inline static int32_t get_offset_of_mWereBindingFieldsExposed_8() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___mWereBindingFieldsExposed_8)); }
	inline bool get_mWereBindingFieldsExposed_8() const { return ___mWereBindingFieldsExposed_8; }
	inline bool* get_address_of_mWereBindingFieldsExposed_8() { return &___mWereBindingFieldsExposed_8; }
	inline void set_mWereBindingFieldsExposed_8(bool value)
	{
		___mWereBindingFieldsExposed_8 = value;
	}

	inline static int32_t get_offset_of_AwakeEvent_11() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___AwakeEvent_11)); }
	inline Action_t1264377477 * get_AwakeEvent_11() const { return ___AwakeEvent_11; }
	inline Action_t1264377477 ** get_address_of_AwakeEvent_11() { return &___AwakeEvent_11; }
	inline void set_AwakeEvent_11(Action_t1264377477 * value)
	{
		___AwakeEvent_11 = value;
		Il2CppCodeGenWriteBarrier((&___AwakeEvent_11), value);
	}

	inline static int32_t get_offset_of_OnEnableEvent_12() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___OnEnableEvent_12)); }
	inline Action_t1264377477 * get_OnEnableEvent_12() const { return ___OnEnableEvent_12; }
	inline Action_t1264377477 ** get_address_of_OnEnableEvent_12() { return &___OnEnableEvent_12; }
	inline void set_OnEnableEvent_12(Action_t1264377477 * value)
	{
		___OnEnableEvent_12 = value;
		Il2CppCodeGenWriteBarrier((&___OnEnableEvent_12), value);
	}

	inline static int32_t get_offset_of_StartEvent_13() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___StartEvent_13)); }
	inline Action_t1264377477 * get_StartEvent_13() const { return ___StartEvent_13; }
	inline Action_t1264377477 ** get_address_of_StartEvent_13() { return &___StartEvent_13; }
	inline void set_StartEvent_13(Action_t1264377477 * value)
	{
		___StartEvent_13 = value;
		Il2CppCodeGenWriteBarrier((&___StartEvent_13), value);
	}

	inline static int32_t get_offset_of_UpdateEvent_14() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___UpdateEvent_14)); }
	inline Action_t1264377477 * get_UpdateEvent_14() const { return ___UpdateEvent_14; }
	inline Action_t1264377477 ** get_address_of_UpdateEvent_14() { return &___UpdateEvent_14; }
	inline void set_UpdateEvent_14(Action_t1264377477 * value)
	{
		___UpdateEvent_14 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateEvent_14), value);
	}

	inline static int32_t get_offset_of_OnLevelWasLoadedEvent_15() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___OnLevelWasLoadedEvent_15)); }
	inline Action_t1264377477 * get_OnLevelWasLoadedEvent_15() const { return ___OnLevelWasLoadedEvent_15; }
	inline Action_t1264377477 ** get_address_of_OnLevelWasLoadedEvent_15() { return &___OnLevelWasLoadedEvent_15; }
	inline void set_OnLevelWasLoadedEvent_15(Action_t1264377477 * value)
	{
		___OnLevelWasLoadedEvent_15 = value;
		Il2CppCodeGenWriteBarrier((&___OnLevelWasLoadedEvent_15), value);
	}

	inline static int32_t get_offset_of_OnApplicationPauseEvent_16() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___OnApplicationPauseEvent_16)); }
	inline Action_1_t269755560 * get_OnApplicationPauseEvent_16() const { return ___OnApplicationPauseEvent_16; }
	inline Action_1_t269755560 ** get_address_of_OnApplicationPauseEvent_16() { return &___OnApplicationPauseEvent_16; }
	inline void set_OnApplicationPauseEvent_16(Action_1_t269755560 * value)
	{
		___OnApplicationPauseEvent_16 = value;
		Il2CppCodeGenWriteBarrier((&___OnApplicationPauseEvent_16), value);
	}

	inline static int32_t get_offset_of_OnDisableEvent_17() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___OnDisableEvent_17)); }
	inline Action_t1264377477 * get_OnDisableEvent_17() const { return ___OnDisableEvent_17; }
	inline Action_t1264377477 ** get_address_of_OnDisableEvent_17() { return &___OnDisableEvent_17; }
	inline void set_OnDisableEvent_17(Action_t1264377477 * value)
	{
		___OnDisableEvent_17 = value;
		Il2CppCodeGenWriteBarrier((&___OnDisableEvent_17), value);
	}

	inline static int32_t get_offset_of_OnDestroyEvent_18() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193, ___OnDestroyEvent_18)); }
	inline Action_t1264377477 * get_OnDestroyEvent_18() const { return ___OnDestroyEvent_18; }
	inline Action_t1264377477 ** get_address_of_OnDestroyEvent_18() { return &___OnDestroyEvent_18; }
	inline void set_OnDestroyEvent_18(Action_t1264377477 * value)
	{
		___OnDestroyEvent_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnDestroyEvent_18), value);
	}
};

struct VuforiaAbstractBehaviour_t3510878193_StaticFields
{
public:
	// System.Action`1<Vuforia.VuforiaAbstractBehaviour> Vuforia.VuforiaAbstractBehaviour::BehaviourCreated
	Action_1_t3683345788 * ___BehaviourCreated_9;
	// System.Action`1<Vuforia.VuforiaAbstractBehaviour> Vuforia.VuforiaAbstractBehaviour::BehaviourDestroyed
	Action_1_t3683345788 * ___BehaviourDestroyed_10;

public:
	inline static int32_t get_offset_of_BehaviourCreated_9() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193_StaticFields, ___BehaviourCreated_9)); }
	inline Action_1_t3683345788 * get_BehaviourCreated_9() const { return ___BehaviourCreated_9; }
	inline Action_1_t3683345788 ** get_address_of_BehaviourCreated_9() { return &___BehaviourCreated_9; }
	inline void set_BehaviourCreated_9(Action_1_t3683345788 * value)
	{
		___BehaviourCreated_9 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourCreated_9), value);
	}

	inline static int32_t get_offset_of_BehaviourDestroyed_10() { return static_cast<int32_t>(offsetof(VuforiaAbstractBehaviour_t3510878193_StaticFields, ___BehaviourDestroyed_10)); }
	inline Action_1_t3683345788 * get_BehaviourDestroyed_10() const { return ___BehaviourDestroyed_10; }
	inline Action_1_t3683345788 ** get_address_of_BehaviourDestroyed_10() { return &___BehaviourDestroyed_10; }
	inline void set_BehaviourDestroyed_10(Action_1_t3683345788 * value)
	{
		___BehaviourDestroyed_10 = value;
		Il2CppCodeGenWriteBarrier((&___BehaviourDestroyed_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VUFORIAABSTRACTBEHAVIOUR_T3510878193_H
#ifndef BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4147679365_H
#define BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4147679365_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.BackgroundPlaneAbstractBehaviour
struct  BackgroundPlaneAbstractBehaviour_t4147679365  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.VuforiaRenderer/VideoTextureInfo Vuforia.BackgroundPlaneAbstractBehaviour::mTextureInfo
	VideoTextureInfo_t1805965052  ___mTextureInfo_2;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewWidth
	int32_t ___mViewWidth_3;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mViewHeight
	int32_t ___mViewHeight_4;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumFramesToUpdateVideoBg
	int32_t ___mNumFramesToUpdateVideoBg_5;
	// UnityEngine.Camera Vuforia.BackgroundPlaneAbstractBehaviour::mCamera
	Camera_t4157153871 * ___mCamera_6;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::defaultNumDivisions
	int32_t ___defaultNumDivisions_8;
	// UnityEngine.Mesh Vuforia.BackgroundPlaneAbstractBehaviour::mMesh
	Mesh_t3648964284 * ___mMesh_9;
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::mStereoDepth
	float ___mStereoDepth_10;
	// UnityEngine.Vector3 Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundOffset
	Vector3_t3722313464  ___mBackgroundOffset_11;
	// Vuforia.VuforiaARController Vuforia.BackgroundPlaneAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1876945237 * ___mVuforiaBehaviour_12;
	// System.Action Vuforia.BackgroundPlaneAbstractBehaviour::mBackgroundPlacedCallback
	Action_t1264377477 * ___mBackgroundPlacedCallback_13;
	// System.Int32 Vuforia.BackgroundPlaneAbstractBehaviour::mNumDivisions
	int32_t ___mNumDivisions_14;

public:
	inline static int32_t get_offset_of_mTextureInfo_2() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mTextureInfo_2)); }
	inline VideoTextureInfo_t1805965052  get_mTextureInfo_2() const { return ___mTextureInfo_2; }
	inline VideoTextureInfo_t1805965052 * get_address_of_mTextureInfo_2() { return &___mTextureInfo_2; }
	inline void set_mTextureInfo_2(VideoTextureInfo_t1805965052  value)
	{
		___mTextureInfo_2 = value;
	}

	inline static int32_t get_offset_of_mViewWidth_3() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mViewWidth_3)); }
	inline int32_t get_mViewWidth_3() const { return ___mViewWidth_3; }
	inline int32_t* get_address_of_mViewWidth_3() { return &___mViewWidth_3; }
	inline void set_mViewWidth_3(int32_t value)
	{
		___mViewWidth_3 = value;
	}

	inline static int32_t get_offset_of_mViewHeight_4() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mViewHeight_4)); }
	inline int32_t get_mViewHeight_4() const { return ___mViewHeight_4; }
	inline int32_t* get_address_of_mViewHeight_4() { return &___mViewHeight_4; }
	inline void set_mViewHeight_4(int32_t value)
	{
		___mViewHeight_4 = value;
	}

	inline static int32_t get_offset_of_mNumFramesToUpdateVideoBg_5() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mNumFramesToUpdateVideoBg_5)); }
	inline int32_t get_mNumFramesToUpdateVideoBg_5() const { return ___mNumFramesToUpdateVideoBg_5; }
	inline int32_t* get_address_of_mNumFramesToUpdateVideoBg_5() { return &___mNumFramesToUpdateVideoBg_5; }
	inline void set_mNumFramesToUpdateVideoBg_5(int32_t value)
	{
		___mNumFramesToUpdateVideoBg_5 = value;
	}

	inline static int32_t get_offset_of_mCamera_6() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mCamera_6)); }
	inline Camera_t4157153871 * get_mCamera_6() const { return ___mCamera_6; }
	inline Camera_t4157153871 ** get_address_of_mCamera_6() { return &___mCamera_6; }
	inline void set_mCamera_6(Camera_t4157153871 * value)
	{
		___mCamera_6 = value;
		Il2CppCodeGenWriteBarrier((&___mCamera_6), value);
	}

	inline static int32_t get_offset_of_defaultNumDivisions_8() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___defaultNumDivisions_8)); }
	inline int32_t get_defaultNumDivisions_8() const { return ___defaultNumDivisions_8; }
	inline int32_t* get_address_of_defaultNumDivisions_8() { return &___defaultNumDivisions_8; }
	inline void set_defaultNumDivisions_8(int32_t value)
	{
		___defaultNumDivisions_8 = value;
	}

	inline static int32_t get_offset_of_mMesh_9() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mMesh_9)); }
	inline Mesh_t3648964284 * get_mMesh_9() const { return ___mMesh_9; }
	inline Mesh_t3648964284 ** get_address_of_mMesh_9() { return &___mMesh_9; }
	inline void set_mMesh_9(Mesh_t3648964284 * value)
	{
		___mMesh_9 = value;
		Il2CppCodeGenWriteBarrier((&___mMesh_9), value);
	}

	inline static int32_t get_offset_of_mStereoDepth_10() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mStereoDepth_10)); }
	inline float get_mStereoDepth_10() const { return ___mStereoDepth_10; }
	inline float* get_address_of_mStereoDepth_10() { return &___mStereoDepth_10; }
	inline void set_mStereoDepth_10(float value)
	{
		___mStereoDepth_10 = value;
	}

	inline static int32_t get_offset_of_mBackgroundOffset_11() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mBackgroundOffset_11)); }
	inline Vector3_t3722313464  get_mBackgroundOffset_11() const { return ___mBackgroundOffset_11; }
	inline Vector3_t3722313464 * get_address_of_mBackgroundOffset_11() { return &___mBackgroundOffset_11; }
	inline void set_mBackgroundOffset_11(Vector3_t3722313464  value)
	{
		___mBackgroundOffset_11 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_12() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mVuforiaBehaviour_12)); }
	inline VuforiaARController_t1876945237 * get_mVuforiaBehaviour_12() const { return ___mVuforiaBehaviour_12; }
	inline VuforiaARController_t1876945237 ** get_address_of_mVuforiaBehaviour_12() { return &___mVuforiaBehaviour_12; }
	inline void set_mVuforiaBehaviour_12(VuforiaARController_t1876945237 * value)
	{
		___mVuforiaBehaviour_12 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_12), value);
	}

	inline static int32_t get_offset_of_mBackgroundPlacedCallback_13() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mBackgroundPlacedCallback_13)); }
	inline Action_t1264377477 * get_mBackgroundPlacedCallback_13() const { return ___mBackgroundPlacedCallback_13; }
	inline Action_t1264377477 ** get_address_of_mBackgroundPlacedCallback_13() { return &___mBackgroundPlacedCallback_13; }
	inline void set_mBackgroundPlacedCallback_13(Action_t1264377477 * value)
	{
		___mBackgroundPlacedCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___mBackgroundPlacedCallback_13), value);
	}

	inline static int32_t get_offset_of_mNumDivisions_14() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365, ___mNumDivisions_14)); }
	inline int32_t get_mNumDivisions_14() const { return ___mNumDivisions_14; }
	inline int32_t* get_address_of_mNumDivisions_14() { return &___mNumDivisions_14; }
	inline void set_mNumDivisions_14(int32_t value)
	{
		___mNumDivisions_14 = value;
	}
};

struct BackgroundPlaneAbstractBehaviour_t4147679365_StaticFields
{
public:
	// System.Single Vuforia.BackgroundPlaneAbstractBehaviour::maxDisplacement
	float ___maxDisplacement_7;

public:
	inline static int32_t get_offset_of_maxDisplacement_7() { return static_cast<int32_t>(offsetof(BackgroundPlaneAbstractBehaviour_t4147679365_StaticFields, ___maxDisplacement_7)); }
	inline float get_maxDisplacement_7() const { return ___maxDisplacement_7; }
	inline float* get_address_of_maxDisplacement_7() { return &___maxDisplacement_7; }
	inline void set_maxDisplacement_7(float value)
	{
		___maxDisplacement_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BACKGROUNDPLANEABSTRACTBEHAVIOUR_T4147679365_H
#ifndef HIDEEXCESSAREAABSTRACTBEHAVIOUR_T3369390328_H
#define HIDEEXCESSAREAABSTRACTBEHAVIOUR_T3369390328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.HideExcessAreaAbstractBehaviour
struct  HideExcessAreaAbstractBehaviour_t3369390328  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.IExcessAreaClipping Vuforia.HideExcessAreaAbstractBehaviour::mClippingImpl
	RuntimeObject* ___mClippingImpl_2;
	// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.HideExcessAreaAbstractBehaviour::mClippingMode
	int32_t ___mClippingMode_3;
	// Vuforia.VuforiaARController Vuforia.HideExcessAreaAbstractBehaviour::mVuforiaBehaviour
	VuforiaARController_t1876945237 * ___mVuforiaBehaviour_4;
	// Vuforia.VideoBackgroundManager Vuforia.HideExcessAreaAbstractBehaviour::mVideoBgMgr
	VideoBackgroundManager_t2198727358 * ___mVideoBgMgr_5;
	// UnityEngine.Vector3 Vuforia.HideExcessAreaAbstractBehaviour::mPlaneOffset
	Vector3_t3722313464  ___mPlaneOffset_6;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mSceneScaledDown
	bool ___mSceneScaledDown_7;
	// System.Boolean Vuforia.HideExcessAreaAbstractBehaviour::mStarted
	bool ___mStarted_8;

public:
	inline static int32_t get_offset_of_mClippingImpl_2() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mClippingImpl_2)); }
	inline RuntimeObject* get_mClippingImpl_2() const { return ___mClippingImpl_2; }
	inline RuntimeObject** get_address_of_mClippingImpl_2() { return &___mClippingImpl_2; }
	inline void set_mClippingImpl_2(RuntimeObject* value)
	{
		___mClippingImpl_2 = value;
		Il2CppCodeGenWriteBarrier((&___mClippingImpl_2), value);
	}

	inline static int32_t get_offset_of_mClippingMode_3() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mClippingMode_3)); }
	inline int32_t get_mClippingMode_3() const { return ___mClippingMode_3; }
	inline int32_t* get_address_of_mClippingMode_3() { return &___mClippingMode_3; }
	inline void set_mClippingMode_3(int32_t value)
	{
		___mClippingMode_3 = value;
	}

	inline static int32_t get_offset_of_mVuforiaBehaviour_4() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mVuforiaBehaviour_4)); }
	inline VuforiaARController_t1876945237 * get_mVuforiaBehaviour_4() const { return ___mVuforiaBehaviour_4; }
	inline VuforiaARController_t1876945237 ** get_address_of_mVuforiaBehaviour_4() { return &___mVuforiaBehaviour_4; }
	inline void set_mVuforiaBehaviour_4(VuforiaARController_t1876945237 * value)
	{
		___mVuforiaBehaviour_4 = value;
		Il2CppCodeGenWriteBarrier((&___mVuforiaBehaviour_4), value);
	}

	inline static int32_t get_offset_of_mVideoBgMgr_5() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mVideoBgMgr_5)); }
	inline VideoBackgroundManager_t2198727358 * get_mVideoBgMgr_5() const { return ___mVideoBgMgr_5; }
	inline VideoBackgroundManager_t2198727358 ** get_address_of_mVideoBgMgr_5() { return &___mVideoBgMgr_5; }
	inline void set_mVideoBgMgr_5(VideoBackgroundManager_t2198727358 * value)
	{
		___mVideoBgMgr_5 = value;
		Il2CppCodeGenWriteBarrier((&___mVideoBgMgr_5), value);
	}

	inline static int32_t get_offset_of_mPlaneOffset_6() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mPlaneOffset_6)); }
	inline Vector3_t3722313464  get_mPlaneOffset_6() const { return ___mPlaneOffset_6; }
	inline Vector3_t3722313464 * get_address_of_mPlaneOffset_6() { return &___mPlaneOffset_6; }
	inline void set_mPlaneOffset_6(Vector3_t3722313464  value)
	{
		___mPlaneOffset_6 = value;
	}

	inline static int32_t get_offset_of_mSceneScaledDown_7() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mSceneScaledDown_7)); }
	inline bool get_mSceneScaledDown_7() const { return ___mSceneScaledDown_7; }
	inline bool* get_address_of_mSceneScaledDown_7() { return &___mSceneScaledDown_7; }
	inline void set_mSceneScaledDown_7(bool value)
	{
		___mSceneScaledDown_7 = value;
	}

	inline static int32_t get_offset_of_mStarted_8() { return static_cast<int32_t>(offsetof(HideExcessAreaAbstractBehaviour_t3369390328, ___mStarted_8)); }
	inline bool get_mStarted_8() const { return ___mStarted_8; }
	inline bool* get_address_of_mStarted_8() { return &___mStarted_8; }
	inline void set_mStarted_8(bool value)
	{
		___mStarted_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEEXCESSAREAABSTRACTBEHAVIOUR_T3369390328_H
#ifndef CLOUDRECOABSTRACTBEHAVIOUR_T3388674407_H
#define CLOUDRECOABSTRACTBEHAVIOUR_T3388674407_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.CloudRecoAbstractBehaviour
struct  CloudRecoAbstractBehaviour_t3388674407  : public MonoBehaviour_t3962482529
{
public:
	// Vuforia.ObjectTracker Vuforia.CloudRecoAbstractBehaviour::mObjectTracker
	ObjectTracker_t4177997237 * ___mObjectTracker_2;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCurrentlyInitializing
	bool ___mCurrentlyInitializing_3;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mInitSuccess
	bool ___mInitSuccess_4;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mCloudRecoStarted
	bool ___mCloudRecoStarted_5;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mOnInitializedCalled
	bool ___mOnInitializedCalled_6;
	// System.Collections.Generic.List`1<Vuforia.ICloudRecoEventHandler> Vuforia.CloudRecoAbstractBehaviour::mHandlers
	List_1_t1049732891 * ___mHandlers_7;
	// System.Boolean Vuforia.CloudRecoAbstractBehaviour::mTargetFinderStartedBeforeDisable
	bool ___mTargetFinderStartedBeforeDisable_8;
	// System.String Vuforia.CloudRecoAbstractBehaviour::AccessKey
	String_t* ___AccessKey_9;
	// System.String Vuforia.CloudRecoAbstractBehaviour::SecretKey
	String_t* ___SecretKey_10;

public:
	inline static int32_t get_offset_of_mObjectTracker_2() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mObjectTracker_2)); }
	inline ObjectTracker_t4177997237 * get_mObjectTracker_2() const { return ___mObjectTracker_2; }
	inline ObjectTracker_t4177997237 ** get_address_of_mObjectTracker_2() { return &___mObjectTracker_2; }
	inline void set_mObjectTracker_2(ObjectTracker_t4177997237 * value)
	{
		___mObjectTracker_2 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTracker_2), value);
	}

	inline static int32_t get_offset_of_mCurrentlyInitializing_3() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mCurrentlyInitializing_3)); }
	inline bool get_mCurrentlyInitializing_3() const { return ___mCurrentlyInitializing_3; }
	inline bool* get_address_of_mCurrentlyInitializing_3() { return &___mCurrentlyInitializing_3; }
	inline void set_mCurrentlyInitializing_3(bool value)
	{
		___mCurrentlyInitializing_3 = value;
	}

	inline static int32_t get_offset_of_mInitSuccess_4() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mInitSuccess_4)); }
	inline bool get_mInitSuccess_4() const { return ___mInitSuccess_4; }
	inline bool* get_address_of_mInitSuccess_4() { return &___mInitSuccess_4; }
	inline void set_mInitSuccess_4(bool value)
	{
		___mInitSuccess_4 = value;
	}

	inline static int32_t get_offset_of_mCloudRecoStarted_5() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mCloudRecoStarted_5)); }
	inline bool get_mCloudRecoStarted_5() const { return ___mCloudRecoStarted_5; }
	inline bool* get_address_of_mCloudRecoStarted_5() { return &___mCloudRecoStarted_5; }
	inline void set_mCloudRecoStarted_5(bool value)
	{
		___mCloudRecoStarted_5 = value;
	}

	inline static int32_t get_offset_of_mOnInitializedCalled_6() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mOnInitializedCalled_6)); }
	inline bool get_mOnInitializedCalled_6() const { return ___mOnInitializedCalled_6; }
	inline bool* get_address_of_mOnInitializedCalled_6() { return &___mOnInitializedCalled_6; }
	inline void set_mOnInitializedCalled_6(bool value)
	{
		___mOnInitializedCalled_6 = value;
	}

	inline static int32_t get_offset_of_mHandlers_7() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mHandlers_7)); }
	inline List_1_t1049732891 * get_mHandlers_7() const { return ___mHandlers_7; }
	inline List_1_t1049732891 ** get_address_of_mHandlers_7() { return &___mHandlers_7; }
	inline void set_mHandlers_7(List_1_t1049732891 * value)
	{
		___mHandlers_7 = value;
		Il2CppCodeGenWriteBarrier((&___mHandlers_7), value);
	}

	inline static int32_t get_offset_of_mTargetFinderStartedBeforeDisable_8() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___mTargetFinderStartedBeforeDisable_8)); }
	inline bool get_mTargetFinderStartedBeforeDisable_8() const { return ___mTargetFinderStartedBeforeDisable_8; }
	inline bool* get_address_of_mTargetFinderStartedBeforeDisable_8() { return &___mTargetFinderStartedBeforeDisable_8; }
	inline void set_mTargetFinderStartedBeforeDisable_8(bool value)
	{
		___mTargetFinderStartedBeforeDisable_8 = value;
	}

	inline static int32_t get_offset_of_AccessKey_9() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___AccessKey_9)); }
	inline String_t* get_AccessKey_9() const { return ___AccessKey_9; }
	inline String_t** get_address_of_AccessKey_9() { return &___AccessKey_9; }
	inline void set_AccessKey_9(String_t* value)
	{
		___AccessKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessKey_9), value);
	}

	inline static int32_t get_offset_of_SecretKey_10() { return static_cast<int32_t>(offsetof(CloudRecoAbstractBehaviour_t3388674407, ___SecretKey_10)); }
	inline String_t* get_SecretKey_10() const { return ___SecretKey_10; }
	inline String_t** get_address_of_SecretKey_10() { return &___SecretKey_10; }
	inline void set_SecretKey_10(String_t* value)
	{
		___SecretKey_10 = value;
		Il2CppCodeGenWriteBarrier((&___SecretKey_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLOUDRECOABSTRACTBEHAVIOUR_T3388674407_H
#ifndef TRACKABLEBEHAVIOUR_T1113559212_H
#define TRACKABLEBEHAVIOUR_T1113559212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.TrackableBehaviour
struct  TrackableBehaviour_t1113559212  : public MonoBehaviour_t3962482529
{
public:
	// System.Double Vuforia.TrackableBehaviour::<TimeStamp>k__BackingField
	double ___U3CTimeStampU3Ek__BackingField_2;
	// System.String Vuforia.TrackableBehaviour::mTrackableName
	String_t* ___mTrackableName_3;
	// System.Boolean Vuforia.TrackableBehaviour::mPreserveChildSize
	bool ___mPreserveChildSize_4;
	// System.Boolean Vuforia.TrackableBehaviour::mInitializedInEditor
	bool ___mInitializedInEditor_5;
	// UnityEngine.Vector3 Vuforia.TrackableBehaviour::mPreviousScale
	Vector3_t3722313464  ___mPreviousScale_6;
	// Vuforia.TrackableBehaviour/Status Vuforia.TrackableBehaviour::mStatus
	int32_t ___mStatus_7;
	// Vuforia.Trackable Vuforia.TrackableBehaviour::mTrackable
	RuntimeObject* ___mTrackable_8;
	// System.Collections.Generic.List`1<Vuforia.ITrackableEventHandler> Vuforia.TrackableBehaviour::mTrackableEventHandlers
	List_1_t2968050330 * ___mTrackableEventHandlers_9;

public:
	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___U3CTimeStampU3Ek__BackingField_2)); }
	inline double get_U3CTimeStampU3Ek__BackingField_2() const { return ___U3CTimeStampU3Ek__BackingField_2; }
	inline double* get_address_of_U3CTimeStampU3Ek__BackingField_2() { return &___U3CTimeStampU3Ek__BackingField_2; }
	inline void set_U3CTimeStampU3Ek__BackingField_2(double value)
	{
		___U3CTimeStampU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_mTrackableName_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableName_3)); }
	inline String_t* get_mTrackableName_3() const { return ___mTrackableName_3; }
	inline String_t** get_address_of_mTrackableName_3() { return &___mTrackableName_3; }
	inline void set_mTrackableName_3(String_t* value)
	{
		___mTrackableName_3 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableName_3), value);
	}

	inline static int32_t get_offset_of_mPreserveChildSize_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreserveChildSize_4)); }
	inline bool get_mPreserveChildSize_4() const { return ___mPreserveChildSize_4; }
	inline bool* get_address_of_mPreserveChildSize_4() { return &___mPreserveChildSize_4; }
	inline void set_mPreserveChildSize_4(bool value)
	{
		___mPreserveChildSize_4 = value;
	}

	inline static int32_t get_offset_of_mInitializedInEditor_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mInitializedInEditor_5)); }
	inline bool get_mInitializedInEditor_5() const { return ___mInitializedInEditor_5; }
	inline bool* get_address_of_mInitializedInEditor_5() { return &___mInitializedInEditor_5; }
	inline void set_mInitializedInEditor_5(bool value)
	{
		___mInitializedInEditor_5 = value;
	}

	inline static int32_t get_offset_of_mPreviousScale_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mPreviousScale_6)); }
	inline Vector3_t3722313464  get_mPreviousScale_6() const { return ___mPreviousScale_6; }
	inline Vector3_t3722313464 * get_address_of_mPreviousScale_6() { return &___mPreviousScale_6; }
	inline void set_mPreviousScale_6(Vector3_t3722313464  value)
	{
		___mPreviousScale_6 = value;
	}

	inline static int32_t get_offset_of_mStatus_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mStatus_7)); }
	inline int32_t get_mStatus_7() const { return ___mStatus_7; }
	inline int32_t* get_address_of_mStatus_7() { return &___mStatus_7; }
	inline void set_mStatus_7(int32_t value)
	{
		___mStatus_7 = value;
	}

	inline static int32_t get_offset_of_mTrackable_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackable_8)); }
	inline RuntimeObject* get_mTrackable_8() const { return ___mTrackable_8; }
	inline RuntimeObject** get_address_of_mTrackable_8() { return &___mTrackable_8; }
	inline void set_mTrackable_8(RuntimeObject* value)
	{
		___mTrackable_8 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackable_8), value);
	}

	inline static int32_t get_offset_of_mTrackableEventHandlers_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t1113559212, ___mTrackableEventHandlers_9)); }
	inline List_1_t2968050330 * get_mTrackableEventHandlers_9() const { return ___mTrackableEventHandlers_9; }
	inline List_1_t2968050330 ** get_address_of_mTrackableEventHandlers_9() { return &___mTrackableEventHandlers_9; }
	inline void set_mTrackableEventHandlers_9(List_1_t2968050330 * value)
	{
		___mTrackableEventHandlers_9 = value;
		Il2CppCodeGenWriteBarrier((&___mTrackableEventHandlers_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEBEHAVIOUR_T1113559212_H
#ifndef DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#define DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.DataSetTrackableBehaviour
struct  DataSetTrackableBehaviour_t3430730379  : public TrackableBehaviour_t1113559212
{
public:
	// System.String Vuforia.DataSetTrackableBehaviour::mDataSetPath
	String_t* ___mDataSetPath_10;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mExtendedTracking
	bool ___mExtendedTracking_11;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mInitializeSmartTerrain
	bool ___mInitializeSmartTerrain_12;
	// Vuforia.ReconstructionFromTargetAbstractBehaviour Vuforia.DataSetTrackableBehaviour::mReconstructionToInitialize
	ReconstructionFromTargetAbstractBehaviour_t2785373562 * ___mReconstructionToInitialize_13;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMin
	Vector3_t3722313464  ___mSmartTerrainOccluderBoundsMin_14;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderBoundsMax
	Vector3_t3722313464  ___mSmartTerrainOccluderBoundsMax_15;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mIsSmartTerrainOccluderOffset
	bool ___mIsSmartTerrainOccluderOffset_16;
	// UnityEngine.Vector3 Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderOffset
	Vector3_t3722313464  ___mSmartTerrainOccluderOffset_17;
	// UnityEngine.Quaternion Vuforia.DataSetTrackableBehaviour::mSmartTerrainOccluderRotation
	Quaternion_t2301928331  ___mSmartTerrainOccluderRotation_18;
	// System.Boolean Vuforia.DataSetTrackableBehaviour::mAutoSetOccluderFromTargetSize
	bool ___mAutoSetOccluderFromTargetSize_19;

public:
	inline static int32_t get_offset_of_mDataSetPath_10() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mDataSetPath_10)); }
	inline String_t* get_mDataSetPath_10() const { return ___mDataSetPath_10; }
	inline String_t** get_address_of_mDataSetPath_10() { return &___mDataSetPath_10; }
	inline void set_mDataSetPath_10(String_t* value)
	{
		___mDataSetPath_10 = value;
		Il2CppCodeGenWriteBarrier((&___mDataSetPath_10), value);
	}

	inline static int32_t get_offset_of_mExtendedTracking_11() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mExtendedTracking_11)); }
	inline bool get_mExtendedTracking_11() const { return ___mExtendedTracking_11; }
	inline bool* get_address_of_mExtendedTracking_11() { return &___mExtendedTracking_11; }
	inline void set_mExtendedTracking_11(bool value)
	{
		___mExtendedTracking_11 = value;
	}

	inline static int32_t get_offset_of_mInitializeSmartTerrain_12() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mInitializeSmartTerrain_12)); }
	inline bool get_mInitializeSmartTerrain_12() const { return ___mInitializeSmartTerrain_12; }
	inline bool* get_address_of_mInitializeSmartTerrain_12() { return &___mInitializeSmartTerrain_12; }
	inline void set_mInitializeSmartTerrain_12(bool value)
	{
		___mInitializeSmartTerrain_12 = value;
	}

	inline static int32_t get_offset_of_mReconstructionToInitialize_13() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mReconstructionToInitialize_13)); }
	inline ReconstructionFromTargetAbstractBehaviour_t2785373562 * get_mReconstructionToInitialize_13() const { return ___mReconstructionToInitialize_13; }
	inline ReconstructionFromTargetAbstractBehaviour_t2785373562 ** get_address_of_mReconstructionToInitialize_13() { return &___mReconstructionToInitialize_13; }
	inline void set_mReconstructionToInitialize_13(ReconstructionFromTargetAbstractBehaviour_t2785373562 * value)
	{
		___mReconstructionToInitialize_13 = value;
		Il2CppCodeGenWriteBarrier((&___mReconstructionToInitialize_13), value);
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMin_14() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mSmartTerrainOccluderBoundsMin_14)); }
	inline Vector3_t3722313464  get_mSmartTerrainOccluderBoundsMin_14() const { return ___mSmartTerrainOccluderBoundsMin_14; }
	inline Vector3_t3722313464 * get_address_of_mSmartTerrainOccluderBoundsMin_14() { return &___mSmartTerrainOccluderBoundsMin_14; }
	inline void set_mSmartTerrainOccluderBoundsMin_14(Vector3_t3722313464  value)
	{
		___mSmartTerrainOccluderBoundsMin_14 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderBoundsMax_15() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mSmartTerrainOccluderBoundsMax_15)); }
	inline Vector3_t3722313464  get_mSmartTerrainOccluderBoundsMax_15() const { return ___mSmartTerrainOccluderBoundsMax_15; }
	inline Vector3_t3722313464 * get_address_of_mSmartTerrainOccluderBoundsMax_15() { return &___mSmartTerrainOccluderBoundsMax_15; }
	inline void set_mSmartTerrainOccluderBoundsMax_15(Vector3_t3722313464  value)
	{
		___mSmartTerrainOccluderBoundsMax_15 = value;
	}

	inline static int32_t get_offset_of_mIsSmartTerrainOccluderOffset_16() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mIsSmartTerrainOccluderOffset_16)); }
	inline bool get_mIsSmartTerrainOccluderOffset_16() const { return ___mIsSmartTerrainOccluderOffset_16; }
	inline bool* get_address_of_mIsSmartTerrainOccluderOffset_16() { return &___mIsSmartTerrainOccluderOffset_16; }
	inline void set_mIsSmartTerrainOccluderOffset_16(bool value)
	{
		___mIsSmartTerrainOccluderOffset_16 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderOffset_17() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mSmartTerrainOccluderOffset_17)); }
	inline Vector3_t3722313464  get_mSmartTerrainOccluderOffset_17() const { return ___mSmartTerrainOccluderOffset_17; }
	inline Vector3_t3722313464 * get_address_of_mSmartTerrainOccluderOffset_17() { return &___mSmartTerrainOccluderOffset_17; }
	inline void set_mSmartTerrainOccluderOffset_17(Vector3_t3722313464  value)
	{
		___mSmartTerrainOccluderOffset_17 = value;
	}

	inline static int32_t get_offset_of_mSmartTerrainOccluderRotation_18() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mSmartTerrainOccluderRotation_18)); }
	inline Quaternion_t2301928331  get_mSmartTerrainOccluderRotation_18() const { return ___mSmartTerrainOccluderRotation_18; }
	inline Quaternion_t2301928331 * get_address_of_mSmartTerrainOccluderRotation_18() { return &___mSmartTerrainOccluderRotation_18; }
	inline void set_mSmartTerrainOccluderRotation_18(Quaternion_t2301928331  value)
	{
		___mSmartTerrainOccluderRotation_18 = value;
	}

	inline static int32_t get_offset_of_mAutoSetOccluderFromTargetSize_19() { return static_cast<int32_t>(offsetof(DataSetTrackableBehaviour_t3430730379, ___mAutoSetOccluderFromTargetSize_19)); }
	inline bool get_mAutoSetOccluderFromTargetSize_19() const { return ___mAutoSetOccluderFromTargetSize_19; }
	inline bool* get_address_of_mAutoSetOccluderFromTargetSize_19() { return &___mAutoSetOccluderFromTargetSize_19; }
	inline void set_mAutoSetOccluderFromTargetSize_19(bool value)
	{
		___mAutoSetOccluderFromTargetSize_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETTRACKABLEBEHAVIOUR_T3430730379_H
#ifndef OBJECTTARGETABSTRACTBEHAVIOUR_T2569206187_H
#define OBJECTTARGETABSTRACTBEHAVIOUR_T2569206187_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Vuforia.ObjectTargetAbstractBehaviour
struct  ObjectTargetAbstractBehaviour_t2569206187  : public DataSetTrackableBehaviour_t3430730379
{
public:
	// Vuforia.ObjectTarget Vuforia.ObjectTargetAbstractBehaviour::mObjectTarget
	RuntimeObject* ___mObjectTarget_20;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXY
	float ___mAspectRatioXY_21;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mAspectRatioXZ
	float ___mAspectRatioXZ_22;
	// System.Boolean Vuforia.ObjectTargetAbstractBehaviour::mShowBoundingBox
	bool ___mShowBoundingBox_23;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMin
	Vector3_t3722313464  ___mBBoxMin_24;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mBBoxMax
	Vector3_t3722313464  ___mBBoxMax_25;
	// UnityEngine.Texture2D Vuforia.ObjectTargetAbstractBehaviour::mPreviewImage
	Texture2D_t3840446185 * ___mPreviewImage_26;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLength
	float ___mLength_27;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mWidth
	float ___mWidth_28;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mHeight
	float ___mHeight_29;
	// System.Single Vuforia.ObjectTargetAbstractBehaviour::mLastTransformScale
	float ___mLastTransformScale_30;
	// UnityEngine.Vector3 Vuforia.ObjectTargetAbstractBehaviour::mLastSize
	Vector3_t3722313464  ___mLastSize_31;

public:
	inline static int32_t get_offset_of_mObjectTarget_20() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mObjectTarget_20)); }
	inline RuntimeObject* get_mObjectTarget_20() const { return ___mObjectTarget_20; }
	inline RuntimeObject** get_address_of_mObjectTarget_20() { return &___mObjectTarget_20; }
	inline void set_mObjectTarget_20(RuntimeObject* value)
	{
		___mObjectTarget_20 = value;
		Il2CppCodeGenWriteBarrier((&___mObjectTarget_20), value);
	}

	inline static int32_t get_offset_of_mAspectRatioXY_21() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mAspectRatioXY_21)); }
	inline float get_mAspectRatioXY_21() const { return ___mAspectRatioXY_21; }
	inline float* get_address_of_mAspectRatioXY_21() { return &___mAspectRatioXY_21; }
	inline void set_mAspectRatioXY_21(float value)
	{
		___mAspectRatioXY_21 = value;
	}

	inline static int32_t get_offset_of_mAspectRatioXZ_22() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mAspectRatioXZ_22)); }
	inline float get_mAspectRatioXZ_22() const { return ___mAspectRatioXZ_22; }
	inline float* get_address_of_mAspectRatioXZ_22() { return &___mAspectRatioXZ_22; }
	inline void set_mAspectRatioXZ_22(float value)
	{
		___mAspectRatioXZ_22 = value;
	}

	inline static int32_t get_offset_of_mShowBoundingBox_23() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mShowBoundingBox_23)); }
	inline bool get_mShowBoundingBox_23() const { return ___mShowBoundingBox_23; }
	inline bool* get_address_of_mShowBoundingBox_23() { return &___mShowBoundingBox_23; }
	inline void set_mShowBoundingBox_23(bool value)
	{
		___mShowBoundingBox_23 = value;
	}

	inline static int32_t get_offset_of_mBBoxMin_24() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mBBoxMin_24)); }
	inline Vector3_t3722313464  get_mBBoxMin_24() const { return ___mBBoxMin_24; }
	inline Vector3_t3722313464 * get_address_of_mBBoxMin_24() { return &___mBBoxMin_24; }
	inline void set_mBBoxMin_24(Vector3_t3722313464  value)
	{
		___mBBoxMin_24 = value;
	}

	inline static int32_t get_offset_of_mBBoxMax_25() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mBBoxMax_25)); }
	inline Vector3_t3722313464  get_mBBoxMax_25() const { return ___mBBoxMax_25; }
	inline Vector3_t3722313464 * get_address_of_mBBoxMax_25() { return &___mBBoxMax_25; }
	inline void set_mBBoxMax_25(Vector3_t3722313464  value)
	{
		___mBBoxMax_25 = value;
	}

	inline static int32_t get_offset_of_mPreviewImage_26() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mPreviewImage_26)); }
	inline Texture2D_t3840446185 * get_mPreviewImage_26() const { return ___mPreviewImage_26; }
	inline Texture2D_t3840446185 ** get_address_of_mPreviewImage_26() { return &___mPreviewImage_26; }
	inline void set_mPreviewImage_26(Texture2D_t3840446185 * value)
	{
		___mPreviewImage_26 = value;
		Il2CppCodeGenWriteBarrier((&___mPreviewImage_26), value);
	}

	inline static int32_t get_offset_of_mLength_27() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mLength_27)); }
	inline float get_mLength_27() const { return ___mLength_27; }
	inline float* get_address_of_mLength_27() { return &___mLength_27; }
	inline void set_mLength_27(float value)
	{
		___mLength_27 = value;
	}

	inline static int32_t get_offset_of_mWidth_28() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mWidth_28)); }
	inline float get_mWidth_28() const { return ___mWidth_28; }
	inline float* get_address_of_mWidth_28() { return &___mWidth_28; }
	inline void set_mWidth_28(float value)
	{
		___mWidth_28 = value;
	}

	inline static int32_t get_offset_of_mHeight_29() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mHeight_29)); }
	inline float get_mHeight_29() const { return ___mHeight_29; }
	inline float* get_address_of_mHeight_29() { return &___mHeight_29; }
	inline void set_mHeight_29(float value)
	{
		___mHeight_29 = value;
	}

	inline static int32_t get_offset_of_mLastTransformScale_30() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mLastTransformScale_30)); }
	inline float get_mLastTransformScale_30() const { return ___mLastTransformScale_30; }
	inline float* get_address_of_mLastTransformScale_30() { return &___mLastTransformScale_30; }
	inline void set_mLastTransformScale_30(float value)
	{
		___mLastTransformScale_30 = value;
	}

	inline static int32_t get_offset_of_mLastSize_31() { return static_cast<int32_t>(offsetof(ObjectTargetAbstractBehaviour_t2569206187, ___mLastSize_31)); }
	inline Vector3_t3722313464  get_mLastSize_31() const { return ___mLastSize_31; }
	inline Vector3_t3722313464 * get_address_of_mLastSize_31() { return &___mLastSize_31; }
	inline void set_mLastSize_31(Vector3_t3722313464  value)
	{
		___mLastSize_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTTARGETABSTRACTBEHAVIOUR_T2569206187_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1700[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (AnalyticsSessionState_t681173134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1702[5] = 
{
	AnalyticsSessionState_t681173134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (AnalyticsSessionInfo_t2322308579), -1, sizeof(AnalyticsSessionInfo_t2322308579_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1703[1] = 
{
	AnalyticsSessionInfo_t2322308579_StaticFields::get_offset_of_sessionStateChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (SessionStateChanged_t3163629820), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (U3CModuleU3E_t692745541), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (WebRequestUtils_t3541624225), -1, sizeof(WebRequestUtils_t3541624225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1706[1] = 
{
	WebRequestUtils_t3541624225_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (U3CModuleU3E_t692745542), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (VideoPlayer_t1683042537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[8] = 
{
	VideoPlayer_t1683042537::get_offset_of_prepareCompleted_2(),
	VideoPlayer_t1683042537::get_offset_of_loopPointReached_3(),
	VideoPlayer_t1683042537::get_offset_of_started_4(),
	VideoPlayer_t1683042537::get_offset_of_frameDropped_5(),
	VideoPlayer_t1683042537::get_offset_of_errorReceived_6(),
	VideoPlayer_t1683042537::get_offset_of_seekCompleted_7(),
	VideoPlayer_t1683042537::get_offset_of_clockResyncOccurred_8(),
	VideoPlayer_t1683042537::get_offset_of_frameReady_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (EventHandler_t3436254912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (ErrorEventHandler_t3211687919), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (FrameReadyEventHandler_t3848515759), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (TimeEventHandler_t445758600), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (VideoClipPlayable_t2598186649)+ sizeof (RuntimeObject), sizeof(VideoClipPlayable_t2598186649 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1713[1] = 
{
	VideoClipPlayable_t2598186649::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (U3CModuleU3E_t692745543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (U3CModuleU3E_t692745544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (ARController_t116632334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1716[1] = 
{
	ARController_t116632334::get_offset_of_mVuforiaBehaviour_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (U3CU3Ec__DisplayClass11_0_t2669575632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[1] = 
{
	U3CU3Ec__DisplayClass11_0_t2669575632::get_offset_of_controller_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (DigitalEyewearARController_t1054226036), -1, sizeof(DigitalEyewearARController_t1054226036_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1718[28] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DigitalEyewearARController_t1054226036::get_offset_of_mCameraOffset_7(),
	DigitalEyewearARController_t1054226036::get_offset_of_mDistortionRenderingMode_8(),
	DigitalEyewearARController_t1054226036::get_offset_of_mDistortionRenderingLayer_9(),
	DigitalEyewearARController_t1054226036::get_offset_of_mEyewearType_10(),
	DigitalEyewearARController_t1054226036::get_offset_of_mStereoFramework_11(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSeeThroughConfiguration_12(),
	DigitalEyewearARController_t1054226036::get_offset_of_mViewerName_13(),
	DigitalEyewearARController_t1054226036::get_offset_of_mViewerManufacturer_14(),
	DigitalEyewearARController_t1054226036::get_offset_of_mUseCustomViewer_15(),
	DigitalEyewearARController_t1054226036::get_offset_of_mCustomViewer_16(),
	DigitalEyewearARController_t1054226036::get_offset_of_mCentralAnchorPoint_17(),
	DigitalEyewearARController_t1054226036::get_offset_of_mParentAnchorPoint_18(),
	DigitalEyewearARController_t1054226036::get_offset_of_mPrimaryCamera_19(),
	DigitalEyewearARController_t1054226036::get_offset_of_mPrimaryCameraOriginalRect_20(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSecondaryCamera_21(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSecondaryCameraOriginalRect_22(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSecondaryCameraDisabledLocally_23(),
	DigitalEyewearARController_t1054226036::get_offset_of_mVuforiaBehaviour_24(),
	DigitalEyewearARController_t1054226036::get_offset_of_mDistortionRenderingBhvr_25(),
	DigitalEyewearARController_t1054226036::get_offset_of_mSetFocusPlaneAutomatically_26(),
	DigitalEyewearARController_t1054226036_StaticFields::get_offset_of_mInstance_27(),
	DigitalEyewearARController_t1054226036_StaticFields::get_offset_of_mPadlock_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (EyewearType_t2277580470)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[4] = 
{
	EyewearType_t2277580470::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (StereoFramework_t3144873991)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[4] = 
{
	StereoFramework_t3144873991::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (SeeThroughConfiguration_t568665021)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1721[3] = 
{
	SeeThroughConfiguration_t568665021::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (SerializableViewerParameters_t2043332680), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[11] = 
{
	SerializableViewerParameters_t2043332680::get_offset_of_Version_0(),
	SerializableViewerParameters_t2043332680::get_offset_of_Name_1(),
	SerializableViewerParameters_t2043332680::get_offset_of_Manufacturer_2(),
	SerializableViewerParameters_t2043332680::get_offset_of_ButtonType_3(),
	SerializableViewerParameters_t2043332680::get_offset_of_ScreenToLensDistance_4(),
	SerializableViewerParameters_t2043332680::get_offset_of_InterLensDistance_5(),
	SerializableViewerParameters_t2043332680::get_offset_of_TrayAlignment_6(),
	SerializableViewerParameters_t2043332680::get_offset_of_LensCenterToTrayDistance_7(),
	SerializableViewerParameters_t2043332680::get_offset_of_DistortionCoefficients_8(),
	SerializableViewerParameters_t2043332680::get_offset_of_FieldOfView_9(),
	SerializableViewerParameters_t2043332680::get_offset_of_ContainsMagnet_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (EyewearDevice_t3223385723), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (NullHoloLensApiAbstraction_t2968904009), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (DeviceTracker_t2315692373), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (DeviceTrackerARController_t1095592542), -1, sizeof(DeviceTrackerARController_t1095592542_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1728[13] = 
{
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_DEFAULT_HEAD_PIVOT_1(),
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_DEFAULT_HANDHELD_PIVOT_2(),
	DeviceTrackerARController_t1095592542::get_offset_of_mAutoInitTracker_3(),
	DeviceTrackerARController_t1095592542::get_offset_of_mAutoStartTracker_4(),
	DeviceTrackerARController_t1095592542::get_offset_of_mPosePrediction_5(),
	DeviceTrackerARController_t1095592542::get_offset_of_mModelCorrectionMode_6(),
	DeviceTrackerARController_t1095592542::get_offset_of_mModelTransformEnabled_7(),
	DeviceTrackerARController_t1095592542::get_offset_of_mModelTransform_8(),
	DeviceTrackerARController_t1095592542::get_offset_of_mTrackerStarted_9(),
	DeviceTrackerARController_t1095592542::get_offset_of_mTrackerWasActiveBeforePause_10(),
	DeviceTrackerARController_t1095592542::get_offset_of_mTrackerWasActiveBeforeDisabling_11(),
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_mInstance_12(),
	DeviceTrackerARController_t1095592542_StaticFields::get_offset_of_mPadlock_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (DistortionRenderingMode_t2592134457)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[4] = 
{
	DistortionRenderingMode_t2592134457::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (DistortionRenderingBehaviour_t1858983148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[12] = 
{
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mSingleTexture_2(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mRenderLayer_3(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mOriginalCullingMasks_4(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mStereoCameras_5(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mMeshes_6(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mTextures_7(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mStarted_8(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mVideoBackgroundChanged_9(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mOriginalLeftViewport_10(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mOriginalRightViewport_11(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mDualTextureLeftViewport_12(),
	DistortionRenderingBehaviour_t1858983148::get_offset_of_mDualTextureRightViewport_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (DelegateHelper_t3231076514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (PlayModeEyewearDevice_t2403363459), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (DedicatedEyewearDevice_t2070131990), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (CameraConfigurationUtility_t1452827745), -1, sizeof(CameraConfigurationUtility_t1452827745_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1735[6] = 
{
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MIN_CENTER_0(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_CENTER_1(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_BOTTOM_2(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_TOP_3(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_LEFT_4(),
	CameraConfigurationUtility_t1452827745_StaticFields::get_offset_of_MAX_RIGHT_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (BaseCameraConfiguration_t3118151474), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[10] = 
{
	BaseCameraConfiguration_t3118151474::get_offset_of_mCameraDeviceMode_0(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mLastVideoBackGroundMirroredFromSDK_1(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mOnVideoBackgroundConfigChanged_2(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mVideoBackgroundBehaviours_3(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mVideoBackgroundViewportRect_4(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mRenderVideoBackground_5(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mProjectionOrientation_6(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mInitialReflection_7(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mBackgroundPlaneBehaviour_8(),
	BaseCameraConfiguration_t3118151474::get_offset_of_mCameraParameterChanged_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (BaseStereoViewerCameraConfiguration_t2611724717), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[5] = 
{
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mPrimaryCamera_10(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mSecondaryCamera_11(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mSkewFrustum_12(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mScreenWidth_13(),
	BaseStereoViewerCameraConfiguration_t2611724717::get_offset_of_mScreenHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (StereoViewerCameraConfiguration_t4050045721), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[6] = 
{
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedLeftNearClipPlane_15(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedLeftFarClipPlane_16(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedRightNearClipPlane_17(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mLastAppliedRightFarClipPlane_18(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mCameraOffset_19(),
	StereoViewerCameraConfiguration_t4050045721::get_offset_of_mIsDistorted_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (HoloLensExtendedTrackingManager_t2009717195), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[15] = 
{
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mNumFramesStablePose_0(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxPoseRelDistance_1(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxPoseAngleDiff_2(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxCamPoseAbsDistance_3(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxCamPoseAngleDiff_4(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMinNumFramesPoseOff_5(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMinPoseUpdateRelDistance_6(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMinPoseUpdateAngleDiff_7(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackableSizeInViewThreshold_8(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mMaxDistanceFromViewCenterForPoseUpdate_9(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mSetWorldAnchors_10(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackingList_11(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackablesExtendedTrackingEnabled_12(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mTrackablesCurrentlyExtendedTracked_13(),
	HoloLensExtendedTrackingManager_t2009717195::get_offset_of_mExtendedTrackablesState_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (PoseInfo_t1612729179)+ sizeof (RuntimeObject), sizeof(PoseInfo_t1612729179 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[3] = 
{
	PoseInfo_t1612729179::get_offset_of_Position_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseInfo_t1612729179::get_offset_of_Rotation_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseInfo_t1612729179::get_offset_of_NumFramesPoseWasOff_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (PoseAgeEntry_t2181165958)+ sizeof (RuntimeObject), sizeof(PoseAgeEntry_t2181165958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[3] = 
{
	PoseAgeEntry_t2181165958::get_offset_of_Pose_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseAgeEntry_t2181165958::get_offset_of_CameraPose_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	PoseAgeEntry_t2181165958::get_offset_of_Age_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (VuforiaExtendedTrackingManager_t262318595), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (VuMarkManagerImpl_t1545617414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[6] = 
{
	VuMarkManagerImpl_t1545617414::get_offset_of_mBehaviours_0(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mActiveVuMarkTargets_1(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mDestroyedBehaviours_2(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mOnVuMarkDetected_3(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mOnVuMarkLost_4(),
	VuMarkManagerImpl_t1545617414::get_offset_of_mOnVuMarkBehaviourDetected_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (InstanceIdImpl_t2824054591), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[5] = 
{
	InstanceIdImpl_t2824054591::get_offset_of_mDataType_0(),
	InstanceIdImpl_t2824054591::get_offset_of_mBuffer_1(),
	InstanceIdImpl_t2824054591::get_offset_of_mNumericValue_2(),
	InstanceIdImpl_t2824054591::get_offset_of_mDataLength_3(),
	InstanceIdImpl_t2824054591::get_offset_of_mCachedStringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (VuMarkTargetImpl_t1052843922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1747[3] = 
{
	VuMarkTargetImpl_t1052843922::get_offset_of_mVuMarkTemplate_0(),
	VuMarkTargetImpl_t1052843922::get_offset_of_mInstanceId_1(),
	VuMarkTargetImpl_t1052843922::get_offset_of_mTargetId_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (VuMarkTemplateImpl_t667343433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[2] = 
{
	VuMarkTemplateImpl_t667343433::get_offset_of_mOrigin_4(),
	VuMarkTemplateImpl_t667343433::get_offset_of_mTrackingFromRuntimeAppearance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (RotationalDeviceTracker_t2847210804), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (MODEL_CORRECTION_MODE_t1953038946)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1750[4] = 
{
	MODEL_CORRECTION_MODE_t1953038946::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (CustomViewerParameters_t463762113), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (DeviceTrackingManager_t3849131975), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[4] = 
{
	DeviceTrackingManager_t3849131975::get_offset_of_mDeviceTrackerPositonOffset_0(),
	DeviceTrackingManager_t3849131975::get_offset_of_mDeviceTrackerRotationOffset_1(),
	DeviceTrackingManager_t3849131975::get_offset_of_mBeforeDevicePoseUpdated_2(),
	DeviceTrackingManager_t3849131975::get_offset_of_mAfterDevicePoseUpdated_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (FactorySetter_t2184571743), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (BackgroundPlaneAbstractBehaviour_t4147679365), -1, sizeof(BackgroundPlaneAbstractBehaviour_t4147679365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1754[13] = 
{
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mTextureInfo_2(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mViewWidth_3(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mViewHeight_4(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mNumFramesToUpdateVideoBg_5(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mCamera_6(),
	BackgroundPlaneAbstractBehaviour_t4147679365_StaticFields::get_offset_of_maxDisplacement_7(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_defaultNumDivisions_8(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mMesh_9(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mStereoDepth_10(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mBackgroundOffset_11(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mVuforiaBehaviour_12(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mBackgroundPlacedCallback_13(),
	BackgroundPlaneAbstractBehaviour_t4147679365::get_offset_of_mNumDivisions_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (RotationalPlayModeDeviceTrackerImpl_t4048275232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[3] = 
{
	RotationalPlayModeDeviceTrackerImpl_t4048275232::get_offset_of_mRotation_1(),
	RotationalPlayModeDeviceTrackerImpl_t4048275232::get_offset_of_mModelCorrectionTransform_2(),
	RotationalPlayModeDeviceTrackerImpl_t4048275232::get_offset_of_mModelCorrection_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (RotationalDeviceTrackerImpl_t1573407597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (IOSCamRecoveringHelper_t4035151671), -1, sizeof(IOSCamRecoveringHelper_t4035151671_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1758[6] = 
{
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sHasJustResumed_0(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sCheckFailedFrameAfterResume_1(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sCheckedFailedFrameCounter_2(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sWaitToRecoverCameraRestart_3(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sWaitedFrameRecoverCounter_4(),
	IOSCamRecoveringHelper_t4035151671_StaticFields::get_offset_of_sRecoveryAttemptCounter_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (MeshUtils_t922322213), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (ExternalStereoCameraConfiguration_t3614889463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1760[20] = 
{
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftNearClipPlane_15(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftFarClipPlane_16(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightNearClipPlane_17(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightFarClipPlane_18(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftVerticalVirtualFoV_19(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftHorizontalVirtualFoV_20(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightVerticalVirtualFoV_21(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightHorizontalVirtualFoV_22(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedLeftProjection_23(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mLastAppliedRightProjection_24(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftNearClipPlane_25(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftFarClipPlane_26(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightNearClipPlane_27(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightFarClipPlane_28(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftVerticalVirtualFoV_29(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewLeftHorizontalVirtualFoV_30(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightVerticalVirtualFoV_31(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mNewRightHorizontalVirtualFoV_32(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mExternallySetLeftMatrix_33(),
	ExternalStereoCameraConfiguration_t3614889463::get_offset_of_mExternallySetRightMatrix_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (NullHideExcessAreaClipping_t465635818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (StencilHideExcessAreaClipping_t2657238862), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[12] = 
{
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mGameObject_0(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mMatteShader_1(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mClippingPlane_2(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCamera_3(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraNearPlane_4(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraFarPlane_5(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraPixelRect_6(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mCameraFieldOfView_7(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mPlanesActivated_8(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mBgPlane_9(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mBgPlaneLocalPos_10(),
	StencilHideExcessAreaClipping_t2657238862::get_offset_of_mBgPlaneLocalScale_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (LegacyHideExcessAreaClipping_t1318580222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[20] = 
{
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mGameObject_0(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mMatteShader_1(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBgPlane_2(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mLeftPlane_3(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mRightPlane_4(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mTopPlane_5(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBottomPlane_6(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCamera_7(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBgPlaneLocalPos_8(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBgPlaneLocalScale_9(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCameraNearPlane_10(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCameraPixelRect_11(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mCameraFieldOFView_12(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mHideBehaviours_13(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mDeactivatedHideBehaviours_14(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mPlanesActivated_15(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mLeftPlaneCachedScale_16(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mRightPlaneCachedScale_17(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mBottomPlaneCachedScale_18(),
	LegacyHideExcessAreaClipping_t1318580222::get_offset_of_mTopPlaneCachedScale_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (DedicatedEyewearCameraConfiguration_t2854098828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[11] = 
{
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mPrimaryCamera_10(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mSecondaryCamera_11(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mScreenWidth_12(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mScreenHeight_13(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNeedToCheckStereo_14(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mLastAppliedNearClipPlane_15(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mLastAppliedFarClipPlane_16(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNewNearClipPlane_17(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNewFarClipPlane_18(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mNewVirtualFoV_19(),
	DedicatedEyewearCameraConfiguration_t2854098828::get_offset_of_mEyewearDevice_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (NullCameraConfiguration_t2773452281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[1] = 
{
	NullCameraConfiguration_t2773452281::get_offset_of_mProjectionOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (MonoCameraConfiguration_t112386736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[6] = 
{
	MonoCameraConfiguration_t112386736::get_offset_of_mPrimaryCamera_10(),
	MonoCameraConfiguration_t112386736::get_offset_of_mCameraViewPortWidth_11(),
	MonoCameraConfiguration_t112386736::get_offset_of_mCameraViewPortHeight_12(),
	MonoCameraConfiguration_t112386736::get_offset_of_mLastAppliedNearClipPlane_13(),
	MonoCameraConfiguration_t112386736::get_offset_of_mLastAppliedFarClipPlane_14(),
	MonoCameraConfiguration_t112386736::get_offset_of_mLastAppliedFoV_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (UnityCameraExtensions_t3394190193), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (View_t3879626884)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1769[6] = 
{
	View_t3879626884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (ViewerParameters_t3396315024), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[1] = 
{
	ViewerParameters_t3396315024::get_offset_of_mNativeVP_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (ViewerParametersList_t3991990123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[1] = 
{
	ViewerParametersList_t3991990123::get_offset_of_mNativeVPL_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (ObjectTargetAbstractBehaviour_t2569206187), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[12] = 
{
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mObjectTarget_20(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mAspectRatioXY_21(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mAspectRatioXZ_22(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mShowBoundingBox_23(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mBBoxMin_24(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mBBoxMax_25(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mPreviewImage_26(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mLength_27(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mWidth_28(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mHeight_29(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mLastTransformScale_30(),
	ObjectTargetAbstractBehaviour_t2569206187::get_offset_of_mLastSize_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (CameraDevice_t960297568), -1, sizeof(CameraDevice_t960297568_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1775[1] = 
{
	CameraDevice_t960297568_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (CameraDeviceMode_t2478715656)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1776[4] = 
{
	CameraDeviceMode_t2478715656::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (CameraDirection_t637748435)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1777[4] = 
{
	CameraDirection_t637748435::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (VideoModeData_t2066817255)+ sizeof (RuntimeObject), sizeof(VideoModeData_t2066817255 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[4] = 
{
	VideoModeData_t2066817255::get_offset_of_width_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoModeData_t2066817255::get_offset_of_height_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoModeData_t2066817255::get_offset_of_frameRate_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VideoModeData_t2066817255::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (CloudRecoAbstractBehaviour_t3388674407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[9] = 
{
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mObjectTracker_2(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mCurrentlyInitializing_3(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mInitSuccess_4(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mCloudRecoStarted_5(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mOnInitializedCalled_6(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mHandlers_7(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_mTargetFinderStartedBeforeDisable_8(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_AccessKey_9(),
	CloudRecoAbstractBehaviour_t3388674407::get_offset_of_SecretKey_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (ReconstructionImpl_t3350922794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1780[5] = 
{
	ReconstructionImpl_t3350922794::get_offset_of_mNativeReconstructionPtr_0(),
	ReconstructionImpl_t3350922794::get_offset_of_mMaximumAreaIsSet_1(),
	ReconstructionImpl_t3350922794::get_offset_of_mMaximumArea_2(),
	ReconstructionImpl_t3350922794::get_offset_of_mNavMeshPadding_3(),
	ReconstructionImpl_t3350922794::get_offset_of_mNavMeshUpdatesEnabled_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (HideExcessAreaAbstractBehaviour_t3369390328), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[7] = 
{
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mClippingImpl_2(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mClippingMode_3(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mVuforiaBehaviour_4(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mVideoBgMgr_5(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mPlaneOffset_6(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mSceneScaledDown_7(),
	HideExcessAreaAbstractBehaviour_t3369390328::get_offset_of_mStarted_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (CLIPPING_MODE_t259981078)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1782[4] = 
{
	CLIPPING_MODE_t259981078::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (ObjectTargetImpl_t3614635090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[2] = 
{
	ObjectTargetImpl_t3614635090::get_offset_of_mSize_2(),
	ObjectTargetImpl_t3614635090::get_offset_of_mDataSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (UnityPlayer_t3127875071), -1, sizeof(UnityPlayer_t3127875071_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1784[1] = 
{
	UnityPlayer_t3127875071_StaticFields::get_offset_of_sPlayer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (NullUnityPlayer_t2922069181), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (PlayModeUnityPlayer_t3763348594), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (ReconstructionFromTargetImpl_t676137874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[6] = 
{
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderMin_5(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderMax_6(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderOffset_7(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mOccluderRotation_8(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mInitializationTarget_9(),
	ReconstructionFromTargetImpl_t676137874::get_offset_of_mCanAutoSetInitializationTarget_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (Device_t64880687), -1, sizeof(Device_t64880687_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1788[1] = 
{
	Device_t64880687_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (Mode_t3830573374)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[3] = 
{
	Mode_t3830573374::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (ViewerButtonType_t3221680132)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1790[5] = 
{
	ViewerButtonType_t3221680132::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (ViewerTrayAlignment_t2810797062)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1791[4] = 
{
	ViewerTrayAlignment_t2810797062::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (VuforiaAbstractBehaviour_t3510878193), -1, sizeof(VuforiaAbstractBehaviour_t3510878193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1793[17] = 
{
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mWorldCenterMode_2(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mWorldCenter_3(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mCentralAnchorPoint_4(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mParentAnchorPoint_5(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mPrimaryCamera_6(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mSecondaryCamera_7(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_mWereBindingFieldsExposed_8(),
	VuforiaAbstractBehaviour_t3510878193_StaticFields::get_offset_of_BehaviourCreated_9(),
	VuforiaAbstractBehaviour_t3510878193_StaticFields::get_offset_of_BehaviourDestroyed_10(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_AwakeEvent_11(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnEnableEvent_12(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_StartEvent_13(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_UpdateEvent_14(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnLevelWasLoadedEvent_15(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnApplicationPauseEvent_16(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnDisableEvent_17(),
	VuforiaAbstractBehaviour_t3510878193::get_offset_of_OnDestroyEvent_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (VuforiaAbstractConfiguration_t2684447159), -1, sizeof(VuforiaAbstractConfiguration_t2684447159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1794[9] = 
{
	VuforiaAbstractConfiguration_t2684447159_StaticFields::get_offset_of_mInstance_2(),
	VuforiaAbstractConfiguration_t2684447159_StaticFields::get_offset_of_mPadlock_3(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_vuforia_4(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_digitalEyewear_5(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_databaseLoad_6(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_videoBackground_7(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_smartTerrainTracker_8(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_deviceTracker_9(),
	VuforiaAbstractConfiguration_t2684447159::get_offset_of_webcam_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (GenericVuforiaConfiguration_t1909783692), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[8] = 
{
	GenericVuforiaConfiguration_t1909783692::get_offset_of_vuforiaLicenseKey_0(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_delayedInitialization_1(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_cameraDeviceModeSetting_2(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_maxSimultaneousImageTargets_3(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_maxSimultaneousObjectTargets_4(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_useDelayedLoadingObjectTargets_5(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_cameraDirection_6(),
	GenericVuforiaConfiguration_t1909783692::get_offset_of_mirrorVideoBackground_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (DigitalEyewearConfiguration_t2828783036), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[10] = 
{
	DigitalEyewearConfiguration_t2828783036::get_offset_of_cameraOffset_0(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_distortionRenderingMode_1(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_distortionRenderingLayer_2(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_eyewearType_3(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_stereoFramework_4(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_seeThroughConfiguration_5(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_viewerName_6(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_viewerManufacturer_7(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_useCustomViewer_8(),
	DigitalEyewearConfiguration_t2828783036::get_offset_of_customViewer_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (DatabaseLoadConfiguration_t3815674388), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[2] = 
{
	DatabaseLoadConfiguration_t3815674388::get_offset_of_dataSetsToLoad_0(),
	DatabaseLoadConfiguration_t3815674388::get_offset_of_dataSetsToActivate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (VideoBackgroundConfiguration_t1174662728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[3] = 
{
	VideoBackgroundConfiguration_t1174662728::get_offset_of_clippingMode_0(),
	VideoBackgroundConfiguration_t1174662728::get_offset_of_matteShader_1(),
	VideoBackgroundConfiguration_t1174662728::get_offset_of_videoBackgroundEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (TrackerConfiguration_t588160133), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[2] = 
{
	TrackerConfiguration_t588160133::get_offset_of_autoInitTracker_0(),
	TrackerConfiguration_t588160133::get_offset_of_autoStartTracker_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
