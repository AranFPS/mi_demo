﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MainMenuController : MonoBehaviour {

	[SerializeField]
	public LayerMask collisionLayer = 1 << 10;

	[SerializeField]
	public Animator _instructionAnimator;

	[SerializeField]
	public GameObject _instructionText;

	[SerializeField]
	public UnityEngine.UI.Button _instructionButton;

	[SerializeField]
	public bool _testing;

	void Update(){
		#if UNITY_EDITOR
		if (Input.GetMouseButtonDown (0)) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 50, collisionLayer)) {
				print(hit.transform.name);
				MainManager.Instance.SetCurrentScene(hit.transform.GetComponent<MenuButton>()._myScene);
			}
		}
		#else
		var touch = Input.GetTouch(0);
		if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved) {
			Ray ray = Camera.main.ScreenPointToRay (touch.position);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit, 50, collisionLayer)) {
				MainManager.Instance.SetCurrentScene(hit.transform.GetComponent<MenuButton>()._myScene);
			}
		}
		#endif
	}

	private bool _isInstructionOpened = true;
	public void OpenCloseInstruction(){
		if (_isInstructionOpened) {
			_isInstructionOpened = false;
			_instructionText.SetActive (_isInstructionOpened);
		} else {
			_isInstructionOpened = true;
		}
		_instructionAnimator.SetTrigger ("StateChange");
		_instructionButton.interactable = false;
		StartCoroutine (ResetButton ());
	}	

	private IEnumerator ResetButton(){
		yield return new WaitForSeconds (1f);
		_instructionButton.interactable = true;
	}
}
