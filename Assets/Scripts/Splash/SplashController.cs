﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashController : MonoBehaviour {

	#region Varables
	[SerializeField]
	Image _fadeImage;


	[SerializeField]
	float _fadeTime;

	#endregion



	#region Private Methods

	IEnumerator Start(){
		StartCoroutine (FadeIn ());

		yield return FadeIn ();

		StartCoroutine (FadeOut ());

		yield return FadeOut ();

		MainManager.Instance.SetCurrentScene (SCENES.MainMenu);
	}

	private IEnumerator FadeIn(){
		float t = 0;

		Debug.LogFormat ("Start FadeIn, current t = {0}", t);

		while (t < 1) {
			t += Time.deltaTime / _fadeTime;

			_fadeImage.color = new Color (_fadeImage.color.r, _fadeImage.color.g, _fadeImage.color.b, t);
				
			yield return null;
		}

		Debug.LogFormat ("End FadeIn, current t = {0}", t);
	}

	private IEnumerator FadeOut(){
		float t = 1;

		Debug.LogFormat ("Start FadeOut, current t = {0}", t);


		while (t > 0) {
			t -= Time.deltaTime / _fadeTime;
		
			_fadeImage.color = new Color (_fadeImage.color.r, _fadeImage.color.g, _fadeImage.color.b, t);

			yield return null;
		}

		Debug.LogFormat ("End FadeOut, current t = {0}", t);
	}

	#endregion


}
