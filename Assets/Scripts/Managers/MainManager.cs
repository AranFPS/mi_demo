﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MainManager : Singleton <MainManager> {

	#region Varables

	public SCENES _currentScene;
	#endregion

	#region Public Methods

	public void SetCurrentScene(SCENES sceneToSet){

		_currentScene = sceneToSet;

		GotoScene ();

	}

	#endregion

	#region Private Methods

	private void GotoScene(){

		switch (_currentScene) {
		case SCENES.Splash:
			SceneManager.LoadScene (0);

			break;
		case SCENES.MainMenu:
			SceneManager.LoadScene (1);

			break;
		case SCENES.ImageTracking:
			SceneManager.LoadScene (2);

			break;
		case SCENES.ThreeSixty:
			SceneManager.LoadScene (3);

			break;
		}

	}

	#endregion

}
