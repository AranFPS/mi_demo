﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour {

	public Animator anime;
	private bool rotate;
	float minSpeed = 1;
	float maxSpeed = 1.2f;
	float currentSpeed;

	private Vector2 fingerDown;
	private Vector2 fingerUp;
	public bool detectSwipeOnlyAfterRelease = false;

	public float SWIPE_THRESHOLD = 20f;

	// Use this for initialization
	void Start () {
		anime = GetComponent<Animator> ();
		rotate = true;
	}
		
	// Update is called once per frame
	void Update ()
	{
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				fingerUp = touch.position;
				fingerDown = touch.position;
			}
			if (touch.phase == TouchPhase.Moved) {
				if (!detectSwipeOnlyAfterRelease) {
					fingerDown = touch.position;
					checkSwipe ();
				}
			}
			if (touch.phase == TouchPhase.Ended) {
				fingerDown = touch.position;
				checkSwipe ();
			}
		}

		if (rotate == true) {
			transform.Rotate (0, minSpeed, 0, Space.World);
		}
		//if (rotate == true && currentSpeed > 1) {
		//	transform.RotateAround (Vector3.down, currentSpeed -=  0.1f * Time.deltaTime);
		//	print (currentSpeed);
		//}
		//if (rotate == true && currentSpeed < 1) {
		//	print (currentSpeed);
		//	currentSpeed = minSpeed;
		//	transform.Rotate (0,currentSpeed, 0, Space.World);
		//}
		if (rotate == false) {
			transform.Rotate (0, 0, 0, Space.World);
		//	float x = Input.GetAxis ("Mouse X") * maxSpeed * Mathf.Deg2Rad;
		//	transform.RotateAround (Vector3.down, x);
		//	if (x > 1) {
		//		currentSpeed = x;
		//		currentSpeed = 1.1f;
			}
		//
		//}

		if (Input.GetMouseButtonDown (0)) {
			anime.Play ("POSE30");
			rotate = false;
		}
		if (Input.GetMouseButtonUp (0)) {
			anime.Play ("POSE28");
			rotate = true;
		}
	}

	void checkSwipe()
	{
		if (verticalMove() > SWIPE_THRESHOLD && verticalMove() > horizontalValMove())
		{
			if (fingerDown.y - fingerUp.y > 0)//up swipe
			{
				OnSwipeUp();
			}
			else if (fingerDown.y - fingerUp.y < 0)//Down swipe
			{
				OnSwipeDown();
			}
			fingerUp = fingerDown;
		}
			
		else if (horizontalValMove() > SWIPE_THRESHOLD && horizontalValMove() > verticalMove())
		{
			if (fingerDown.x - fingerUp.x > 0)//Right swipe
			{
				OnSwipeRight();
			}
			else if (fingerDown.x - fingerUp.x < 0)//Left swipe
			{
				OnSwipeLeft();
			}
			fingerUp = fingerDown;
		}
		else
		{
			
		}
	}

	float verticalMove()
	{
		return Mathf.Abs(fingerDown.y - fingerUp.y);
	}

	float horizontalValMove()
	{
		return Mathf.Abs(fingerDown.x - fingerUp.x);
	}

	//////////////////////////////////CALLBACK FUNCTIONS/////////////////////////////
	void OnSwipeUp()
	{
		Debug.Log("Swipe UP");
	}

	void OnSwipeDown()
	{
		Debug.Log("Swipe Down");
	}

	void OnSwipeLeft()
	{
		Debug.Log("Swipe Left");
	}

	void OnSwipeRight()
	{
		Debug.Log("Swipe Right");
	}
}	
	
