﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageTrackingController : MonoBehaviour {

	public GameObject _unityChan;

	public void OnTracked(){
		_unityChan.SetActive (true);
	}

	public void OnLost(){
		_unityChan.SetActive (false);
	}

	public void GoBack(){
		MainManager.Instance.SetCurrentScene (SCENES.MainMenu);
	}
}
